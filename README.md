# Archive management tool
#
# Python 3 / Django
#
# demo version

createdb -U postgres -h localhost archive

./manage.py makemigrations

./manage.py migrate

./manage.py runserver
