// JavaScript Document
Number.prototype.formatMoney = function(c, d, t) {
    c = isNaN(c = Math.abs(c)) ? 2 : c;
    d = d || ".";
    t = t || ",";
    var n = this,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (i.length) > 3 ? i.length % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };

function switchPass(obj,type) {
	$(obj).addClass('hidden').siblings().removeClass('hidden');
	if (type=='in') $(obj).siblings().focus();
}

/**
 * Attach datepicker to all elements named date_from, date_to
 */
function attachDatepicker(){

    var conf = {
        yearRange: '2009:' + new Date().getFullYear(),
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yy-mm-dd'
    };
    $("input[name=date_from], input[name=date_to]").datepicker(conf);
}



$(document).ready(function(){
    $('#menuLeft').find('ul li.cur ul li a.cur').each(function(){
        var linkText = $(this).text();
        $(this).html('<span class="t"><span class="b">'+linkText+'</span></span>');
    });
    try{$('form.transFormx :input, .blockButtons a').uniform();}catch (e){}
    // attach datepickers
    try{attachDatepicker();}catch (e){}
	$('a.toggleSpoiler').click(function(){
		$(this).toggleClass('hidden');
		$(this).parent().next('div.spoiler').slideToggle();		// due to div.blockLinks
		return false;
	});
	$('table.default tr:not(".legend")').hover(function(){
			$('td',this).css({'background-color':'#faf0d2','border-color':'#f9c923'});
			$(this).prev('tr').find('td').css({'border-color':'#f9c923'});
		},function(){
			if ($(this).hasClass('c')) {
				$('td',this).css({'background-color':'#f8f8f8'});
			} else {
				$('td',this).css({'background-color':'#fff'});
			}
			$('td',this).css({'border-color':'#e1e1e1'});
			$(this).prev('tr').find('td').css({'border-color':'#e1e1e1'});
		});

    $('#datatable').bind('update_chart', function(event, params) {
        var is_report_admin = params.indexOf('report=SPR') != -1,
            is_report_aff = params.indexOf('report=AAPR') != -1,
            is_report_merch = params.indexOf('report=SMP') != -1;
        if (!(is_report_admin || is_report_aff || is_report_merch)) {
           return;
        }

        var $table = $(this), data = [], html = '', re_money = /(-?\d+(,\d+)*[\.,]\d+)/, regex_float = /^\d+[,\.]\d+$/,
            $rows = $table.find('tr:not(.legend)');
        if (!$rows.length) return;
        var col_count = $rows[0].childNodes.length;
        for (var i = 0; i < col_count; i++) {
            var res = 0, is_float_type = false, money_pattern = '';
            $.each($rows, function(r, $row){
                var cellVal = $row.childNodes[i].innerHTML;
                if (!is_float_type) {
                    is_float_type = re_money.test(cellVal);
                    money_pattern = cellVal;
                }
                // skip if date or not number
                if (cellVal.indexOf('-') != -1 || (isNaN(parseFloat(cellVal)) && !is_float_type)) {
                    res = '-';
                    return false;
                }
                if (regex_float.test(cellVal)) {
                    res += parseFloat(cellVal.replace(',', '.'));
                } else {
                    res += parseFloat(cellVal.replace(/[^\d.]/g, ''));
                }
            });
            data[i] = (is_float_type) ? money_pattern.replace(re_money, res.formatMoney(2)) : res;
        }

        if (is_report_admin) {
            var earned = data[10].replace(/[^\d.]/g, ''), created = data[4], sold = data[5],
                avg_price = (created) ? earned / created : 0,
                avg_price_sold = (sold) ? earned / sold : 0,
                conversion = (created) ? sold / created * 100 : 0;
            data[7] = (avg_price) ? avg_price.formatMoney(2) : 0;
            data[8] = (avg_price_sold) ? avg_price_sold.formatMoney(2) : 0;
            data[9] = (conversion) ? conversion.formatMoney(2) : 0;
        } else if (is_report_aff) {
            var earned = data[9].replace(/[^\d.]/g, ''), created = data[3], sold = data[4],
                avg_price = (created) ? earned / created : 0,
                avg_price_sold = (sold) ? earned / sold : 0,
                conversion = (created) ? sold / created * 100 : 0;
            data[6] = (avg_price) ? avg_price.formatMoney(2) : 0;
            data[7] = (avg_price_sold) ? avg_price_sold.formatMoney(2) : 0;
            data[8] = (conversion) ? conversion.formatMoney(2) : 0;
        }

        for (var x = 0, len = data.length; x < len; x++) {
            html += '<th>' + ((!x) ? "Summary:" : data[x]) + '</th>';
        }
        $table.find('tbody').append('<tr class="legend">' + html + '</tr>');
    });
});

/**
 * regex replacer for removeCurrencyFromRevenue
 * @param str
 * @param p1
 * @param offset
 * @param s
 * @returns {*}
 */
function sumWithCurrReplacer(str, p1, offset, s) {
    var res, regex_float = /^\d+[,\.]\d+$/;
    if (regex_float.test(p1)) {
        res = parseFloat(p1.replace(',', '.'));
    } else {
        res = parseFloat(p1.replace(/[^\d.]/g, ''));
    }
    return res;
}

/**
 * clear currency symbols from revenue for Chart
 * @param data
 * @returns {*}
 */
function removeCurrencyFromRevenue(data) {
    var loop_count = (data.match(/endsum/g) || []).length,
        res = data,
        re_patt_str = '<!--sum{i}-->(.*)<!--endsum{i}-->';
    for (var i = 1; i <= loop_count; i++) {
        var re_sum = new RegExp(re_patt_str.replace(new RegExp('{i}','g'), i), 'g');
        res = res.replace(re_sum, sumWithCurrReplacer)
    }
    return res;
}
