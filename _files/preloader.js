var i = 2;
var timer = setTimeout(function run() {
    if (i >= 9) {
        i = 1;
    }
    var preloader = document.getElementById("preloader");
    if (preloader != null) {
        preloader.setAttribute("class", "s" + i++);
        timer = setTimeout(run, 70);
    }
}, 10);
