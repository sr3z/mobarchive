# -*- coding: utf-8 -*-

from django.contrib.auth.models import User as DefaultUser, Group
from django.db import models
from django.utils.translation import ugettext_lazy as _
from archive.utils.models import TimeStampedModelIndexed
from archive.users.models import Customer
from archive.storage.models import Item
from django.conf import settings

MISSION_STATUS = (
    ('debug', 'Отладка'),
    ('inprogress', 'Выполняется'),
    ('done', 'Выполнена'),
)

MISSION_NAME = (
    ('IN', 'Прием'),
    ('OUT', 'Выдача'),
    ('REMOVE', 'Уничтожение'),
    ('DEFRAG', 'Дефрагментация'),
)

class Mission(TimeStampedModelIndexed):
    name = models.CharField(
        _('Задание'), max_length=20,
        choices=MISSION_NAME,
        blank=True, null=True,
    )
    owner = models.ForeignKey(DefaultUser,
                null=True, blank=True,
                on_delete=models.SET_NULL,
                verbose_name=_(u"Архивариус")
    )
    status = models.CharField(
        _('Состояние'), max_length=20,
        choices=MISSION_STATUS,
        blank=True, null=True,
    )

    def __str__(self):
        return '"%s" %s' % ([name for name in MISSION_NAME if name[0] == self.name][0][1], self.owner)

    def __unicode__(self):
        return '%s %s %s' % (self.name, self.owner, self.created)

    class Meta:
        verbose_name = _('Операция')
        verbose_name_plural = _('Операции')
        ordering = ['created']


class Task(TimeStampedModelIndexed):
    command = models.CharField(
        _('Команда'), max_length=20,
        blank=True, null=True,
    )
    status = models.CharField(
        _('Состояние'), max_length=20,
        choices=MISSION_STATUS,
        blank=True, null=True,
    )
    item = models.ForeignKey(Item, blank=True, null=True)
    mission = models.ForeignKey(Mission, models.CASCADE)
    owner = models.ForeignKey(DefaultUser,
                              null=True, blank=True,
                              on_delete=models.SET_NULL,
                              verbose_name=_(u"Выдан"))
    description = models.CharField(_(u'Комментарий'), max_length=1024, null=True, blank=True)

    search = models.TextField(_(u'Поиск'), null=True, blank=True)

    def create_index(self):
        if self.mission and self.mission.status == 'done':
            return
        search = ''
        if self.item:
            search = search + self.item.number.replace(' ', '')
            search = search + self.item.name.replace(' ', '')
        if self.description:
            search = search + self.description.replace(' ', '')
        if self.item and self.item.slot_set:
            if self.item.slot_set.all().first():
                search = search + self.item.slot_set.all().first().shelf.section.row.rack.rack_group.__str__().replace('', '')
                search = search + self.item.slot_set.all().first().shelf.section.row.__str__().replace('', '')
                search = search + self.item.slot_set.all().first().shelf.section.__str__().replace('', '')
                search = search + self.item.slot_set.all().first().shelf.__str__().replace('', '')
                search = search + self.item.slot_set.all().first().__str__().replace('', '')
        if self.owner:
            search = search + self.owner.__str__().replace(' ', '')
        search = search + self.__str__().replace(' ', '')

        if search and len(search) > 1:
            self.search = search.lower().replace(' ' , '')
        else:
            self.search = ''

        if settings.DEBUG_MESSAGES:
            print("LOG: %s SEARCH: %s" % (self.__class__, self.search))
        return self.search

    def save(self, *args, **kwargs):
        self.create_index()
        super(self.__class__, self).save(*args, **kwargs)
        self.create_index()

    def __str__(self):
        return '[%s]' % ([status for status in MISSION_STATUS if status[0] == self.status][0][1])

    def __unicode__(self):
        return 'Задача #%s [%s]' % (self.id, [status for status in MISSION_STATUS if status[0] == self.status][0][1])

    class Meta:
        verbose_name = _('Задание')
        ordering = ['created']

class License(TimeStampedModelIndexed):
    code = models.CharField(
        _('Код'), max_length=1024,
        null=True,
    )
