import socket
from socket import *
import sys
import time
import timeout_decorator
from timeout_decorator.timeout_decorator import TimeoutError
from collections import Iterable, OrderedDict
from codecs import decode

TIMEOUT=5
BUFFER_SIZE=1024

def hex_command(data):
    #data_str = '[ {} ]'.format(' ][ '.join(map(lambda i: str(i), data)))
    # print data_str
    map_data = ['{:02x}'.format(c) for c in data]
    #data_str_hex = '[ {} ]'.format(' ][ '.join(map(lambda c: '\\x{}'.format(c), map_data)))
    #print data_str_hex
    data_send = decode(''.join(map_data), 'hex_codec')
    print('LOG: NET: DATA:', list(data))
    return data_send

#@timeout_decorator.timeout(TIMEOUT)
def wait_for_response(udp_socket):
    #print 'LOG: NET: LISTEN AT:", addr2
    data = udp_socket.recvfrom(BUFFER_SIZE)
    message = data[0]
    #print('LOG: NET: RECV: 1 MESSAGE:', list(message), 'MAPPED:', list(map(ord, message))) # python 2
    print('LOG: NET: RECV: 1 MESSAGE:', list(message), 'MAPPED:', list(message))
    data = udp_socket.recvfrom(BUFFER_SIZE)
    message = data[0]
    #print('LOG: NET: RECV: 2 MESSAGE:', list(message), 'MAPPED:', list(map(ord, message))) # python 2
    print('LOG: NET: RECV: 2 MESSAGE:', list(message), 'MAPPED:', list(message))
    print('')
    #result = ord(list(message)[2]) # python 2
    result = list(message)[2]
    print('LOG: NET: BYTE[3]:', result)
    print('')
    return int(result)


def send_packet(host, port_src, dest, port_dst, command='\x0b\x00\x00\x00'):
    addr = (host, int(port_src))
    controller = (dest, int(port_dst))
    udp_socket = socket(AF_INET, SOCK_DGRAM)

    if command[0] == '\x01':
        for i in range(0, MOBILE_RACK_COUNT):
            for j in range(1, 3):
                #command2 = [1, (i * 2 + j), 0, 0]
                print('LOG: NET: command', command2)
                command2 = [int(command[0]), int(i * 2 + j), int(command[2]), int(command[3])]

                print('LOG: NET: SENDING FROM:', addr, ' FROM PAIR #', j, ' UPDATED DATA:', list(command2), ' TO:', controller)
                udp_socket.sendto(hex_command(command2), controller)

                try:
                    result = wait_for_response(udp_socket)
                except TimeoutError:
                    print('LOG: NET: NO RESPONSE ..\n')
                time.sleep(0.5)
    else:
        print('LOG: NET: SENDING UDP FROM:', addr, 'DATA:', list(command), 'MAPPED:', list(command), 'TO:', controller)
        udp_socket.sendto(bytes(command), controller)

        try:
            result = wait_for_response(udp_socket)

            if command[0] == '\x0b':
                MOBILE_RACK_COUNT = result 
                print('LOG: NET: MOBILE_RACK_COUNT:', MOBILE_RACK_COUNT, "\n")
        except TimeoutError:
            print('LOG: NET: NO RESPONSE ..\n')
            pass
        time.sleep(0.5)

    print('LOG: NET: DONE ..\n')
    udp_socket.close()
    print('LOG: NET: EXITING ..')
    return result


def send_message(dest, port_dst, command='$014;58;1;73C3'):
    s = socket(AF_INET, SOCK_STREAM)
    controller = (dest, port_dst)
    s.connect(controller)
    print('LOG: NET: SENDING TCP ', 'DATA:', list(command), 'MAPPED:', list(map(ord, command)), 'TO:', controller)
    s.send(command.encode())
    print('LOG: NET: SENT, WAITING FOR RESPONSE\n')
    message = s.recv(BUFFER_SIZE)
    s.close()
    string = message.decode()
    parts = string.split(';')
    print('LOG: NET: RECV: MESSAGE:', string, list(string), 'MAPPED:', list(message), " PARTS:", len(parts))
    print('LOG: NET: DONE ..\n')

    if len(parts) == 4:
        print('LOG: NET: ERROR RECVED. EXITING ..')
        return parts[2]

    if ';54;' in command:
        if len(parts) > 1:
            if parts[1] == '2':
                print('LOG: NET: ERROR NACK RECVED. EXITING ..')
            else:
                print('LOG: NET: STATUS RECVED. EXITING ..')
            return parts[2]
        else:
            print('LOG: NET: UNKNOWN ERROR RECVED. EXITING ..')
            return 1

    print('LOG: NET: EXITING ..')
    return 1

