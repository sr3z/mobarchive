# coding: utf-8
from archive.utils.helpers import reverse_absolute_uri, get_remote_addr
from django.views.generic import ListView, UpdateView, View
from braces.views import JSONResponseMixin
from celery.task import task
from .network import send_message, send_packet
from archive.api.models import Task, Mission
from celeryapp import app
from django.db import transaction
import datetime
from django.forms import model_to_dict

from django.contrib.auth.models import User as DefaultUser, Group
from django.db.models import Q, Max, Min
from django.core.urlresolvers import reverse_lazy

from archive.storage.models import Slot, Command, ItemRemoved, Item, RackGroup
import time

def get_command_code(rackgroup_model, command_name, row_id=None):
    commands = Command.objects.filter(model=rackgroup_model, name=command_name)
    code = None
    print("LOG: ROW: %s" % (row_id))

    if not (commands and commands.count() > 0):
        message = u'Запрошенная команда "%s" отсутствует .. ' % (command_name)
        print("LOG: ERROR: %s" % (message))
        raise Exception(message)

    command = commands[0]

    if not command.code:
        if not command.function or len(command.function) < 3 or not hasattr(command, command.function):
            raise Exception(u'Отсутствует функция получения кода команды ..')

        try:
            if row_id != None:
                code = getattr(command, command.function)(int(row_id))
            else:
                code = getattr(command, command.function)()
        except Exception as e:
            print("LOG: FUNCTION ERROR: %s" % (e))
            response = 'FUNCTION ERROR: ' + str(e)
            raise Exception(response)
    else:
        code = command.code
    return code

def get_error_description(code, err_type):
    if err_type == 254:
        err_msg = 'помеха на левом инфракрасном барьере'
    elif err_type == 253:
        err_msg = 'помеха на правом инфракрасном барьере'
    elif err_type == 251:
        err_msg = 'слишком высокий ток двигателя'
    elif err_type == 247:
        err_msg = 'слишком низкий ток двигателя'
    elif err_type == 239:
        err_msg = 'сработал датчик движения'
    elif err_type == 223:
        err_msg = 'мобильная группа заблокирована'
    elif err_type == 191:
        err_msg = 'включен ручной режим управления'
    elif err_type == '1':
        err_msg = 'помеха на левом инфракрасном барьере'
    else:
        #err_msg = 'получена неизвестная ошибка с кодом {}'.format(err_type)
        err_msg = err_type

    return err_msg
    #return 'неисправен стеллаж номер {}. устраните неполадку и повторите команду'.format(code)


def prepare_command(command_name, rackgroup_id, row_id, remote_addr):
    rackgroups = RackGroup.objects.filter(pk=rackgroup_id).order_by('-created')

    if not (rackgroups and rackgroups.count() > 0):
        raise Exception(u'В команде отсутствует номер мобильной группы ..')

    rackgroup = rackgroups[0]
    code = get_command_code(rackgroup.model, command_name, row_id)

    print('\nLOG: SYNC COMMAND:', command_name, 'CODE:', code, 'PROTOCOL:', rackgroup.model, '\n')
    e = None

    if not code:
        raise Exception(u'Отсутствует код команды ..')

    try:
        response = send_command(command_name, rackgroup.model, rackgroup.ip, rackgroup.port, code)

        if not response:
            if 'open' == command_name and 1 == rackgroup.model:
                response = ''
                counter = 10
                code = get_command_code(rackgroup.model, 'get_status')

                while response != '5' and counter:
                    print (u'\nLOG: Запрос статуса контроллера %s ..' % (counter))
                    counter = counter - 1
                    response = send_command('get_status', rackgroup.model, rackgroup.ip, rackgroup.port, code)
                    time.sleep(1)
                error_variants = {'6': u'Движение влево', '7': u'Движение вправо', '3': u'Завершено', '12': u'Стоп'}

                if response in error_variants:
                    response = error_variants[response]

    except Exception as e:
        print("LOG: RUN ERROR: %s" % (e))
        response = '{0}'.format(e)

    if response:
        raise Exception(response)


class Status(JSONResponseMixin, ListView):

    http_method_names = ['get']
    error_message = 'Bad request'

    def get(self, request, *args, **kwargs):
        remote_addr = get_remote_addr(request)
        key = kwargs.get('key', None)
        #return HttpResponse(status=204)
        return self.render_json_response({'status': 'PENDING', 'remote_addr': remote_addr}, status=204)

class ExecuteSync(JSONResponseMixin, ListView):

    http_method_names = ['get']
    error_message = 'Bad request'

    def get(self, request, *args, **kwargs):
        remote_addr = get_remote_addr(request)
        command = kwargs.get('command', None)
        rackgroup_id = kwargs.get('key', None)
        print("LOG: KEY:%s" % (rackgroup_id))
        row_id = request.GET.get('id', None)
        print("LOG: ROW ID:%s" % (id))

        if not command:
            return self.render_json_response({'status': 'ERROR', 'remote_addr': remote_addr, 'message': 'Отсутствует имя команды'}, status=201)

        if not rackgroup_id:
            return self.render_json_response({'status': 'ERROR.', 'remote_addr': remote_addr, 'message': 'В команде отсутствует номер мобильной группы'}, status=201)

        error = None

        try:
            response = prepare_command(command, rackgroup_id, row_id, remote_addr)
        except Exception as e:
            error = True
            print("LOG: EXECUTE SYNC ERROR: %s" % (e))
            response = str(e)

        print("LOG: SYNC RESPONSE: ", response if response else 'empty (OK) ', "\n")

        if error:
            response = get_error_description(rackgroup_id, response)
            return self.render_json_response({'status': 'ERROR.', 'remote_addr': remote_addr, 'message': response}, status=201)
        else:
            response = 'OK'
            return self.render_json_response({'status': 'OK', 'remote_addr': remote_addr, 'uuid': response})



class Execute(JSONResponseMixin, ListView):

    http_method_names = ['get']
    error_message = 'Bad request'

    def get(self, request, *args, **kwargs):
        remote_addr = get_remote_addr(request)
        command = kwargs.get('command', None)
        key = kwargs.get('key', None)

        if not command:
            return self.render_json_response({'status': 'ERROR', 'remote_addr': remote_addr, 'message': 'Отсутствует имя команды'}, status=201)

        if not key:
            return self.render_json_response({'status': 'ERROR.', 'remote_addr': remote_addr, 'message': 'В команде отсутствует номер мобильной группы'}, status=201)
        # select command from class Command (controller, command_name, command_value)

        rackgroups = RackGroup.objects.filter(pk=key).order_by('-created')

        if not (rackgroups and rackgroups.count() > 0):
            return self.render_json_response({'status': 'ERROR.', 'remote_addr': remote_addr, 'message': 'В команде отсутствует номер мобильной группы ..'}, status=201)

        rackgroup = rackgroups[0]
        code = get_command_code(rackgroup.model, command_name, row_id)

        print('\nCOMMAND:', command, 'CODE:', code, 'PROTOCOL:', rackgroup.model, '\n')
        e = None

        if not code:
            return self.render_json_response({'status': 'ERROR.', 'remote_addr': remote_addr, 'message': 'Отсутствует код команды ..'}, status=201)

        try:
            response = str(send_command.apply_async((command.name, rackgroup.model, rackgroup.ip, rackgroup.port, code))) #**{'model': rackgroup.model})
            response = response.replace('-', '')
        except Exception as e:
            print("LOG: RUN ERROR: %s" % (e))
            response = str(e)

        print("LOG: RESPONSE: ", response, "\n")

        if e:
            return self.render_json_response({'status': 'ERROR.', 'remote_addr': remote_addr, 'message': response}, status=201)
        else:
            return self.render_json_response({'status': 'OK', 'remote_addr': remote_addr, 'uuid': response})


class RunMission(JSONResponseMixin, ListView):
    http_method_names = ['get']
    error_message = 'Bad request'

    def get(self, request, *args, **kwargs):
        remote_addr = get_remote_addr(request)
        mission_name = request.GET.get('mission_name', '')

        error = None
        response = ''

        try:
            response = run_mission_task(mission_name=mission_name, username=request.user, remote_addr=remote_addr)
        except Exception as e:
            error = True
            print("LOG: RUN_MISSION_ERROR: %s" % (e))
            response = str(e)

        if 'IN' == mission_name:
            url = reverse_lazy('storage:itemin')
        elif 'OUT' == mission_name or 'REMOVE' == mission_name:
            url = reverse_lazy('storage:itemout')
        else:
            url = '/'

        if error:
            return self.render_json_response({'status': 'ERROR.', 'remote_addr': remote_addr, 'message': response}, status=201)
        else:
            return self.render_json_response({'status': 'OK', 'remote_addr': remote_addr, 'uuid': response, 'redirect_url': url})

@app.task
def send_command(name, model, ip, port, code, **kwargs):
    print('\nLOG: NAME:', name, 'MODEL:', model, 'COMMAND:', code, '\n')

    host ='127.0.0.1'
    port_src = 5000
    #command = '\x0b\x00\x00\x00'
    #model = kwargs.get('model', None)

    if model == 1:
        try:
            result = send_message(dest=ip, port_dst=port, command=code)

            if 1 == result:
                return False
            error_variants = {'6': u'Некорректные параметры', '1': u'Неизвестная команда'}#, '3': u'Система занята'}

            if result in error_variants:
                return error_variants[result]
            return result
        except Exception as e:
            print("LOG: TCP RUN_COMMAND_ERROR:\n", e, "\n")
            return str(e)
    else:
        try:
            result = send_packet(host=str(host), port_src=int(port_src), dest=str(ip), port_dst=int(port), command=code)

            if 1 == result:
                return False
            error_variants = [254, 253, 251, 247, 239, 223, 191]

            if result in error_variants:
                return get_error_description(0, result)
            return result
            return False
        except Exception as e:
            print("LOG: UDP RUN_COMMAND_ERROR:", e, "\n")
            return str(e)


def run_mission_task(mission_name, username, remote_addr=None):
    user = DefaultUser.objects.filter(username=username).last()
    send_command = user.arcuser.send_command

    if not user:
        print("LOG: RUN_MISSION_TASK: USER_NOT_FOUND\n")
        raise Exception(u"Пользователь не найден")
        #return

    if mission_name == 'IN':
        STATE_START = 1
        STATE_END = 3
        RELEASED = False
        RELEASED_NAME = None
    elif mission_name == 'OUT' or mission_name == 'REMOVE':
        STATE_START = 3
        STATE_END = 1
        RELEASED = True
        RELEASED_NAME = None
    else:
        raise Exception(u"Неверное имя Задания")
        #return

    ########################
    # check mission and task

    missions = Mission.objects.filter(name=mission_name, status='inprogress', owner=user)

    if not missions or missions.count() < 1:
        print("LOG: RUN_MISSION_TASK: NO_MISSIONS_FOUND [%s; %s]\n" % (mission_name, user))
        raise Exception(u"Задание не найдено для пользователя '%s'" % user)
        #return
    mission = missions.last()

    #with transaction.atomic():
    if True:
        tasks = Task.objects.filter(mission=mission, status='inprogress').order_by('id')

        if not tasks or tasks.count() < 1:
            print("LOG: RUN_MISSION_TASK: NO_TASKS_FOUND\n")
            if mission.status != 'done':
                mission.status = 'done'
                mission.save(update_fields=['status'])
            raise Exception(u"Задач не найдено")
            #return

        print("LOG: RUN_MISSION_TASK: TASKS:", tasks, "SEND_COMMAND:", send_command, "\n")

        task = tasks.first()

        #if mission_name == 'OUT':
        RELEASED_NAME = task.owner

        ########################
        # block rack_group if required
        rack_groups = None

        if send_command and send_command != 0:
            rack_group_ids = []

            for slot in task.item.slot_set.filter(state=STATE_START):
                rack_group_ids.append(slot.shelf.section.row.rack.rack_group.id)

            rack_groups = RackGroup.objects.filter(Q(id__in=rack_group_ids, blocker=None) | Q(id__in=rack_group_ids, blocker=user))

            if len(rack_group_ids) < 1:
                raise Exception(u"Список Мобильных групп пуст")
                #return

            if not rack_groups or rack_groups.count() < 1:  
                print("LOG: RUN_MISSION_TASK: BLOCKED\n")
                blocked_rack_groups = RackGroup.objects.filter(id__in=rack_group_ids)
                raise Exception(u"Мобильная группа %s заблокирована" % (blocked_rack_groups))
                #return

            for rack_group in rack_groups:
                try:
                    rack_group.blocker = user
                    rack_group.save(update_fields = ['blocker'])
                except Exception as e:
                    print("LOG: RUN_MISSION_TASK: ERROR: 1 %s\n" % (e))
                print (u'LOG: Мобильная группа %s заблокирована' % (rack_group))

            rackgroup_id = slot.shelf.section.row.rack.rack_group.id
            row_id = slot.shelf.section.row.get_pass_number()
            prepare_command('open', rackgroup_id, row_id, remote_addr)

        ########################
        # update slot status

        slots = Slot.objects.filter(item=task.item).exclude(state=STATE_START)

        for slot in slots:
            print("LOG: RUN_MISSION_TASK: RESTORE SLOT BEFORE:", slot, "\n")
            try:
                slot.state = 0
                slot.item = None
                slot.save(update_fields=['item', 'state'])
            except:
                pass
            print("LOG: RUN_MISSION_TASK: RESTORE SLOT:", slot, "\n")

        for slot in task.item.slot_set.filter(state=STATE_START):
            print("LOG: RUN_MISSION_TASK: SLOT BEFORE:", slot, "\n")
            try:
                if mission_name == 'REMOVE':
                    slot.item = None
                else:
                    slot.item = task.item
                slot.state = STATE_END
                slot.save(update_fields=['item', 'state'])
            except:
                pass
            print("LOG: RUN_MISSION_TASK: SLOT:", slot, "\n")

        ########################
        # update task

        if mission_name == 'REMOVE':
            data = model_to_dict(task.item, exclude=['id', 'owner', 'search', 'created'])
            data['rid'] = task.item.id
            data['created'] = task.item.created
            data['item_type'] = task.item.item_type or None
            print("LOG: DICT: %s\n" % (data))
            item_removed = ItemRemoved.objects.create(**data)
            task.item.delete()
        else:
            try:
                task.item.released = RELEASED
                task.item.owner = RELEASED_NAME
                task.item.search = ''
                task.item.save(update_fields=['released', 'search'])
            except:
                pass

        try:
            task.status = 'done'
            task.save(update_fields=['status'])
        except Exception as e:
            print("LOG: RUN_MISSION_TASK: ERROR: 2 %s\n" % (e))

        print("LOG: RUN_MISSION_TASK: TASK_DONE:", task, "\n")

        if tasks.count() == 1:
            try:
                mission.status = 'done'
                mission.save(update_fields=['status'])
            except Exception as e:
                print("LOG: RUN_MISSION_TASK: ERROR: 3 %s\n" % (e))

        ########################
        # unblock rack_group

            if send_command and send_command != 0 and rack_groups:
                for rack_group in rack_groups:
                    try:
                        rack_group.blocker = None
                        rack_group.save(update_fields=['blocker'])
                    except Exception as e:
                        print("LOG: RUN_MISSION_TASK: ERROR: 4 %s\n" % (e))

            print("LOG: RUN_MISSION_TASK: MISSION DONE\n")

        return 'OK'

