# coding: utf-8
from django.conf.urls import include, url
from django.views.generic import TemplateView
from .tasks import RunMission, Status, Execute, ExecuteSync
from archive.storage.views import InTaskDelete, TaskDelete

urlpatterns = (
    url(r'^sync/mission/$', RunMission.as_view(), name="run_mission"),
    url(r'^sync/(?P<command>[a-zA-Z]+)/(?P<key>[0-9])/$', ExecuteSync.as_view(), name="execute_sync"),
    url(r'^async/(?P<command>[a-zA-Z]+)/(?P<key>[0-9])/$', Execute.as_view(), name="execute"),
    url(r'^task/delete/(?P<pk>\d+)/$',
        TaskDelete.as_view(), name='task_delete'),
    url(r'^task/(?P<uuid>[a-f0-9]+)/$', Status.as_view(), name="status"),
    url(r'^intask/delete/(?P<pk>\d+)/$',
        InTaskDelete.as_view(), name='intask_delete'),
)
