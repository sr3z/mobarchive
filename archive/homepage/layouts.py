# coding: utf-8
from crispy_forms.layout import Row, Div, Field


class FormField(Field):
    template = 'homepage/field.html'


class SingleColumnRow(Row):

    def __init__(self, field, **kwargs):
        super(SingleColumnRow, self).__init__(FormField(field), **kwargs)


class TwoColumnRow(Row):

    def __init__(self, first=None, second=None, **kwargs):
        fields = []
        if first is not None:
            fields.append(Div(FormField(first), css_class="left-col"))
        if second is not None:
            fields.append(Div(FormField(second), css_class="right-col"))
        super(TwoColumnRow, self).__init__(*fields, **kwargs)
