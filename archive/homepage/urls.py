# coding: utf-8
from django.conf.urls import include, url
from django.views.generic import TemplateView

from archive.homepage.views import HomepageView

urlpatterns = (
    url(r'^$', HomepageView.as_view(), name='homepage'),
)

urlpatterns += (
    url(r'^', include('archive.users.urls')),
)
