# -*- coding: utf-8 -*-

from django.contrib.auth import logout, login
from django.views.generic import ListView, DetailView
from django.contrib.auth.models import Permission, User
from django.contrib.contenttypes.models import ContentType

from archive.storage.models import Item, ItemType
from archive.users.models import ArcUser

class HomepageView(ListView):
    model = ArcUser
    template_name = 'homepage/homepage.html'
    success_url = "/users/"

    def __init__(self, **kwargs):
        super(HomepageView, self).__init__(**kwargs)

    def get_queryset(self):
        result = super(HomepageView, self).get_queryset()
        user = self.request.user
        print ("LOG: HOMEPAGE: USER", user)

        '''
        if user.username == u'администратор':
            print("LOG: HOMEPAGE: item_type.edit", user.has_perm('storage.item_type.edit'))
            print("LOG: HOMEPAGE: item.edit", user.has_perm('storage.item.edit'))

            if not hasattr(user.user_permissions, 'add'):
                return result

            content_type = ContentType.objects.get_for_model(ItemType)
            permission = Permission.objects.get(
                                        codename='item_type.edit',
                                        content_type=content_type,
            )
            print("LOG: HOMEPAGE: permission", permission)
            user.user_permissions.add(permission)

            content_type = ContentType.objects.get_for_model(Item)
            permission = Permission.objects.get(
                                        codename='item.edit',
                                        content_type=content_type,
            )
            print("LOG: HOMEPAGE: permission", permission)
            user.user_permissions.add(permission)

            content_type = ContentType.objects.get_for_model(ArcUser)
            permission = Permission.objects.get(
                                        codename='user.edit',
                                        content_type=content_type,
            )
            print("LOG: HOMEPAGE: permission", permission)
            user.user_permissions.add(permission)

            print("LOG: HOMEPAGE: item_type.edit", user.has_perm('storage.item_type.edit'))
            print("LOG: HOMEPAGE: item_type.delete", user.has_perm('storage.item_type.delete'))
            print("LOG: HOMEPAGE: item.edit", user.has_perm('storage.item.edit'))
            print("LOG: HOMEPAGE: user.edit", user.has_perm('users.user.edit'))
        '''

        return result
