# -*- coding: utf-8 -*-

from django.db import models

from django.contrib.auth.models import User as DefaultUser, Group
from archive.utils.models import TimeStampedModelIndexed

class Report(TimeStampedModelIndexed):
    user = models.ForeignKey(DefaultUser)
    fields = models.TextField(null=True, blank=True)
    remove_fields = models.TextField(null=True, blank=True)
