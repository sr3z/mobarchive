# coding: utf-8

from django.contrib.auth.views import logout, login
from django.contrib import admin
from django.conf.urls import include, url
from .views import OutTasksPDF, InTasksPDF, StorageItemsPDF, RemovableItemsPDF, StorageItemsReport, RemovableItemsReport

urlpatterns = (
    url(r'^removable_items/$', RemovableItemsReport.as_view(), name='rem-items'),
    url(r'^storage_items/$', StorageItemsReport.as_view(), name='storage-items'),
    url(r'^removable_items_pdf/$', RemovableItemsPDF.as_view(), name='removable-items-pdf'),
    url(r'^storage_items_pdf/$', StorageItemsPDF.as_view(), name='storage-items-pdf'),
    url(r'^intasks_pdf/$', InTasksPDF.as_view(), name='intasks_pdf'),
    url(r'^outtasks_pdf/$', OutTasksPDF.as_view(), name='outtasks_pdf'),
)
