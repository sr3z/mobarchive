# -*- coding: utf-8 -*-

import inspect
import locale
from braces.views import LoginRequiredMixin
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, TemplateView
from django.http import FileResponse, HttpResponse, HttpResponseRedirect
from django.views import View
from django.contrib.auth.models import Permission, User as DefaultUser, Group
from datetime import datetime
from django.db import transaction

from wkhtmltopdf.views import PDFTemplateView

from archive.reports.models import Report
from archive.api.models import Task
from archive.storage.models import Settings, RackGroup, ItemType, Department, RetentionPolicy, Item
from archive.storage.views import BaseStorage
from archive.utils.decorators import allow_access, set_created, set_modified
from archive.api.models import Task, Mission

from io import StringIO
from xhtml2pdf import pisa
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
from cgi import escape
from django.shortcuts import render
from django.template.loader import render_to_string


def render_to_pdf(rendered_html_name):

    with open(rendered_html_name + '.html', "r") as html_file:
        content = html_file.read()
        pdfFile = open(rendered_html_name + '.pdf', "wb")
        pdf = pisa.CreatePDF(StringIO(content), pdfFile, encoding='UTF-8', show_error_as_pdf=True)
        pdfFile.close()
        #pdf = pisa.CreatePDF(StringIO(content), pdf_file, encoding='UTF-8')
    return open(rendered_html_name + '.pdf', "rb")


class StorageItemsReport(BaseStorage, ListView):
    paginate_by = 10
    model = Item
    template_name = 'reports/storage_items.html'
    methods = ['get', 'post']
    default_fields = ['created', 'fiscal_year', 'retention_policy', 'item_type', 'department', 'rack_group', 'released_to', 'owner', 'created_by', 'description', 'name', 'client', 'number']
    contains_fields = ['created', 'fiscal_year', 'description', 'name', 'number']

    def __init__(self, **kwargs):
        super(StorageItemsReport, self).__init__(**kwargs)
        settings = Settings.objects.all().first()
        try:
            self.paginate_by = int(settings.paginate_by)
        except:
            pass

    def get_queryset(self):
        queryset = super(self.__class__, self).get_queryset().order_by('number', 'id')
        for q in self.request.GET:
            if 'topdf' == q:
                continue
            if 'page' == q:
                continue
            value = self.request.GET.get(q, None)
            filters = {}

            if value:
                if q in self.contains_fields:
                    filters[q + '__contains'] = value
                else:
                    if 'rack_group' == q:
                        q = 'slot__shelf__section__row__rack__rack_group'
                    elif 'scope' == q:
                        if value == u'На хранении':
                            q = 'released'
                            value = False
                        elif value == u'Выданные':
                            q = 'released'
                            value = True
                        else:
                            continue
                    elif 'released_to' == q:
                        q = 'task__owner__id'
                        filters['task__mission__name'] = 'OUT'
                        filters['released'] = True
                    filters[q] = value
                print("LOG: %s QUERYSET FILTER: %s" % (self.__class__, filters))
            queryset = queryset.filter(**filters)
        return queryset

    def get_objects_list(self, model):
        obj_queryset = model.objects.all()

        if obj_queryset.count() > 0:
            self.obj_list = [(str(obj.id), obj) for obj in obj_queryset]
            self.obj_list.insert(0, (-1, '-------'))
        else:
            self.obj_list = []
        self.object_list = self.obj_list
        return self.obj_list

    def post(self, request, *args, **kwargs):
        print("LOG: %s POST: %s" % (self.__class__, request.POST))
        reports = Report.objects.filter(user=request.user)
        report = None
        filters = {}

        if not reports or not reports.exists():
            my_fields = self.default_fields

            try:
                report = Report.objects.create(user=request.user, fields=my_fields)
            except Exception as e:
                print("LOG: %s POST CREATE ERROR: %s" % (self.__class__, e))
                return HttpResponseRedirect(self.get_success_url())
        else:
            report = reports.first()

            if report.fields and len(report.fields) > 2:
                try:
                    my_fields = report.fields.split(',')

                    for item in my_fields:
                        if item not in self.default_fields:
                            my_fields.remove(item)
                except Exception as e:
                    my_fields = self.default_fields
                    print("LOG: %s POST FIELDS ERROR: %s" % (self.__class__, e))
            else:
                my_fields = []

        if 'print_pdf' in request.POST:
            # TODO: refactor this to function
            for name in self.request.POST:
                if 'val_' in name:
                    field = name.replace('val_', '')
                    value = self.request.POST.get(name, None)

                    if not value or len(value) == 0 or value == '-1':
                        pass
                    else:
                        filters[field] = value

            return HttpResponseRedirect(self.get_success_url(filters, to_pdf=True))

            content = render_to_string(self.template_name, self.get_context_data())
            with open('_files/storage_report.html', "wb") as static_file:
                static_file.write(content.encode('UTF-8'))

            return FileResponse(render_to_pdf(
                                render(request, self.template_name, self.get_context_data()),#, encoding='UTF-8'),
                                user=request.user,
                                ), content_type='application/pdf')

        if 'cleanall' in request.POST:
            request.POST = {}

        if 'del_' in str(request.POST):
            for name in request.POST:
                #print("LOG: name:", name)
                if 'del_' in str(name):
                    #print("LOG: del name:", name)
                    field = str(name).replace('del_', '')
                    #print("LOG: del name:", field)

                    if field and field in my_fields:
                        my_fields.remove(field)
                    break
        elif 'add_field' in request.POST and request.POST.get('add_field', None) != '':
            field = request.POST.get('add_field', None)
            #print("LOG: %s CONTEXT FIELDS: %s FIELD: %s" % (self.__class__, my_fields, field))

            if field and not field in my_fields:
                my_fields.append(field)
        else:
            for name in self.request.POST:
                if 'val_' in name:
                    field = name.replace('val_', '')
                    value = self.request.POST.get(name, None)

                    if not value or len(value) == 0 or value == '-1':
                        pass
                    else:
                        filters[field] = value
            print("LOG: %s CONTEXT FILTERS: %s" % (self.__class__, filters))

        if report and not report.fields == ','.join(my_fields):
            report.fields = ','.join(my_fields)
            report.save(update_fields=['fields'])

        return HttpResponseRedirect(self.get_success_url(filters))

    def get_context_data(self, **kwargs):
        try:
            context = super(StorageItemsReport, self).get_context_data(**kwargs)
        except Exception as e:
            print(e)
            context = {}

        if self.request.GET.get('topdf'):
            context['REPORT'] = True
        reports = Report.objects.filter(user=self.request.user)

        if not reports or not reports.exists():
            my_fields = self.default_fields
        else:
            if reports.first().fields and len(reports.first().fields) > 2:
                try:
                    my_fields = reports.first().fields.split(',')
                except Exception as e:
                    print("LOG: %s CONTEXT_FIELDS ERROR: %s" % (self.__class__, e))
                    my_fields = self.default_fields
            else:
                my_fields = []
        print("LOG: %s CONTEXT_FIELDS: %s" % (self.__class__, my_fields))

        context['fields'] = my_fields
        context['departments'] = self.get_objects_list(Department)
        context['rack_groups'] = self.get_objects_list(RackGroup)
        context['item_types'] = self.get_objects_list(ItemType)
        context['pers'] = self.get_objects_list(DefaultUser)
        context['policies'] = self.get_objects_list(RetentionPolicy)

        return context

    def get_success_url(self, filters={}, to_pdf=False):
        query_string = ''

        for k in filters:
            if len(query_string) > 1:
                query_string = query_string + '&' + k + '=' + filters[k]
            else:
                query_string = k + '=' + filters[k]
        if to_pdf:
            query_string = query_string + '&' + 'topdf=true'
            return reverse_lazy('reports:storage-items-pdf') + '?' + query_string
        print("LOG: %s FILTER: %s" % (self.__class__, query_string))

        return reverse_lazy('reports:storage-items') + '?' + query_string

    @set_created
    def form_valid(self, form):
        try:
            response = super(self.__class__, self).form_valid(form)
        except Exception as e:
            print("LOG: %s FORM ERROR: %s" % (self.__class__, e))
            return HttpResponseRedirect(self.get_success_url())
        return response


class RemovableItemsReport(BaseStorage, ListView):
    paginate_by = 10
    model = Item
    template_name = 'reports/removable_items.html'
    default_fields = ['created', 'retention_policy', 'item_type', 'department', 'rack_group', 'owner', 'created_by', 'name', 'number', 'retention_date']
    contains_fields = ['created', 'name', 'number', 'retention_date']

    def __init__(self, **kwargs):
        super(RemovableItemsReport, self).__init__(**kwargs)
        settings = Settings.objects.all().first()
        try:
            self.paginate_by = int(settings.paginate_by)
        except:
            pass

    def get_queryset(self):
        if None == self.request.GET.get('datepicker', None):
            return super(RemovableItemsReport, self).get_queryset().none()
        queryset = super(RemovableItemsReport, self).get_queryset().order_by('number', 'id')

        for q in self.request.GET:
            if 'topdf' == q:
                continue

            if 'page' == q:
                continue
            value = self.request.GET.get(q, None)
            filters = {}

            if value:
                if q in self.contains_fields:
                    filters[q + '__contains'] = value
                else:
                    if 'rack_group' == q:
                        q = 'slot__shelf__section__row__rack__rack_group'
                    elif 'scope' == q:
                        if value == u'На хранении':
                            q = 'released'
                            value = 'false'
                        elif value == u'Выданные':
                            q = 'released'
                            value = 'true'
                    elif 'datepicker' == q:
                        q = 'combined_date__lte'
                        datetime_object = datetime.strptime(value, '%d.%m.%Y')
                        value = datetime_object.strftime('%Y-%m-%d')
                    elif 'released_to' == q:
                        q = 'task__owner__id'
                        filters['task__mission__name'] = 'OUT'
                        filters['released'] = True
                    filters[q] = value
                print("LOG: %s QUERYSET FILTER: %s" % (self.__class__, filters))
            queryset = queryset.filter(**filters)
        return queryset

    def get_objects_list(self, model):
        obj_queryset = model.objects.all()

        if obj_queryset.count() > 0:
            self.obj_list = [(str(obj.id), obj) for obj in obj_queryset]
            self.obj_list.insert(0, (-1, '-------'))
        else:
            self.obj_list = []
        self.object_list = self.obj_list
        return self.obj_list

    def post(self, request, *args, **kwargs):
        print("LOG: %s %s %s" % (self.__class__, inspect.stack()[0][3].upper(), request.POST))

        ###########################################################################################################
        if 'send_delete' in request.POST:
            missions = Mission.objects.filter(name='REMOVE', status='inprogress', owner=self.request.user)

            if missions and missions.count() > 0:
                for mission in missions:
                    if Task.objects.filter(mission=mission, status='inprogress').count() > 0:
                        return HttpResponseRedirect(self.get_success_url(error_message=u'Операция невозможна при наличии незавершенного задания'))
                    else:
                        mission.status = 'done'
                        mission.save(update_fields=['status'])
            idx = []
            idx_str = request.POST.get('idx_list', None)
            if idx_str:
                idx = idx_str.split(',')

            items = Item.objects.filter(id__in=idx)

            if not items or items.count() < 1:
                print("LOG: %s %s IDX: %s" % (self.__class__, inspect.stack()[0][3].upper(), idx))
                return HttpResponseRedirect(self.get_success_url(error_message=u'ЕХ не найдены'))
            status = 'inprogress'

            with transaction.atomic():
                mission = Mission.objects.create(name='REMOVE', owner=request.user, status='inprogress')

                for item in items:
                    task = Task.objects.create(mission=mission, item=item, status=status, owner_id=request.user.id, description=u'Уничтожение') 

            return HttpResponseRedirect(reverse_lazy('storage:itemout') + '?remove_item=true')
        ###########################################################################################################


        reports = Report.objects.filter(user=request.user)
        report = None
        filters = {}

        if not reports or not reports.exists():
            my_fields = self.default_fields

            try:
                report = Report.objects.create(user=request.user, fields=my_fields)
            except Exception as e:
                print("LOG: %s POST CREATE ERROR: %s" % (self.__class__, e))
                return HttpResponseRedirect(self.get_success_url())
        else:
            report = reports.first()

            if report.remove_fields and len(report.remove_fields) > 2:
                try:
                    my_fields = report.remove_fields.split(',')

                    for item in my_fields:
                        if item not in self.default_fields:
                            my_fields.remove(item)
                except Exception as e:
                    my_fields = self.default_fields
                    print("LOG: %s POST FIELDS ERROR: %s" % (self.__class__, e))
            else:
                my_fields = []

        if 'print_pdf' in request.POST:
            # TODO: refactor this to function
            for name in self.request.POST:
                if 'val_' in name:
                    field = name.replace('val_', '')
                    value = self.request.POST.get(name, None)

                    if not value or len(value) == 0 or value == '-1':
                        pass
                    else:
                        filters[field] = value

            return HttpResponseRedirect(self.get_success_url(filters, to_pdf=True))


        if 'cleanall' in request.POST:
            request.POST = {}

        if 'del_' in str(request.POST):
            for name in request.POST:
                #print("LOG: name:", name)
                if 'del_' in str(name):
                    #print("LOG: del name:", name)
                    field = str(name).replace('del_', '')
                    #print("LOG: del name:", field)

                    if field and field in my_fields:
                        my_fields.remove(field)
                    break
        elif 'add_field' in request.POST and request.POST.get('add_field', None) != '':
            field = request.POST.get('add_field', None)
            #print("LOG: %s CONTEXT FIELDS: %s FIELD: %s" % (self.__class__, my_fields, field))

            if field and not field in my_fields:
                my_fields.append(field)
        else:
            for name in self.request.POST:
                if 'val_' in name:
                    field = name.replace('val_', '')
                    value = self.request.POST.get(name, None)

                    if not value or len(value) == 0 or value == '-1':
                        pass
                    else:
                        filters[field] = value
            print("LOG: %s CONTEXT FILTERS: %s" % (self.__class__, filters))

        if report and not report.remove_fields == ','.join(my_fields):
            report.remove_fields = ','.join(my_fields)
            report.save(update_fields=['remove_fields'])

        return HttpResponseRedirect(self.get_success_url(filters))

    def get_context_data(self, **kwargs):
        context = super(RemovableItemsReport, self).get_context_data(**kwargs)
        context['error_message'] = self.request.GET.get('error', '')
        reports = Report.objects.filter(user=self.request.user)

        if not reports or not reports.exists():
            my_fields = self.default_fields
        else:
            if reports.first().remove_fields and len(reports.first().remove_fields) > 2:
                try:
                    my_fields = reports.first().remove_fields.split(',')
                except Exception as e:
                    print("LOG: %s CONTEXT_FIELDS ERROR: %s" % (self.__class__, e))
                    my_fields = self.default_fields
            else:
                my_fields = []
        print("LOG: %s CONTEXT_FIELDS: %s" % (self.__class__, my_fields))

        context['fields'] = my_fields
        context['departments'] = self.get_objects_list(Department)
        context['rack_groups'] = self.get_objects_list(RackGroup)
        context['item_types'] = self.get_objects_list(ItemType)
        context['pers'] = self.get_objects_list(DefaultUser)
        context['policies'] = self.get_objects_list(RetentionPolicy)
        objects = self.get_queryset().values('id')
        idx_list = []
        for obj in objects:
            idx_list.append(str(obj['id']))
        context['idx_list'] = ','.join(idx_list)

        return context

    def get_success_url(self, filters={}, error_message=None,to_pdf=False):
        query_string = ''

        for k in filters:
            if len(query_string) > 1:
                query_string = query_string + '&' + k + '=' + filters[k]
            else:
                query_string = k + '=' + filters[k]

        if to_pdf:
            query_string = query_string + '&' + 'topdf=true'
            return reverse_lazy('reports:removable-items-pdf') + '?' + query_string

        if error_message:
                query_string = query_string + '&error=' + error_message
        print("LOG: %s FILTER: %s" % (self.__class__, query_string))

        return reverse_lazy('reports:rem-items') + '?' + query_string

class RemovableItemsPDF(RemovableItemsReport, PDFTemplateView):
    """Class to generate PDF for removable items report"""

    object_list = []
    filename = 'removable_items_report.pdf'
    template_name = 'removable_items_report_pdf.html'
    show_content_in_browser = True
    cmd_options = {
        'zoom': 3,
    }

    def get_context_data(self, **kwargs):
        qs = Item.objects.none()
        if None != self.request.GET.get('datepicker', None):
            qs = Item.objects.all()
        context = super(RemovableItemsPDF, self).get_context_data()
        filters = {}

        for q in self.request.GET:
            if 'topdf' == q:
                continue

            if 'page' == q:
                continue
            value = self.request.GET.get(q, None)


            if value:
                if q in self.contains_fields:
                    filters[q + '__contains'] = value
                else:
                    if 'rack_group' == q:
                        q = 'slot__shelf__section__row__rack__rack_group'
                    elif 'scope' == q:
                        if value == u'На хранении':
                            q = 'released'
                            value = 'false'
                        elif value == u'Выданные':
                            q = 'released'
                            value = 'true'
                    elif 'datepicker' == q:
                        q = 'combined_date__lte'
                        datetime_object = datetime.strptime(value, '%d.%m.%Y')
                        value = datetime_object.strftime('%Y-%m-%d')
                    elif 'released_to' == q:
                        q = 'task__owner__id'
                        filters['task__mission__name'] = 'OUT'
                        filters['released'] = True
                    filters[q] = value
                print("LOG: %s QUERYSET FILTER: %s" % (self.__class__, filters))
        queryset = qs.filter(**filters)


        context['object_list'] = queryset.all()

        locale.setlocale(locale.LC_ALL, 'ru_RU.utf8')
        context['report_date'] = datetime.strftime(datetime.now(), '%c')
        context['report_input_date'] = self.request.GET.get('datepicker', None)

        context['report_user'] = self.request.user.username
        return context


class StorageItemsPDF(StorageItemsReport, PDFTemplateView):
    """Class to generate PDF for storage items report"""

    filename = 'storage_items_report.pdf'
    template_name = 'storage_items_report_pdf.html'
    show_content_in_browser = True
    cmd_options = {
        'zoom': 3,
    }

    def get_context_data(self, **kwargs):
        qs = Item.objects.all()
        context = super(StorageItemsPDF, self).get_context_data()
        filters = {}

        for q in self.request.GET:
            if 'topdf' == q:
                continue
            if 'page' == q:
                continue
            if 'datepicker' == q:
                continue
            value = self.request.GET.get(q, None)

            if value:
                if q in self.contains_fields:
                    filters[q + '__contains'] = value
                else:
                    if 'rack_group' == q:
                        q = 'slot__shelf__section__row__rack__rack_group'
                    elif 'scope' == q:
                        if value == u'На хранении':
                            q = 'released'
                            value = False
                        elif value == u'Выданные':
                            q = 'released'
                            value = True
                        else:
                            continue
                    elif 'released_to' == q:
                        q = 'task__owner__id'
                        filters['task__mission__name'] = 'OUT'
                        filters['released'] = True
                    filters[q] = value
                print("LOG: %s QUERYSET FILTER: %s" % (self.__class__, filters))
        queryset = qs.filter(**filters)

        context['object_list'] = queryset.all()
        context['report_input_date'] = self.request.GET.get('datepicker', None)

        locale.setlocale(locale.LC_ALL, 'ru_RU.utf8')
        context['report_date'] = datetime.strftime(datetime.now(), '%c')
        context['report_user'] = self.request.user.username

        locale.setlocale(locale.LC_ALL, 'ru_RU.utf8')
        return context

class InTasksPDF(PDFTemplateView):
    model = Mission
    filename = 'intasks.pdf'
    template_name = 'intasks_pdf.html'
    show_content_in_browser = True
    cmd_options = {
        'zoom': 3,
    }
    def get_queryset(self):
        return Mission.objects.filter(name__in=['IN'], status='inprogress', owner=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data()
        context['object_list'] = self.get_queryset()
        context['report_date'] = datetime.strftime(datetime.now(), '%c')
        context['report_user'] = self.request.user.username

        locale.setlocale(locale.LC_ALL, 'ru_RU.utf8')
        return context

class OutTasksPDF(PDFTemplateView):
    model = Mission
    filename = 'outtasks.pdf'
    template_name = 'outtasks_pdf.html'
    show_content_in_browser = True
    cmd_options = {
        'zoom': 3,
    }
    def get_queryset(self):
        return Mission.objects.filter(name__in=['OUT', 'REMOVE'], status='inprogress', owner=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data()
        context['object_list'] = self.get_queryset()
        context['report_date'] = datetime.strftime(datetime.now(), '%c')
        context['report_user'] = self.request.user.username

        locale.setlocale(locale.LC_ALL, 'ru_RU.utf8')
        return context

