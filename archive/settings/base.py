import os
import sys
import json

from unipath import FSPath as Path

PROJECT_ROOT = Path(__file__).absolute().ancestor(2)
print("PROJECT_ROOT", PROJECT_ROOT)
BASE_DIR = Path(os.path.dirname(os.path.dirname(os.path.dirname(Path(__file__)))))
print("BASE_DIR", BASE_DIR)

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.10/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '1yi#v27=+2+hs!o$_x$_^=pa=hef%a081uow$2&_a!0ov*=(2o'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
DEBUG_MESSAGES = False

STATIC_FILES = BASE_DIR.child('_files')
print("STATIC_FILES", STATIC_FILES)
STATIC_URL = '/f/'
STATIC_ROOT = BASE_DIR.child('_collected_files')

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "_files"),
    '/var/www/static/',
]
JS_BUNDLES = [
    {
        'filename': 'bundle.js',
        'files': ['underscore-min.js', 'angular.min.js', 'ui-utils.min.js', 'ui-bootstrap-custom-tpls-0.10.0.min.js']
    },
    {
        'filename': 'ie_bundle.js',
        'files': ['json2.js', 'spike.min.js', 'es5-shim.min.js', 'underscore-min.js',
        'angular.min.js', 'ui-utils.min.js', 'ui-bootstrap-custom-tpls-0.10.0.min.js']
    }
]

FILE_BUNDLES = {
    'js': {
        'files_root': STATIC_FILES.child('js'),
        'bundles_root': STATIC_FILES.child('js'),
        'bundles': JS_BUNDLES,
        'delimiter': '\n;'
    },
}
ALLOWED_HOSTS = []

PROJECT_APPS = [
    'archive.storage',
    'archive.users',
    'archive.api',
    'archive.utils',
    'archive.homepage',
    'archive.reports',
]

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_extensions',

    'crispy_forms',
    'corsheaders',
    #'django_activeurl',
    'django_filters',
    'registration',
    'pagination_ru',
    'celery',
    'wkhtmltopdf',
] + PROJECT_APPS 

SOUTH_TESTS_MIGRATE = False
CORS_ORIGIN_ALLOW_ALL = True  # CROSS-ORIGIN RESOURCE SHARING

MIDDLEWARE_CLASSES = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'corsheaders.middleware.CorsMiddleware',
]

CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = 'Europe/Moscow'
BROKER_URL = 'redis://localhost:6379'

ROOT_URLCONF = 'archive.urls'
LOGIN_URL = '/login/'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'archive.utils.license.license',
            ],
        },
    },
]

WSGI_APPLICATION = 'archive.wsgi.application'

'''
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
'''

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "HOST": '127.0.0.1',
        "NAME": "archive",
        "USER": "postgres",
        "PASSWORD": "postgres",
    },
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

ACCOUNT_ACTIVATION_DAYS=1
LOGIN_REDIRECT_URL='/'
GROUPS = [
    "Administrator",
    "User",
    "Archivarius",
    "Guest"
]


TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.contrib.messages.context_processors.messages",
)

LANGUAGE_CODE = 'ru-RU'
TIME_ZONE = 'Europe/Moscow'
USE_I18N = True
USE_L10N = True
USE_TZ = True

EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'

USE_ELASTIC=False
#USE_ELASTIC=True

if USE_ELASTIC:
    HAYSTACK_CONNECTIONS = {
        'default': {
            'ENGINE': 'haystack_elasticsearch.elasticsearch5.Elasticsearch5SearchEngine',
            'URL': 'http://127.0.0.1:9200/',
            'INDEX_NAME': 'haystack',
        },
    }

    HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'

    INSTALLED_APPS += ('haystack',)

LOCAL_SETTINGS = {}

try:
    with open(PROJECT_ROOT.ancestor(2).child('archive_local_settings.json')) as handle:
        LOCAL_SETTINGS = json.load(handle)
        print('LOG: LOCAL_SETTINGS: %s' % (LOCAL_SETTINGS))

        if 'USE_ELASTIC' in LOCAL_SETTINGS:
            USE_ELASTIC = LOCAL_SETTINGS.get('USE_ELASTIC', False)
        if 'mode' in LOCAL_SETTINGS and LOCAL_SETTINGS.get('mode', None) == 'DEBUG':
            DEBUG_MESSAGES = True
        if 'WKHTMLTOPDF_CMD' in LOCAL_SETTINGS:
            WKHTMLTOPDF_CMD = LOCAL_SETTINGS.get('WKHTMLTOPDF_CMD')
        else:
            WKHTMLTOPDF_CMD = '/usr/bin/wkhtmltopdf'

except Exception as e:
    #print('LOG: LOCAL_SETTINGS ERROR: %s' % (e))
    pass
