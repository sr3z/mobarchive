
import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'archive.settings.base')

app = Celery('archive')
#app.config_from_object('django.conf:settings', namespace='CELERY')
#app.autodiscover_tasks()
app.autodiscover_tasks(lambda: [n.name for n in apps.get_app_configs()])

@app.task(bind=True)
def debug_task(self):
        print(('Request: {0!r}'.format(self.request)))
