# coding: utf-8
from django import forms
from django.utils.translation import ugettext_lazy as _

from archive.users.models import License, ArcUser
from .models import Command, Department, Document, DocumentType,\
                    Item, ItemType, Rack, RackGroup, \
                    Row, Section, Shelf, Client, ClientType, RetentionPolicy,\
                    Settings, RACK_ROLES, PERIOD_COLOR, POSITION

class RackGroupForm(forms.ModelForm):
    enabled = forms.ChoiceField(label=u'Состояние',
        choices=[(True, u'Активен'), (False, u'Не активен')]
    )

    def __init__(self, *args, **kwargs):
        super(RackGroupForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({'style' : 'width: 50%;'})
        for field in self.fields:
            self.fields[field].required = False

    def clean_name(self):
        name = self.cleaned_data['name']
        if not name:
            raise forms.ValidationError(u'Поле обязательное')
        return name

    def clean_ip(self):
        ip = self.cleaned_data['ip']
        if not ip:
            raise forms.ValidationError(u'Поле обязательное')
        return ip

    def clean_port(self):
        port = self.cleaned_data['port']
        if not port:
            raise forms.ValidationError(u'Поле обязательное')
        return port

    class Meta:
        model = RackGroup
        exclude = ('groups', 'blocker', 'temperature_min', 'temperature_max', 'humidity_min', 'humidity_max', \
                'created_by', 'modified_by', 'move_duration', 'search')


class RowForm(forms.ModelForm):
    row_name = forms.CharField(label=_(u'Наименование ряда'))

    rack_group = forms.ModelChoiceField(
        queryset = RackGroup.objects.order_by('name'),
        label=u'Мобильная группа'
    )
    unit = forms.IntegerField(label=_(u'Номер стеллажа'))

    position = forms.ChoiceField(label=_(u'Сторона стеллажа'),
        choices=[r for r in POSITION],
    )
    section_count = forms.IntegerField(label=_(u'Количество секций в ряду'))
    shelf_count = forms.IntegerField(label=_(u'Количество полок в секции'))

    enabled = forms.ChoiceField(label=u'Состояние',
        choices=[(True, u'Активен'), (False, u'Не активен')]
    )

    role = forms.ChoiceField(label=_(u'Тип стеллажа'),
        choices=[r for r in RACK_ROLES],
    )

    def __init__(self, *args, **kwargs):
        super(RowForm, self).__init__(*args, **kwargs)
        self.fields['row_name'].widget.attrs.update({'style' : 'width: 50%;'})

        if self.instance and self.instance.pk:
            self.fields['row_name'].initial = self.instance.description
            self.fields['rack_group'].initial = self.instance.rack.rack_group
            self.fields['unit'].initial = self.instance.rack.unit
            self.fields['section_count'].initial = self.instance.section_set.count
            self.fields['shelf_count'].initial = self.instance.section_set.first().shelf_set.count

        for field in self.fields:
            self.fields[field].required = False

    def clean_row_name(self):
        row_name = self.cleaned_data['row_name']
        if not row_name:
            raise forms.ValidationError(u'Поле обязательное')
        else:
            return row_name

    def clean_rack_group(self):
        field = self.cleaned_data['rack_group']
        if field == None:
            raise forms.ValidationError(u'Поле обязательное')
        else:
            return field

    def clean_unit(self):
        unit = self.cleaned_data['unit']
        if not unit:
            raise forms.ValidationError(u'Поле обязательное')
        else:
            return unit

    def clean_role(self):
        role = self.cleaned_data['role']
        if role == None:
            print ("LOG: RackForm ERROR: ROLE:", role)
            raise forms.ValidationError(u'Поле обязательное')
        else:
            return role

    def clean_position(self):
        position = self.cleaned_data['position']
        if not position:# == None:
            print ("LOG: RackForm ERROR: POSITION:", position)
            raise forms.ValidationError(u'Поле обязательное')
        else:
            return position

    def clean_section_count(self):
        field = self.cleaned_data['section_count']
        if field == None:
            raise forms.ValidationError(u'Поле обязательное')
        else:
            return field

    def clean_shelf_count(self):
        field = self.cleaned_data['shelf_count']
        if field == None:
            raise forms.ValidationError(u'Поле обязательное')
        else:
            return field

    #def clean(self):
    #    super(RowForm, self).clean()
    #    user = self.initial.get('user', None)

    #    if not user:
    #        error_message = 'Роль пользователя None не позволяет редактировать запись'
    #        raise forms.ValidationError(error_message)

    #    if not hasattr(user, 'id'):
    #        error_message = 'Роль пользователя NoneID не позволяет редактировать запись'
    #        raise forms.ValidationError(error_message)

    #    users = ArcUser.objects.filter(user=user, role__in=[1])#admin

    #    if not users or not users.exists():
    #        error_message = 'Роль пользователя %s не позволяет редактировать запись' % (users)
    #        raise forms.ValidationError(error_message)

    def save(self, commit=True):
        row = super(RowForm, self).save(commit=commit)

        if self.instance and self.instance.pk:
            row.description = self.cleaned_data['row_name']
            row.save(update_fields=['description'])

        return row

    class Meta:
        model = Row
        exclude = ('description',)
        fields = ['row_name', 'rack_group', 'unit', 'role', 'position', 'section_count', 'shelf_count', 'enabled']

class CommandForm(forms.ModelForm):
    class Meta:
        model = Command
        fields = '__all__'
        exclude = ('created_by', 'modified_by', 'enabled')

class ItemTypeForm(forms.ModelForm):
    enabled = forms.ChoiceField(label=u'Состояние',
        choices=[(True, u'Активен'), (False, u'Не активен')]
    )

    def __init__(self, *args, **kwargs):
        super(ItemTypeForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({'style' : 'width: 50%;'})

        for field in self.fields:
            self.fields[field].required = False

    def clean_name(self):
        name = self.cleaned_data['name']
        if not name:
            raise forms.ValidationError(u'Поле обязательное')
        return name

    class Meta:
        model = ItemType
        fields = '__all__'
        exclude = ('created_by', 'modified_by', 'x', 'y', 'z', 'weight', 'search')

class ItemForm(forms.ModelForm):
    enabled = forms.ChoiceField(label=u'Состояние',
        choices=[(True, u'Активен'), (False, u'Не активен')]
    )

    def __init__(self, *args, **kwargs):
        super(ItemForm, self).__init__(*args, **kwargs)
        self.fields['released'].widget.attrs['disabled'] = True
        self.fields['item_type'].queryset = ItemType.objects.filter(enabled=True)
        self.fields['department'].queryset = Department.objects.filter(enabled=True)
        self.fields['client'].queryset = Client.objects.filter(enabled=True)
        self.fields['retention_date'].widget = forms.widgets.TextInput(attrs={
                                                'id': 'datepicker',
        })
        self.fields['number'].widget.attrs.update({'style' : 'width: 50%;'})
        self.fields['name'].widget.attrs.update({'style' : 'width: 50%;'})

        user = self.initial.get('user', None)
        self.fields['owner'].initial = user

        for field in self.fields:
            self.fields[field].required = False

    def clean_owner(self):
        field = self.cleaned_data['owner']
        if not field:
            raise forms.ValidationError(u'Поле обязательное')
        return field

    def clean_released(self):
        if self.instance: 
            return self.instance.released
        else: 
            return self.fields['released']

    def clean_number(self):
        number = self.cleaned_data['number']

        if not number:
            raise forms.ValidationError(u'Поле обязательное')

        if self.instance and self.instance.pk: # edit
            return number

        settings = Settings.objects.all()

        if settings and settings.exists() and settings.first().item_unique == True:
            items = Item.objects.filter(number=number)

            if items and items.exists():
                raise forms.ValidationError(u'Регистрационный № %s существует' % (number))
        return number

    class Meta:
        model = Item
        fields = ['number', 'name', 'description', 'item_type', 'department', 'client', 'owner', 'released', \
                   'retention_date', 'fiscal_year', 'retention_policy', 'enabled']
        exclude = ('created_by', 'modified_by')

class DocumentTypeForm(forms.ModelForm):
    enabled = forms.ChoiceField(label=u'Состояние',
        choices=[(True, u'Активен'), (False, u'Не активен')]
    )
    def __init__(self, *args, **kwargs):
        super(DocumentTypeForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({'style' : 'width: 50%;'})

        for field in self.fields:
            self.fields[field].required = False

    def clean_name(self):
        name = self.cleaned_data['name']
        if not name:
            raise forms.ValidationError(u'Поле обязательное')
        return name

    class Meta:
        model = DocumentType
        fields = '__all__'
        exclude = ('created_by', 'modified_by', 'search')

class DepartmentForm(forms.ModelForm):
    enabled = forms.ChoiceField(label=u'Состояние',
        choices=[(True, u'Активен'), (False, u'Не активен')]
    )

    def __init__(self, *args, **kwargs):
        super(DepartmentForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({'style' : 'width: 50%;'})

        for field in self.fields:
            self.fields[field].required = False

    def clean_name(self):
        name = self.cleaned_data['name']
        if not name:
            raise forms.ValidationError(u'Поле обязательное')
        return name

    class Meta:
        model = Department
        fields = '__all__'
        exclude = ('created_by', 'modified_by', 'search', )


class DocumentForm(forms.ModelForm):
    enabled = forms.ChoiceField(label=u'Состояние',
        choices=[(True, u'Активен'), (False, u'Не активен')]
    )
    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        #self.fields['item'].queryset = Item.objects.all().exclude(item_type__enabled=False)
        self.fields['item'].queryset = Item.objects.all()
        self.fields['document_type'].queryset = DocumentType.objects.filter(enabled=True)
        self.fields['number'].widget.attrs.update({'style' : 'width: 50%;'})
        self.fields['name'].widget.attrs.update({'style' : 'width: 50%;'})

        user = self.initial.get('user', None)
        self.fields['owner'].initial = user

        for field in self.fields:
            self.fields[field].required = False

    def clean_number(self):
        number = self.cleaned_data['number']

        if not number:
            raise forms.ValidationError(u'Поле обязательное')

        if self.instance and self.instance.pk: # edit
            return number

        settings = Settings.objects.all()

        if settings and settings.exists() and settings.first().document_unique == True:
            items = Document.objects.filter(number=number)

            if items and items.exists():
                raise forms.ValidationError(u'Регистрационный № %s существует' % (number))
        return number

    def clean_item(self):
        field = self.cleaned_data['item']
        if not field:
            raise forms.ValidationError(u'Поле обязательное')
        return field

    def clean_document_type(self):
        field = self.cleaned_data['document_type']
        if not field:
            raise forms.ValidationError(u'Поле обязательное')
        return field

    def clean_owner(self):
        field = self.cleaned_data['owner']
        if not field:
            raise forms.ValidationError(u'Поле обязательное')
        return field

    class Meta:
        model = Document
        fields = ['number', 'name', 'document_type', 'description', 'item', 'owner']
        exclude = ('created_by', 'modified_by')

class ClientForm(forms.ModelForm):
    enabled = forms.ChoiceField(label=u'Состояние',
        choices=[(True, u'Активен'), (False, u'Не активен')]
    )
    def __init__(self, *args, **kwargs):
        super(ClientForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({'style' : 'width: 50%;'})
        self.fields['client_type'].queryset = ClientType.objects.filter(enabled=True)

        for field in self.fields:
            self.fields[field].required = False

    def clean_name(self):
        name = self.cleaned_data['name']
        if not name:
            raise forms.ValidationError(u'Поле обязательное')
        return name

    class Meta:
        model = Client 
        fields = '__all__'
        exclude = ('created_by', 'modified_by', 'search')

class ClientTypeForm(forms.ModelForm):
    enabled = forms.ChoiceField(label=u'Состояние',
        choices=[(True, u'Активен'), (False, u'Не активен')]
    )

    def __init__(self, *args, **kwargs):
        super(ClientTypeForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({'style' : 'width: 50%;'})

        for field in self.fields:
            self.fields[field].required = False

    def clean_name(self):
        name = self.cleaned_data['name']
        if not name:
            raise forms.ValidationError(u'Поле обязательное')
        return name

    class Meta:
        model = ClientType
        fields = '__all__'
        exclude = ('created_by', 'modified_by', 'search')

class RetentionPolicyForm(forms.ModelForm):
    color = forms.ChoiceField(label=_(u'Цвет'),
        choices=[(None, '-----')] + [r for r in PERIOD_COLOR],
    )
    def __init__(self, *args, **kwargs):
        super(RetentionPolicyForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({'style' : 'width: 50%;'})

        for field in self.fields:
            self.fields[field].required = False

    def clean_name(self):
        name = self.cleaned_data['name']
        if not name:
            raise forms.ValidationError(u'Поле обязательное')
        return name

    def clean_description(self):
        description = self.cleaned_data['description']
        if not description:
            raise forms.ValidationError(u'Поле обязательное')
        else:
            return description

    def clean_value(self):
        value = self.cleaned_data['value']
        if not value:
            raise forms.ValidationError(u'Поле обязательное')
        else:
            return value

    class Meta:
        model = RetentionPolicy
        fields = ['name', 'description', 'date_period_type', 'value', 'color']
        exclude = ('created_by', 'modified_by', 'search')

class SettingsForm(forms.ModelForm):
    class Meta:
        model = Settings
        exclude = ('exists',)

