# -*- coding: utf-8 -*-

import operator

from archive.storage.models import Rack


class RackSortMiddleware(object):
    '''
    special rack group sorting
    '''

    def process_template_response(self, request, response):
        '''
        after view / before rendering
        '''

        response.data = self.sort(getattr(response, 'data', ''))

        return response


    @classmethod
    def sort(cls, data):
        '''
        racks sorting
        '''

        if data and not isinstance(data, str):
            key = 'rack_group'

            rack_groups = data.get('rack_groups', [])
            if isinstance(rack_groups, list):
                for g in range(0, len(rack_groups)): rack_groups[g] = cls.sort({key: rack_groups[g]})[key]

            rack_group = data.get(key, {})
            if rack_group:
                racks_key = 'rack_set'
                rack_set = rack_group[racks_key]
                racks = []
                rack_right = {}
                for r in rack_set:
                    if r['role'] == Rack._roles['static_left']:
                        racks = [r] + racks
                    elif r['role'] == Rack._roles['mobile']:
                        racks.append(r)
                    elif r['role'] == Rack._roles['static_right']:
                        rack_right = r

                if rack_right: racks.append(rack_right)
                rack_group[racks_key] = racks
                rack_group = IndicatorCountMiddleware.set_count(rack_group)

        return data


class IndicatorCountMiddleware(object):
    '''
    add indicator count for IndicatorGroup
    '''

    def process_template_response(self, request, response):
        '''
        after view / before rendering
        '''

        response.data = self.set_count(getattr(response, 'data', ''))

        return response


    @classmethod
    def set_count(cls, data):
        '''
        update indicator controller instance with it's own indicator count
        '''

        if data and not isinstance(data, str):
            key = 'indicator_group'

            indicator_groups = data.get('indicator_groups', data.get('indicatorgroup_set', []))
            if isinstance(indicator_groups, list):
                for c in range(0, len(indicator_groups)):
                    indicator_groups[c] = cls.set_count({key: indicator_groups[c]})[key]

            indicator_group = data.get(key, {})
            if indicator_group: indicator_group['indicator_count'] = len(indicator_group['indicator_set'])

        return data

