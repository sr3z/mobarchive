# -*- coding: utf-8 -*-

from codecs import decode
import inspect

from django.contrib.auth.models import User as DefaultUser, Group
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import UUIDField
from django.conf import settings

from archive.utils.models import TimeStampedModelIndexed, BaseParameter
from django.apps import apps
import datetime
from django.utils.dateformat import DateFormat
from dateutil.relativedelta import relativedelta
from django.utils.formats import get_format
from django.utils.timezone import localtime

PERIOD_TYPE = (
    ('1year', u'1 год'),
    ('3years', u'3 года'),
    ('5years', u'5 лет'),
    ('10years', u'10 лет'),
    ('15years', u'15 лет'),
    ('20years', u'20 лет'),
    ('30years', u'30 лет'),
    ('75years', u'75 лет'),
    ('unlimited', u'неограниченно'),
)

PERIOD_COLOR = (
    ('#ff0000', u'Красный'),
    ('#6e390e', u'Коричневый'),
    ('#0000ff', u'Синий'),
    ('#990099', u'Фиолетовый'),
    ('#e46c0a', u'Оранжевый'),
    ('#7f7f7f', u'Серый'),
    ('#ffff00', u'Желтый'),
    ('#00ffff', u'Голубой'),
    ('#00ff00', u'Зеленый'),
    ('#999933', u'Оливковый'),
    ('#ff99cc', u'Розовый'),
    ('#cc99ff', u'Сиреневый'),
    ('#008b00', u'Темно-зеленый'),
    ('#000000', u'Черный'),
    ('#ffaa7f', u'Персиковый'),
)

DATE_PERIOD_TYPE = (
    ('YEAR', u'год'),
    ('MONTH', u'месяц'),
    ('WEEK', u'неделя'),
    ('DAY',u'день'),
    ('UNLIM', u'неограниченный'),
)

POSITION = (
    (0, u'Левая'),
    (1, u'Правая'),
)

RACK_GROUP_STATUS = (
    (0, u'Отладка'),
    (2, u'Доступна'),
    (3, u'Зарезервирована'),
    (4, u'Исполняет команду'),
    (5, u'Готова'),
    (6, u'Недоступна'),
)

CONTROLLER_MODEL = (
    #(0, u'ИСТА'),
    #(1, u'Brunzel'),
    (0, u'Модель 0'),
    (1, u'Модель 1'),
)

RACK_ROLES = (
    (0, u'Передвижной'),
    (1, u'Стационарный'),
    #(2, u'Стационарный правый'),
)

FUNCTIONS = (
    ('open_aisle_friend', 'open_aisle_friend'),
    ('open_aisle_alien', 'open_aisle_alien'),
)

SLOT_STATUS = (
    (0, u'Свободно'),
    (1, u'Зарезервировано'),
    (2, u'Выдача'),
    (3, u'Занято'),
)

def hex_command(data):
    map_data = ['{:02x}'.format(c) for c in data]
    data_send = decode(''.join(map_data), 'hex_codec')
    return data_send


class Command(models.Model):
    name = models.CharField(_(u'Наименование'), max_length=100)
    code = models.CharField(max_length=100, null=True, blank=True,
                            verbose_name=_(u'Код'))
    model = models.PositiveSmallIntegerField(
        choices=CONTROLLER_MODEL,
        default=0,
        verbose_name=_(u'Модель')
    )
    function = models.CharField(max_length=512, null=True, blank=True,
                                choices=FUNCTIONS,
                                verbose_name=_(u'Функция'))

    def __str__(self):
        return '%s %s' % (self.name, self.model)

    def __unicode__(self):
        return '%s %s' % (self.name, self.model)

    def open_aisle_friend(self, param=None):
        command = [2, param, 0, 0]
        return hex_command(command)

    def open_aisle_alien(self, param=None):
        command = 0x3a
        parameter = str(int(param) + 1)  # 0-based aisle index convert
        # to 1-based
        return ",".join(list[command, str(parameter)])

    def get_status_alien(self, param=None):
        command = 0x36
        if param:
            parameter = str(int(param) + 1)  # 0-based aisle index convert
        else:
            parameter = ''
        # to 1-based
        return ",".join(list[command, str(parameter)])

    class Meta:
        unique_together = ('name', 'model')


class RackGroup(TimeStampedModelIndexed, BaseParameter):
    #state = models.PositiveSmallIntegerField(
    #    choices=RACK_GROUP_STATUS,
    #    default=0,
    #    verbose_name=_(u'Статус')
    #)
    model = models.PositiveSmallIntegerField(
        choices=CONTROLLER_MODEL,
        default=0,
        verbose_name=_(u'Модель')
    )
    #    slot_x = models.PositiveSmallIntegerField() # width
    #    slot_y = models.PositiveSmallIntegerField() # height
    #    slot_z = models.PositiveSmallIntegerField() # depth

    ip = models.GenericIPAddressField(protocol='IPv4')

    port = models.IntegerField(default=5000, verbose_name=_(u'Порт'))

    move_duration = models.PositiveSmallIntegerField(default=60,
                                                     verbose_name=(u'Время перемещения'))

    groups = models.ManyToManyField(Group, null=True, blank=True,
                                    related_name="rack_groups",
                                    verbose_name=_(u'Архивные Группы'))

    blocker = models.ForeignKey(DefaultUser, null=True, blank=True,
                                on_delete=models.SET_NULL,
                                related_name="occupied_by",
                                verbose_name=_(u"Использует"))
    temperature_min = models.PositiveSmallIntegerField(null=True, blank=True, verbose_name=_(u'MIN Температура'))
    temperature_max = models.PositiveSmallIntegerField(null=True, blank=True, verbose_name=_(u'MAX Температура'))

    humidity_min = models.PositiveSmallIntegerField(null=True, blank=True, verbose_name=_(u'MIN Влажность'))
    humidity_max = models.PositiveSmallIntegerField(null=True, blank=True, verbose_name=_(u'MAX Влажность'))

    search = models.TextField(_(u'Поиск'), null=True, blank=True)

    def create_index(self):
        search = ''
        try:
            if self.id:
                search = search + str(self.id)
            if self.name:
                search = search + self.name.replace(' ', '')
            if self.ip:
                search = search + self.ip
            if self.port:
                search = search + str(self.port)
            if self.model:
                search = search + str(self.model)
            if self.created:
                df = DateFormat(self.created)
                search = search + df.format(get_format('DATE_FORMAT'))
                search = search + localtime(self.created).strftime('%H:%M')
            if self.enabled:
                search = search + 'Активен'
            else:
                search = search + 'Не активен'
        except Exception as e:
            print("LOG: %s %s ERROR: %s" % (self.__class__, inspect.stack()[0][3].upper(), e))

        if search and len(search) > 1:
            self.search = search.lower().replace(' ' , '')
        else:
            self.search = ''

        if settings.DEBUG_MESSAGES:
            print("LOG: %s SEARCH: %s" % (self.__class__, self.search))
        return self.search

    def save(self, *args, **kwargs):
        self.create_index()
        super(self.__class__, self).save(*args, **kwargs)
        self.create_index()

    def __str__(self):
        return '%s [%s:%s]' % (self.name, self.ip, self.port)

    def __unicode__(self):
        return '%s [%s:%s:%s]' % (self.name,
                [m for m in CONTROLLER_MODEL if m[0] == self.model][0][1],
                self.ip, self.port)

    class Meta:
        verbose_name = _('Мобильная группа')
        unique_together = ('ip', 'port',)
        unique_together = ('ip', 'model',)
        ordering = ['id']


class IndicatorGroup(TimeStampedModelIndexed, BaseParameter):
    _models = {
        'atop': 0,
    }
    model = models.PositiveSmallIntegerField(
        choices=(
            (_models['atop'], 'Atop'),
        ),
        default=_models['atop'],
        verbose_name=_(u'Модель')
    )
    ip = models.GenericIPAddressField(protocol='IPv4', unique=True)
    port = models.PositiveSmallIntegerField(default=4660,
                                            verbose_name=_(u'Порт'))
    rack_group = models.ForeignKey(RackGroup,
                                   on_delete=models.CASCADE,
                                   verbose_name=_(u'Архивная группа'))

    def __str__(self):
        return '%s [%s:%s]' % (self.name, self.ip, self.port)

    def __unicode__(self):
        return '%s [%s:%s]' % (self.name, self.ip, self.port)


class Indicator(models.Model):
    indicator_group = models.ForeignKey(IndicatorGroup,
                                        on_delete=models.CASCADE,
                                        verbose_name=_(u'Индикатор'))

    def __str__(self):
        return '%s @ %s' % (self.pk, self.indicator_group)

    def __unicode__(self):
        return '%s @ %s' % (self.pk, self.indicator_group)


class Rack(models.Model):
    rack_group = models.ForeignKey(RackGroup, on_delete=models.CASCADE,
                                   verbose_name=_(u'Мобильная группа'))
    unit = models.PositiveSmallIntegerField(default=0,
                                            verbose_name=_(u'Место'))
    role = models.PositiveSmallIntegerField(
        choices=RACK_ROLES,
        default=0,
        verbose_name=_(u'Тип стеллажа'))
    enabled = models.BooleanField(default=True, verbose_name=_(u'Состояние'))
    # use this shelf width in calculations
    shelf_width = models.PositiveSmallIntegerField(default=0,
                                                   verbose_name=_(u'Ширина полки'))
    def get_shelfs(self):
        ''' Getting all shelfs of rack with priority from lower to upper levels.
        '''
        shelfs = []
        rows = self.rows_set.all()
        for row in rows:
            sections = row.sections_set.all()
            for section in sections:
                shelf_objects = section.shelfs_set.order_by("level")
                for shelf in shelf_objects:
                    shelfs.append(shelf)
        return shelfs

    def __str__(self):
        return u'Стеллаж %s [%s]' % (self.unit, [r for r in RACK_ROLES if r[0]
                                         == self.role][0][1])
    def __unicode__(self):
        return u'Стеллаж "%s" [%s]' % (self.name, [r for r in RACK_ROLES if r[0]
                                         == self.role][0][1])
    class Meta:
        verbose_name = _('Стеллаж')
        ordering = ['rack_group__id', 'unit', 'id']


class Row(models.Model):
    rack = models.ForeignKey(Rack, on_delete=models.CASCADE,
                             verbose_name=_(u'Cтеллаж'))
    position = models.PositiveSmallIntegerField(
        verbose_name=_(u'Ряд'),
        choices=POSITION,
        default=0,
    )
    description = models.TextField(_(u'Описание'), null=True, blank=True)
    search = models.TextField(_(u'Поиск'), null=True, blank=True)

    def create_index(self):
        search = ''
        try:
            search = search + str(self.get_pass_number()).replace(' ', '')
            if self.description:
                search = search + self.description.replace(' ', '')
            if self.rack:
                search = search + self.rack.rack_group.__str__().replace(' ' , '') 
                search = search + self.rack.__str__().replace(' ' , '') 
                search = search + [r for r in POSITION if r[0] == self.position][0][1]

                if self.section_set:
                    search = search + str(self.section_set.count())
                    sections = self.section_set.all()

                    max_count = 0
                    for section in sections:
                        if section.shelf_set.all().count() > max_count:
                            max_count = section.shelf_set.all().count()
                    search = search + str(max_count)
        except Exception as e:
            print("LOG: %s %s ERROR: %s" % (self.__class__, inspect.stack()[0][3].upper(), e))

        if search and len(search) > 1:
            self.search = search.lower().replace(' ' , '')
        else:
            self.search = ''

        if settings.DEBUG_MESSAGES:
            print("LOG: %s SEARCH: %s" % (self.__class__, self.search))
        return self.search

    def get_pass_number(self):
        if settings.DEBUG_MESSAGES:
            print("LOG: GET_PASS_NUMBER ****************************************")
        rows = Row.objects.filter(rack__rack_group=self.rack.rack_group).order_by('rack__unit', 'rack__id', 'position')
        last_position = -1
        last_rack = None
        idx = -1
        for row in rows:
            if settings.DEBUG_MESSAGES:
                print("LOG: RACK: ", row.rack.id, ": ", row.rack)
                print("LOG:      ROW: ", row.id, ": ", row)
            if row.position != last_position and last_rack == row.rack:
                idx = idx + 1
            elif row.position > last_position and last_rack !=row.rack:
                idx = idx + 1
            elif row.position == last_position and last_rack != row.rack:
                idx = idx + 1

            if row.id == self.id:
                if settings.DEBUG_MESSAGES:
                    print("LOG: ID: %s ********************************************" % idx)
                return idx

            if row.position != last_position:
                last_position = row.position
            if last_rack != row.rack:
                last_rack = row.rack

    @property
    def position_name(self):
        return u'%s' % ([r for r in POSITION if r[0] == self.position][0][1])

    def get_row_number(self):
        rows = Row.objects.filter(rack__rack_group=self.rack.rack_group).order_by('rack__unit', 'rack__id', 'position')

        for idx, row in enumerate(rows):
            if row.id == self.id:
                return idx + 1
        return None

    def save(self, *args, **kwargs):
        self.create_index()
        super(self.__class__, self).save(*args, **kwargs)
        self.create_index()

    class Meta:
        unique_together = (
            (
                'rack',
                'position'
            ),
        )
        ordering = ['rack__rack_group__id', 'rack__unit', 'rack__id', 'position']

    def __str__(self):
        return u'%s %s сторона' % (self.description if self.description else '', [r for r in POSITION if r[0] == self.position][0][1])

    def __unicode__(self):
        return u'Ряд "%s" %s сторона' % (self.get_row_number(), [r for r in POSITION if r[0] == self.position][0][1])


class Section(models.Model):
    number = models.PositiveSmallIntegerField(
        default=1,
        verbose_name=_(u'Номер')
    )
    row = models.ForeignKey(Row, on_delete=models.CASCADE,
                            verbose_name=_(u'Проход'))
    rack = models.ForeignKey(Rack, on_delete=models.CASCADE,
                             blank=True, null=True, verbose_name=_(u'Cтеллаж'))

    def __str__(self):
        return u'Секция "%s"' % (self.number)

    def __unicode__(self):
        return u'Секция "%s"' % (self.number)

    class Meta:
        unique_together = ('row', 'number')
        ordering = ['id']


class Shelf(models.Model):
    number = models.PositiveSmallIntegerField(
        default=1,
        verbose_name=_(u'Номер')
    )
    section = models.ForeignKey(Section, on_delete=models.CASCADE,
                                verbose_name=_(u'Cекция'))
    filling_percent = models.PositiveSmallIntegerField(default=0,
                                                       verbose_name=_(u'Процент заполнения'))
    level = models.PositiveSmallIntegerField(default=0,
                                             verbose_name=_(u'Уровень полки (от пола)'))

    x = models.PositiveSmallIntegerField(_(u'Ширина'), default=1)  # width
    y = models.PositiveSmallIntegerField(_(u'Высота'), default=5)  # height
    z = models.PositiveSmallIntegerField(_(u'Глубина'), default=3)  # depth

    def __str__(self):
        return u'Полка "%s"' % (self.number)

    def __unicode__(self):
        return u'Полка "%s"' % (self.number)

    def get_weight(self):
        return None

    class Meta:
        unique_together = ('number', 'section')
        ordering = ['id']

class ItemType(TimeStampedModelIndexed, BaseParameter):
    x = models.PositiveSmallIntegerField(_(u'Ширина'), default=1)  # width
    y = models.PositiveSmallIntegerField(_(u'Высота'), default=5)  # height
    z = models.PositiveSmallIntegerField(_(u'Глубина'), default=3)  # depth

    weight = models.PositiveSmallIntegerField(default=1, verbose_name=_(u'Вес'))  # depth

    search = models.TextField(_(u'Поиск'), null=True, blank=True)

    def create_index(self):
        search = ''
        try:
            if self.name:
                search = search + self.name.replace(' ', '')
            if self.description:
                search = search + self.description.replace(' ', '')
            if self.enabled:
                search = search + 'Активен'
            else:
                search = search + 'Не активен'
            if self.created:
                df = DateFormat(self.created)
                search = search + df.format(get_format('DATE_FORMAT'))
                search = search + localtime(self.created).strftime('%H:%M')
            if self.created_by != None:
                search = search + self.created_name
            if self.modified:
                df = DateFormat(self.modified)
                search = search + df.format(get_format('DATE_FORMAT'))
                search = search + localtime(self.modified).strftime('%H:%M')
            if self.modified_by != None:
                search = search + self.modified_name
        except Exception as e:
            print("LOG: %s %s ERROR: %s" % (self.__class__, inspect.stack()[0][3].upper(), e))

        if search and len(search) > 1:
            self.search = search.lower().replace(' ' , '')
        else:
            self.search = ''

        if settings.DEBUG_MESSAGES:
            print("LOG: %s SEARCH: %s" % (self.__class__, self.search))
        return self.search

    def save(self, *args, **kwargs):
        self.create_index()
        super(self.__class__, self).save(*args, **kwargs)
        self.create_index()

    @property
    def created_name(self):
        creator = DefaultUser.objects.filter(id=self.created_by).first()
        if creator:
            return creator.username
        else:
            return ''

    @property
    def modified_name(self):
        modifier = DefaultUser.objects.filter(id=self.modified_by).first()
        if modifier:
            return modifier.username
        else:
            return ''

    def __str__(self):
        return '%s%s' % (self.name, '' if self.enabled else
        u' [Не активен]')

    def __unicode__(self):
        return '%s%s' % (self.name, '' if self.enabled else
        u' [Не активен]')

    class Meta:
        permissions = [
            ("item_type.edit", _("Редактирование типа ЕХ")),
        ]
        ordering = ['name']


class ClientType(BaseParameter, models.Model):
    search = models.TextField(_(u'Поиск'), null=True, blank=True)

    def create_index(self):
        search = ''
        try:
            if self.id:
                search = search + str(self.id)
            if self.name:
                search = search + self.name.replace(' ', '')
            if self.description:
                search = search + self.description.replace(' ', '')
            if self.enabled:
                search = search + 'Активен'
            else:
                search = search + 'Не активен'
        except Exception as e:
            print("LOG: %s %s ERROR: %s" % (self.__class__, inspect.stack()[0][3].upper(), e))

        if search and len(search) > 1:
            self.search = search.lower().replace(' ' , '')
        else:
            self.search = ''

        if settings.DEBUG_MESSAGES:
            print("LOG: %s SEARCH: %s" % (self.__class__, self.search))
        return self.search

    def save(self, *args, **kwargs):
        self.create_index()
        super(self.__class__, self).save(*args, **kwargs)
        self.create_index()

    def __str__(self):
        return '%s%s' % (self.name, '' if self.enabled else
        u' [Не активен]')

    def __unicode__(self):
        return '%s%s' % (self.name, '' if self.enabled else
        u' [Не активен]')

    class Meta:
        permissions = [
            ("client_type.edit", _("Редактирование типа клиента")),
        ]

class Client(BaseParameter, models.Model):
    client_type = models.ForeignKey(ClientType,
                                blank=True, null=True,
                                on_delete=models.SET_NULL,
                                verbose_name=_(u'Тип группы'))

    enabled = models.BooleanField(default=True)

    uuid = UUIDField()

    search = models.TextField(_(u'Поиск'), null=True, blank=True)

    def create_index(self):
        search = ''
        try:
            if self.id:
                search = search + str(self.id)
            if self.name:
                search = search + self.name.replace(' ', '')
            if self.description:
                search = search + self.description.replace(' ', '')
            if self.client_type:
                search = search + self.client_type.__str__().replace(' ', '')
            if self.enabled:
                search = search + 'Активен'
            else:
                search = search + 'Не активен'
        except Exception as e:
            print("LOG: %s %s ERROR: %s" % (self.__class__, inspect.stack()[0][3].upper(), e))

        if search and len(search) > 1:
            self.search = search.lower().replace(' ' , '')
        else:
            self.search = ''

        if settings.DEBUG_MESSAGES:
            print("LOG: %s SEARCH: %s" % (self.__class__, self.search))
        return self.search

    @property
    def created_name(self):
        creator = DefaultUser.objects.filter(id=self.created_by).first()
        if creator:
            return creator.username
        else:
            return ''

    @property
    def modified_name(self):
        modifier = DefaultUser.objects.filter(id=self.modified_by).first()
        if modifier:
            return modifier.username
        else:
            return ''

    def save(self, *args, **kwargs):
        self.create_index()
        super(self.__class__, self).save(*args, **kwargs)
        self.create_index()

    def __str__(self):
        return '%s%s' % (self.name, '' if self.enabled else
        u' [Не активен]')

    def __unicode__(self):
        return '%s%s' % (self.name, '' if self.enabled else
        u' [Не активен]')

    class Meta:
        permissions = [
            ("client.edit", _("Редактирование клиента")),
        ]
        ordering = ['id']

class RetentionPolicy(TimeStampedModelIndexed, models.Model):
    date_period_type = models.CharField(
        _(u'Временной период'),
        max_length=64,
        choices=DATE_PERIOD_TYPE,
        default='YEAR'
    )
    name = models.CharField(_(u'Наименование периода'), max_length=64, unique=True, default='')
    description = models.TextField(_(u'Описание'), default='', null=True)
    value = models.IntegerField(_(u'Значение'), default=0)

    color = models.CharField(
        _(u'Цвет'),
        max_length=64,
        choices=PERIOD_COLOR,
        blank=True, null=True
    )
    search = models.TextField(_(u'Поиск'), null=True, blank=True)

    def create_index(self):
        search = ''
        try:
            if self.name:
                search = search + self.name.replace(' ', '')
            if self.description:
                search = search + self.description.replace(' ', '')
            if self.color:
                search = search + [r for r in PERIOD_COLOR if r[0] == self.color][0][1]
                #search = search + self.color.replace(' ', '')
        except Exception as e:
            print("LOG: %s %s ERROR: %s" % (self.__class__, inspect.stack()[0][3].upper(), e))

        if search and len(search) > 1:
            self.search = search.lower().replace(' ' , '')
        else:
            self.search = ''

        if settings.DEBUG_MESSAGES:
            print("LOG: %s SEARCH: %s" % (self.__class__, self.search))
        return self.search

    def save(self, *args, **kwargs):
        self.create_index()
        super(self.__class__, self).save(*args, **kwargs)
        self.create_index()

    def __str__(self):
        return u'%s' % (self.name if self.name else '')

    def __unicode__(self):
        return u'%s' % (self.name if self.name else '')


class DocumentType(TimeStampedModelIndexed, BaseParameter):
    uuid = UUIDField()

    search = models.TextField(_(u'Поиск'), null=True, blank=True)

    def create_index(self):
        search = ''
        try:
            if self.name:
                search = search + self.name.replace(' ', '')
            if self.description:
                search = search + self.description.replace(' ', '')
            if self.enabled:
                search = search + 'Активен'
            else:
                search = search + 'Не активен'
            if self.created:
                df = DateFormat(self.created)
                search = search + df.format(get_format('DATE_FORMAT'))
                search = search + localtime(self.created).strftime('%H:%M')
            if self.created_by != None:
                search = search + self.created_name
            if self.modified:
                df = DateFormat(self.modified)
                search = search + df.format(get_format('DATE_FORMAT'))
                search = search + localtime(self.modified).strftime('%H:%M')
            if self.modified_by != None:
                search = search + self.modified_name
        except Exception as e:
            print("LOG: %s %s ERROR: %s" % (self.__class__, inspect.stack()[0][3].upper(), e))

        if search and len(search) > 1:
            self.search = search.lower().replace(' ' , '')
        else:
            self.search = ''

        if settings.DEBUG_MESSAGES:
            print("LOG: %s SEARCH: %s" % (self.__class__, self.search))
        return self.search

    @property
    def created_name(self):
        creator = DefaultUser.objects.filter(id=self.created_by).first()
        if creator:
            return creator.username
        else:
            return ''

    @property
    def modified_name(self):
        modifier = DefaultUser.objects.filter(id=self.modified_by).first()
        if modifier:
            return modifier.username
        else:
            return ''

    def save(self, *args, **kwargs):
        self.create_index()
        super(self.__class__, self).save(*args, **kwargs)
        self.create_index()

    def __str__(self):
        return '%s%s' % (self.name, '' if self.enabled else
        u' [Не активен]')

    def __unicode__(self):
        return '%s%s' % (self.name, '' if self.enabled else
        u' [Не активен]')


class Department(TimeStampedModelIndexed, BaseParameter):
    uuid = UUIDField()

    search = models.TextField(_(u'Поиск'), null=True, blank=True)

    class Meta:
        ordering = ["id"]

    def create_index(self):
        search = ''
        try:
            if self.id:
                search = search + str(self.id)
            if self.name:
                search = search + self.name.replace(' ', '')
            if self.description:
                search = search + self.description.replace(' ', '')
            if self.enabled:
                search = search + 'Активен'
            else:
                search = search + 'Не активен'
        except Exception as e:
            print("LOG: %s %s ERROR: %s" % (self.__class__, inspect.stack()[0][3].upper(), e))

        if search and len(search) > 1:
            self.search = search.lower().replace(' ' , '')
        else:
            self.search = ''

        if settings.DEBUG_MESSAGES:
            print("LOG: %s SEARCH: %s" % (self.__class__, self.search))
        return self.search

    def save(self, *args, **kwargs):
        self.create_index()
        super(self.__class__, self).save(*args, **kwargs)
        self.create_index()

    def __str__(self):
        return '%s' % (self.name)

    def __unicode__(self):
        return '%s' % (self.name)

class ItemManager(models.Manager):
    def get_queryset(self):
        return super(self.__class__, self).get_queryset().filter(delete_date__isnull=True)

class Item(TimeStampedModelIndexed):
    number = models.CharField(
        _(u'Регистрационный №'),
        max_length=64,
        blank=True, null=True
    )
    name = models.CharField(_(u'Наименование'), max_length=100, null=True, blank=True)
    description = models.TextField(_(u'Описание'), null=True, blank=True)
    enabled = models.BooleanField(default=True, verbose_name=_(u'Состояние'))

    item_type = models.ForeignKey(ItemType, verbose_name=_(u'Тип единицы хранения'),
                                  on_delete=models.SET_NULL,
                                  null=True)
    client = models.ForeignKey(Client, verbose_name=_(u'Группа'),
                               on_delete=models.SET_NULL,
                               null=True, blank=True)
    department = models.ForeignKey(Department,
                                   on_delete=models.SET_NULL,
                                   blank=True, null=True,
                                   verbose_name=_(u'Отдел'))
    owner = models.ForeignKey(DefaultUser,
                                null=True, blank=True,
                                on_delete=models.SET_NULL,
                                verbose_name=_(u"Ответственное лицо"))
    released = models.BooleanField(_(u'Выдан?'), default=True)

    retention_date = models.DateField(
        _(u'Дата уничтожения'),
        blank=True, null=True
    )
    fiscal_year = models.PositiveSmallIntegerField(
        default=2017,
        verbose_name=_(u'Финансовый год')
    )
    retention_policy = models.ForeignKey(RetentionPolicy,
                                         null=True, blank=True,
                                         on_delete=models.SET_NULL,
                                         verbose_name=_(u"Период хранения"))
    combined_date = models.DateField(
        _(u'Дата уничтожения общая'),
        blank=True, null=True
    )
    search = models.TextField(_(u'Поиск'), null=True, blank=True)

    #objects = ItemManager()

    def create_index(self):
        search = ''
        try:
            if self.number:
                search = search + self.number.replace(' ', '')
            if self.name:
                search = search + self.name.replace(' ', '')
            if self.description:
                search = search + self.description.replace(' ', '')
            if self.item_type:
                search = search + self.item_type.__str__().replace(' ', '')
            if self.client:
                search = search + self.client.__str__().replace(' ', '')
            if self.department:
                search = search + self.department.__str__().replace(' ', '')
            if self.owner:
                search = search + self.owner.__str__().replace(' ', '')
            if self.released:
                search = search + 'Выдан'
            else:
                search = search + 'Не выдан'
            if self.retention_date:
                df = DateFormat(self.retention_date)
                search = search + df.format(get_format('DATE_FORMAT'))
            if self.fiscal_year:
                search = search + str(self.fiscal_year).replace(' ', '')
            if self.retention_policy:
                search = search + self.retention_policy.__str__().replace(' ', '')
            if self.slot_set and self.slot_set.all().first():
                search = search + self.slot_set.all().first().shelf.section.row.rack.rack_group.__str__().replace(' ' , '') 
                search = search + self.slot_set.all().first().shelf.section.row.__str__().replace(' ' , '') 
                search = search + self.slot_set.all().first().shelf.section.__str__().replace(' ' , '') 
                search = search + self.slot_set.all().first().shelf.__str__().replace(' ' , '') 
                search = search + self.slot_set.all().first().__str__().replace(' ' , '') 
            if self.created:
                df = DateFormat(self.created)
                search = search + df.format(get_format('DATE_FORMAT'))
                search = search + localtime(self.created).strftime('%H:%M')
            if self.created_by != None:
                search = search + self.created_name
            if self.modified:
                df = DateFormat(self.modified)
                search = search + df.format(get_format('DATE_FORMAT'))
                search = search + localtime(self.modified).strftime('%H:%M')
            if self.modified_by != None:
                search = search + self.modified_name
        except Exception as e:
            print("LOG: %s %s ERROR: %s" % (self.__class__, inspect.stack()[0][3].upper(), e))

        if search and len(search) > 1:
            self.search = search.lower().replace(' ' , '')
        else:
            self.search = ''

        if settings.DEBUG_MESSAGES:
            print("LOG: %s SEARCH: %s" % (self.__class__, self.search))
        return self.search

    def __str__(self):
        return '%s%s%s' % ('№ %s ' % self.number if
        self.number else '', '"%s" ' % self.name if self.name else '', ' [%s]' % self.item_type if self.item_type else '')

    def __unicode__(self):
        return '%s%s%s' % ('№ %s ' % self.number if
        self.number else '', '"%s" ' % self.name if self.name else '', ' [%s]' % self.item_type if self.item_type else '')

    @property
    def released_to(self):
        try:
            Task = apps.get_model('api', 'Task')
            task = Task.objects.filter(item=self).order_by('created').last()
            if task and task.mission.name == 'OUT':
                return task.owner
            else:
                return None
                return 'Not released'
        except Exception as e:
            return e
            return None

    @property
    def created_name(self):
        creator = DefaultUser.objects.filter(id=self.created_by).first()
        if creator:
            return creator.username
        else:
            return ''

    @property
    def modified_name(self):
        modifier = DefaultUser.objects.filter(id=self.modified_by).first()
        if modifier:
            return modifier.username
        else:
            return ''

    @property
    def weight(self):
        return self.item_type.weight

    def save(self, *args, **kwargs):
        self.create_index()
        super(self.__class__, self).save(*args, **kwargs)

        if self.retention_date != None:
            self.combined_date = self.retention_date
        else:
            if self.retention_policy != None:
                policies = RetentionPolicy.objects.filter(id=self.retention_policy.id)
                if policies and policies.count() > 0:
                    policy = policies.first()

                    now = datetime.datetime.now()
                    if policy.date_period_type == 'YEAR':
                        date = datetime.datetime.combine(now + relativedelta(years=policy.value), datetime.time.min)
                    elif policy.date_period_type == 'UNLIM':
                        date = datetime.datetime.combine(now + relativedelta(years=100), datetime.time.min)
                    elif policy.date_period_type == 'MONTH':
                        date = datetime.datetime.combine(now + relativedelta(months=policy.value), datetime.time.min)
                    elif policy.date_period_type == 'DAY':
                        date = datetime.datetime.combine(now + relativedelta(days=policy.value), datetime.time.min)
                    self.combined_date = date
        self.create_index()

    class Meta:
        permissions = [
            ("item.edit", _("Редактирование ЕХ")),
        ]

class ItemRemoved(models.Model):
    rid = models.PositiveSmallIntegerField(default=0)
    name = models.CharField(_(u'Наименование'), max_length=100, null=True)
    description = models.TextField(_(u'Описание'), null=True, blank=True)
    enabled = models.BooleanField(default=True, verbose_name=_(u'Состояние'))
    created = models.DateTimeField(_('Создан'), db_index=True)
    created_by = models.IntegerField(_(u'№ пользователя'), default=-1)
    modified = models.DateTimeField(_('Изменен'), null=True)
    modified_by = models.IntegerField(_(u'№ пользователя'), default=-1)
    number = models.CharField(
        _(u'Регистрационный №'),
        max_length=64,
        blank=True, null=True
    )

    item_type = models.ForeignKey(ItemType, verbose_name=_(u'Тип единицы хранения'),
                                  on_delete=models.SET_NULL,
                                  null=True)
    client = models.ForeignKey(Client, verbose_name=_(u'Группа'),
                               on_delete=models.SET_NULL,
                               null=True, blank=True)
    department = models.ForeignKey(Department,
                                   on_delete=models.SET_NULL,
                                   blank=True, null=True,
                                   verbose_name=_(u'Отдел'))
    owner = models.ForeignKey(DefaultUser,
                                null=True, blank=True,
                                on_delete=models.SET_NULL,
                                verbose_name=_(u"Ответственное лицо"))
    released = models.BooleanField(_(u'Выдан?'), default=True)

    retention_date = models.DateField(
        _(u'Дата уничтожения'),
        blank=True, null=True
    )
    fiscal_year = models.PositiveSmallIntegerField(
        default=2017,
        verbose_name=_(u'Финансовый год')
    )
    retention_policy = models.ForeignKey(RetentionPolicy,
                                         null=True, blank=True,
                                         on_delete=models.SET_NULL,
                                         verbose_name=_(u"Период хранения"))
    combined_date = models.DateField(
        _(u'Дата уничтожения общая'),
        blank=True, null=True
    )

    delete_date = models.DateTimeField(
        _(u'Дата уничтожения, выполнено'),
        blank=True, null=True
    )

class Document(TimeStampedModelIndexed, BaseParameter):
    document_type = models.ForeignKey(DocumentType,
                                #blank=True, null=True,
                                #on_delete=models.SET_NULL,
                                verbose_name=_(u'Тип документа'))
    number = models.CharField(
                                _(u'Номер'),
                                max_length=64,
                                blank=True, null=True)
    item = models.ForeignKey(Item, verbose_name=_(u'Единица хранения'),
                                on_delete=models.SET_NULL,
                                null=True)
    owner = models.ForeignKey(DefaultUser,
                                null=True, blank=True,
                                on_delete=models.SET_NULL,
                                verbose_name=_(u"Ответственное лицо"))
    uuid = UUIDField()

    search = models.TextField(_(u'Поиск'), null=True, blank=True)

    def create_index(self):
        search = ''
        try:
            if self.number:
                search = search + self.number.replace(' ', '')
            if self.name:
                search = search + self.name.replace(' ', '')
            if self.description:
                search = search + self.description.replace(' ', '')
            if self.owner:
                search = search + self.owner.__str__().replace(' ', '')
            if self.document_type:
                search = search + self.document_type.__str__().replace(' ', '')
            if self.item:
                search = search + self.item.__str__().replace(' ', '')
            if self.created:
                df = DateFormat(self.created)
                search = search + df.format(get_format('DATE_FORMAT'))
                search = search + localtime(self.created).strftime('%H:%M')
            if self.created_by != None:
                search = search + self.created_name
            if self.modified:
                df = DateFormat(self.modified)
                search = search + df.format(get_format('DATE_FORMAT'))
                search = search + localtime(self.modified).strftime('%H:%M')
            if self.modified_by != None:
                search = search + self.modified_name
        except Exception as e:
            print("LOG: %s %s ERROR: %s" % (self.__class__, inspect.stack()[0][3].upper(), e))

        if search and len(search) > 1:
            self.search = search.lower().replace(' ' , '')
        else:
            self.search = ''

        if settings.DEBUG_MESSAGES:
            print("LOG: %s SEARCH: %s" % (self.__class__, self.search))
        return self.search

    @property
    def created_name(self):
        creator = DefaultUser.objects.filter(id=self.created_by).first()
        if creator:
            return creator.username
        else:
            return ''

    @property
    def modified_name(self):
        modifier = DefaultUser.objects.filter(id=self.modified_by).first()
        if modifier:
            return modifier.username
        else:
            return ''

    def save(self, *args, **kwargs):
        self.create_index()
        super(self.__class__, self).save(*args, **kwargs)
        self.create_index()

    def __str__(self):
        return '%s "%s"' % (self.number + ' ' if
        self.number else '', self.name)

    def __unicode__(self):
        return '%s "%s"' % (self.number + ' ' if
        self.number else '', self.name)


class Slot(models.Model):
    shelf = models.ForeignKey(Shelf, on_delete=models.CASCADE,
                              verbose_name=_(u'Полка'))
    item = models.ForeignKey(Item, on_delete=models.SET_NULL,
                             blank=True, null=True,
                             verbose_name=_(u'Единица хранения'))

    number = models.PositiveSmallIntegerField(
        default=1,
        verbose_name=_(u'Номер места')
    )

    state = models.PositiveSmallIntegerField(
        choices=SLOT_STATUS,
        default=0,
        verbose_name=_(u'Статус')
    )

    def __str__(self):
        return u'Место %s [%s]' % (self.number, [r for r in SLOT_STATUS if r[0]
                                             == self.state][0][1])

    def __unicode__(self):
        return u'Место %s для "%s" [%s]' % (self.number, self.item, [r for r in SLOT_STATUS if r[0]
                                             == self.state][0][1])
    class Meta:
        unique_together = (('shelf', 'number'), ('item', 'state'), )

class FillingRule(BaseParameter, models.Model):
    uuid = UUIDField()


class Priority(BaseParameter, models.Model):
    level = models.PositiveSmallIntegerField(
        default=0,
        verbose_name=_(u'Степень')
    )
    uuid = UUIDField()


class CostCenter(BaseParameter, models.Model):
    uuid = UUIDField()


class Settings(models.Model):
    exists = models.PositiveSmallIntegerField(default=1, unique=True, null=True)
    item_unique = models.BooleanField(default=True, verbose_name=_(u'Проверка уникальности регистрационного номера ЕХ'), unique=True)
    document_unique = models.BooleanField(default=True, verbose_name=_(u'Проверка уникальности регистрационного номера Документа'), unique=True)
    paginate_by = models.PositiveSmallIntegerField(
        default=10,
        verbose_name=_(u'Записей на страницу')
    )

