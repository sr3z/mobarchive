# -*- coding: utf-8 -*-

from .models import RackGroup, Item, ItemType, Rack, Row, Shelf, Slot


def occupy_slot(slot, item):
    '''Helper function to really occupy the slot by item'''
    item.released = False
    slot.item = item
    slot.save()


def is_fits_rack(item, rack):
    '''Helper function to determine if item fits rack'''
    pass


def is_fits_shelf(item, shelf):
    '''Helter function to determine in item fits shelf'''
    pass


def check_racks(item, racks):
    '''Central function to check the rack and all the shelfs within it to
    find / check for item placement.

    Within rack we go from lower levels to upper from center of the
    shelf to the ends.

    '''
    for rack in racks:
        if is_fits_rack(item, rack):
            shelfs = rack.get_shelfs()
            for shelf in shelfs:
                # shelf validation
                pass


def place_item(rack_group, item):
    '''Place new storage item (SI) to the defined RackGroup. Find better
    option for placing within the defined RackGroup. Returns slot
    where this item should be placed, but do not places this
    item. This shoud be done by caller.

    Placing is done from static to mobile racks. From lower to upper
    shelfs and from the middle to the ends.

    '''

    if(rack_group.state in [6, 3]):
        return None

    # search for place on static racks first
    result = check_racks(item, rack_group.rack_set.filter(role__in=(1, 2)))
    if result is not None:
        return result

    # go through the mobile racks if we haven't found place on static
    # one
    result = check_racks(item, rack_group.rack_set.filter(role=0))
    if result is not None:
        return result

    return None  # No place found on this rack_group


def retrieve_item():
    '''This is function for furer implementation to do retrieve level
    optimization.

    '''
    # TODO - optimization on retrieve
    return None
