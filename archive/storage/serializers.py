# -*- coding: utf-8 -*-

from django.db import transaction
from dynamic_rest.serializers import DynamicModelSerializer, DynamicRelationField

from archive.utils.exceptions import APIException
from .models import IndicatorGroup, Indicator, RackGroup, Rack, Row, Section, Shelf


class ShelfSerializer(DynamicModelSerializer):
    '''
    shelf serializer
    '''

    class Meta: model = Shelf


class SectionSerializer(DynamicModelSerializer):
    '''
    section serializer
    '''

    shelf_set = DynamicRelationField('ShelfSerializer', many=True, embed=True, read_only=True)

    @transaction.atomic
    def create(self, validated_data):
        '''
        create minimal set for row
        '''

        try:

            section = self.Meta.model.objects.create(**validated_data)
            serializer = ShelfSerializer(
                data={
                    'name': 'Полка 1',
                    'section': section.pk
                }
            )
            if serializer.is_valid():
                serializer.save()
            else:
                raise Exception('Некорректные данные для создания дочерней полки')
        except Exception as e:
            raise APIException('Не удалось создать новую секцию', data=e)

        return section

    class Meta: model = Section


class RowSerializer(DynamicModelSerializer):
    '''
    row serializer
    '''

    indicator = DynamicRelationField('IndicatorSerializer', many=False)
    section_set = DynamicRelationField('SectionSerializer', many=True, embed=True, read_only=True)


    def is_valid(self, strict=True, **kwargs):
        '''
        validate data, but skip fields
        that can not be validated
        for example; drop 'id' check
        on creation (unique_togther constraint
        forces argument existence and breaks validation)
        '''

        if not strict:
            for f in self.Meta.drop_check:
                del self.fields[f]

        return super(DynamicModelSerializer, self).is_valid(**kwargs)

    @transaction.atomic
    def create(self, validated_data):
        '''
        create minimal set for row
        '''

        try:

            row = self.Meta.model.objects.create(**validated_data)
            serializer = SectionSerializer(
                data={
                    'name': 'Секция 1',
                    'row': row.pk
                }
            )
            if serializer.is_valid():
                serializer.save()
            else:
                raise Exception('Некорректные данные для создания дочерней секции')
        except Exception as e:
            raise APIException('Не удалось создать новый ряд', data=e)

        return row

    class Meta:
        model = Row
        drop_check = ('id',)


class RackSerializer(DynamicModelSerializer):
    '''
    rack serializer
    '''

    row_set = DynamicRelationField('RowSerializer', many=True, embed=True, read_only=True)

    @transaction.atomic
    def create(self, validated_data):
        '''
        create minimal set for rack
        '''

        try:
            rack = self.Meta.model.objects.create(**validated_data)
            rows = [
                {
                    'name': 'Левый ряд',
                },
                {
                    'name': 'Правый ряд',
                    'position': Row._positions['right']
                },
            ]
            for row in rows:
                row.update(rack=rack.pk)
                serializer = RowSerializer(data=row)
                serializer.is_valid(strict=False)
                serializer.save()
                if serializer.is_valid():
                    serializer.save()
                else:
                    raise Exception('Некорректные данные для создания дочернего ряда')
        except Exception as e:
            raise APIException('Не удалось создать новый стеллаж', data=e)

        return rack


    class Meta: model = Rack


class IndicatorGroupSerializer(DynamicModelSerializer):
    '''
    indicator group serializer
    '''

    indicator_set = DynamicRelationField('IndicatorSerializer', many=True, embed=True, read_only=True)


    class Meta: model = IndicatorGroup


class IndicatorSerializer(DynamicModelSerializer):
    '''
    indicator group serializer
    '''

    class Meta: model = Indicator


class RackGroupSerializer(DynamicModelSerializer):
    '''
    rack group serializer
    '''

    rack_set = DynamicRelationField('RackSerializer', many=True, embed=True, read_only=True)
    indicatorgroup_set = DynamicRelationField('IndicatorGroupSerializer', many=True, embed=True, read_only=True)
    blocker = DynamicRelationField('archive.user.serializers.UserSerializerMinimal', embed=True)

    @transaction.atomic
    def create(self, validated_data):
        '''
        create minimal set for rack group
        '''

        try:
            rack_group = self.Meta.model.objects.create(**validated_data)

            racks = [
                {
                    'name': 'Левый статичный стеллаж',
                    'role': Rack._roles['static_left'],
                    'disabled': Rack.disabled
                },
                {
                    'name': 'Стеллаж 1',
                    'disabled': Rack.disabled
                },
                {
                    'name': 'Правый статичный стеллаж',
                    'role': Rack._roles['static_right'],
                    'disabled': Rack.disabled
                }
            ]

            for rack in racks:
                rack.update(rack_group=rack_group.pk)
                serializer = RackSerializer(data=rack)
                if serializer.is_valid():
                    serializer.save()
                else:
                    raise Exception('Некорректные данные для создания дочернего стеллажа')
        except Exception as e:
            raise APIException('Не удалось создать новую мобильную группу', data=e)

        return rack_group

    class Meta:
        model = RackGroup
        read_only_fields = ('date_created', 'date_edited')

