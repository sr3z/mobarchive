# -*- coding: utf-8 -*-

# TODO: these tests can be placed into test_models.py later

from django.test import TestCase
from .models import RackGroup, Rack, Row, Shelf, Slot, Item, ItemType
from .placement_utils import place_item
# from .models import RACK_GROUP_STATUS


class PlacementTestCase(TestCase):
    def setUp(self):
        self.rack_group = RackGroup.objects.create(ip='192.168.1.3',
                                                   state=5, role=1)
        self.rack1 = Rack.objects.create(rack_group=self.rack_group,
                                         unit=0, role=0)
        self.rack2 = Rack.objects.create(rack_group=self.rack_group,
                                         unit=1, role=0)
        self.rack3 = Rack.objects.create(rack_group=self.rack_group,
                                         unit=2, role=0)
        self.rack4 = Rack.objects.create(rack_group=self.rack_group,
                                         unit=3, role=0)
        self.rack5 = Rack.objects.create(rack_group=self.rack_group,
                                         unit=4, role=0)
        self.rack6 = Rack.objects.create(rack_group=self.rack_group,
                                         unit=5, role=2)

        self.row1 = Row.objects.create(rack=self.rack1, position=1)
        self.row2 = Row.objects.create(rack=self.rack2, position=0)
        self.row2 = Row.objects.create(rack=self.rack2, position=1)
        

        self.rack_group_reserved = RackGroup.objects.create(ip='192.168.1.4',
                                                            state=3)

        self.rack_group.save()
        self.rack_group_reserved.save()
        self.rack1.save()
        self.rack2.save()
        self.rack3.save()
        self.rack4.save()
        self.rack5.save()
        self.rack6.save()

        self.light_item_type = ItemType()
        self.heavy_item_type = ItemType(weight=10)
        self.medium_item_type = ItemType(weight=3)

        self.light_item_type.save()
        self.heavy_item_type.save()
        self.medium_item_type.save()

    def test_reserved_rack_group(self):
        item = Item()
        self.assertIs(place_item(self.rack_group_reserved, item), None)

    def test_placement(self):
        pass
