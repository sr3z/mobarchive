# coding: utf-8
from django.conf.urls import include, url
from archive.storage.views import ItemIn, ItemInSelect, ItemInDocs, ItemInCreate, ItemOut, ItemOutSelect, ItemOutDocs, ItemOutCreate, \
        RackList, RackCreate, RackEdit, RackDelete, RackDocs, \
        WaitinglistList, DestroylistList, \
        RackGroupList, RackGroupEdit, RackGroupDelete, RackGroupDocs, RackGroupCreate, SettingsEdit, SettingsList, \
        MyIssuedItemList, MyWaitingList, DocumentList, DocumentDocs, DocumentCreate, DocumentEdit, DocumentDelete, \
        ClientList, ClientDocs, ClientCreate, ClientEdit, ClientDelete, BarCode, \
        ItemCreate, ItemList, ItemEdit, ItemDelete, ItemDetails, ItemAccount, ItemDocs, \
        DepartmentList, DepartmentDocs, DepartmentCreate, DepartmentEdit, DepartmentDelete, \
        ClientTypeList, ClientTypeCreate, ClientTypeEdit, ClientTypeDelete, ClientTypeDocs, FileTypeList, \
        FillingRuleList, ItemTypeList, ItemTypeCreate, ItemTypeEdit, ItemTypeDelete, ItemTypeDocs, \
        PriorityList, DocumentTypeList, DocumentTypeCreate, DocumentTypeEdit, DocumentTypeDelete, DocumentTypeDocs, CostCenterList,\
        RetentionPolicyList, RetentionPolicyCreate, RetentionPolicyEdit, RetentionPolicyDelete, RetentionPolicyDocs, \
        CommandList, CommandCreate, CommandEdit, CommandDelete, CommandDocs

urlpatterns = (
    url(r'^itemout/$', ItemOut.as_view(), name='itemout'),
    url(r'^itemout/docs/$', ItemOutDocs.as_view(), name='itemout_docs'),
    url(r'^itemout/select/$', ItemOutSelect.as_view(), name='itemout_select'),
    url(r'^itemout/create/$', ItemOutCreate.as_view(), name='itemout_create'),
    url(r'^itemin/$', ItemIn.as_view(), name='itemin'),
    url(r'^itemin/docs/$', ItemInDocs.as_view(), name='itemin_docs'),
    url(r'^itemin/select/$', ItemInSelect.as_view(), name='itemin_select'),
    url(r'^itemin/create/$', ItemInCreate.as_view(), name='itemin_create'),

    url(r'^waitinglist/$', WaitinglistList.as_view(), name='waitinglist'),

    url(r'^rack/create/$', RackCreate.as_view(), name='rack_create'),
    url(r'^rack/edit/(?P<pk>\d+)/$',
        RackEdit.as_view(), name='rack_edit'),
    url(r'^rack/delete/(?P<pk>\d+)/$',
        RackDelete.as_view(), name='rack_delete'),
    url(r'^rack/docs/$', RackDocs.as_view(), name='rack_docs'),
    url(r'^rack/(?P<group>\d*)?', RackList.as_view(), name='racks'),

    url(r'^destroylist/$', DestroylistList.as_view(), name='destroylist'),

    url(r'^rackgroup/$', RackGroupList.as_view(), name='rackgroups'),
    url(r'^rackgroup/create/$', RackGroupCreate.as_view(), name='rackgroup_create'),
    url(r'^rackgroup/edit/(?P<pk>\d+)/$',
        RackGroupEdit.as_view(), name='rackgroup_edit'),
    url(r'^rackgroup/delete/(?P<pk>\d+)/$',
        RackGroupDelete.as_view(), name='rackgroup_delete'),
    url(r'^rackgroup/docs/$', RackGroupDocs.as_view(), name='rackgroup_docs'),

    url(r'^command/$', CommandList.as_view(), name='command'),
    url(r'^command/create/$', CommandCreate.as_view(), name='command_create'),
    url(r'^command/edit/(?P<pk>\d+)/$',
        CommandEdit.as_view(), name='command_edit'),
    url(r'^command/delete/(?P<pk>\d+)/$',
        CommandDelete.as_view(), name='command_delete'),
    url(r'^command/docs/$', CommandDocs.as_view(), name='command_docs'),

    url(r'^barcode/(?P<code>\d*)?', BarCode.as_view(), name='barcode'),

    url(r'^settings/$', SettingsList.as_view(), name='settings'),
    url(r'^settings/edit/(?P<pk>\d+)/$',
        SettingsEdit.as_view(), name='settings_edit'),

    url(r'^item/$', ItemList.as_view(), name='items'),
    url(r'^item/create/$', ItemCreate.as_view(), name='item_create'),
    url(r'^item/edit/(?P<pk>\d+)/$',
        ItemEdit.as_view(), name='item_edit'),
    url(r'^item/delete/(?P<pk>\d+)/$',
        ItemDelete.as_view(), name='item_delete'),
    url(r'^item/details/(?P<pk>\d+)/$',
        ItemDetails.as_view(), name='item_details'),
    url(r'^item/account/(?P<pk>\d+)/$',
        ItemAccount.as_view(), name='item_account'),
    url(r'^item/docs/$', ItemDocs.as_view(), name='item_docs'),

    url(r'^client/$', ClientList.as_view(), name='clients'),
    url(r'^client/create/$', ClientCreate.as_view(), name='clients_create'),
    url(r'^client/edit/(?P<pk>\d+)/$',
        ClientEdit.as_view(), name='clients_edit'),
    url(r'^client/delete/(?P<pk>\d+)/$',
        ClientDelete.as_view(), name='clients_delete'),
    url(r'^client/docs/$', ClientDocs.as_view(), name='clients_docs'),

    url(r'^client_type/$', ClientTypeList.as_view(), name='client_types'),
    url(r'^client_type/create/$', ClientTypeCreate.as_view(), name='client_type_create'),
    url(r'^client_type/edit/(?P<pk>\d+)/$',
        ClientTypeEdit.as_view(), name='client_type_edit'),
    url(r'^client_type/delete/(?P<pk>\d+)/$',
        ClientTypeDelete.as_view(), name='client_type_delete'),
    url(r'^client_type/docs/$', ClientTypeDocs.as_view(), name='client_type_docs'),

    url(r'^document/$', DocumentList.as_view(), name='documents'),
    url(r'^document/create/$', DocumentCreate.as_view(), name='document_create'),
    url(r'^document/edit/(?P<pk>\d+)/$',
        DocumentEdit.as_view(), name='document_edit'),
    url(r'^document/delete/(?P<pk>\d+)/$',
        DocumentDelete.as_view(), name='document_delete'),
    url(r'^document/docs/$', DocumentDocs.as_view(), name='document_docs'),

    url(r'^my_waitings/$', MyWaitingList.as_view(), name='my_waitings'),

    url(r'^my_issued_items/$', MyIssuedItemList.as_view(), name='my_issued_items'),

    url(r'^department/$', DepartmentList.as_view(), name='departments'),
    url(r'^department/create/$', DepartmentCreate.as_view(), name='department_create'),
    url(r'^department/edit/(?P<pk>\d+)/$',
        DepartmentEdit.as_view(), name='department_edit'),
    url(r'^department/delete/(?P<pk>\d+)/$',
        DepartmentDelete.as_view(), name='department_delete'),
    url(r'^department/docs/$', DepartmentDocs.as_view(), name='department_docs'),

    url(r'^filling_rules/$', FillingRuleList.as_view(), name='filling_rules'),

    url(r'^item_type/$', ItemTypeList.as_view(), name='item_types'),
    url(r'^item_type/create/$', ItemTypeCreate.as_view(), name='item_type_create'),
    url(r'^item_type/edit/(?P<pk>\d+)/$',
        ItemTypeEdit.as_view(), name='item_type_edit'),
    url(r'^item_type/delete/(?P<pk>\d+)/$',
        ItemTypeDelete.as_view(), name='item_type_delete'),
    url(r'^item_type/docs/$', ItemTypeDocs.as_view(), name='itemtype_docs'),

    url(r'^priority/$', PriorityList.as_view(), name='priorities'),

    url(r'^document_type/$', DocumentTypeList.as_view(), name='document_types'),
    url(r'^document_type/create/$', DocumentTypeCreate.as_view(), name='document_type_create'),
    url(r'^document_type/edit/(?P<pk>\d+)/$',
        DocumentTypeEdit.as_view(), name='document_type_edit'),
    url(r'^document_type/delete/(?P<pk>\d+)/$',
        DocumentTypeDelete.as_view(), name='document_type_delete'),
    url(r'^document_type/docs/$', DocumentTypeDocs.as_view(), name='document_type_docs'),

    url(r'^cost_centers/$', CostCenterList.as_view(), name='cost_centers'),

    url(r'^retention_policies/$', RetentionPolicyList.as_view(), name='retention_policies'),
    url(r'^retention_policies/create/$', RetentionPolicyCreate.as_view(), name='retention_policies_create'),
    url(r'^retention_policies/edit/(?P<pk>\d+)/$',
        RetentionPolicyEdit.as_view(), name='retention_policies_edit'),
    url(r'^retention_policies/delete/(?P<pk>\d+)/$',
        RetentionPolicyDelete.as_view(), name='retention_policies_delete'),
    url(r'^retention_policies/docs/$', RetentionPolicyDocs.as_view(), name='retention_policies_docs'),
)
