# -*- coding: utf-8 -*-

import inspect
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse_lazy
from django.utils.http import urlencode
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import Permission, User as DefaultUser, Group
from django.utils.translation import ugettext as _
from django.utils import timezone
from django.contrib import messages
from django.contrib.auth import logout, login
from django.views.generic import CreateView, DeleteView, ListView, UpdateView, DetailView
from braces.views import SuperuserRequiredMixin, PrefetchRelatedMixin, LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django import forms
from django.views.decorators.csrf import csrf_exempt
from dateutil.parser import parse
from django.db import transaction
from django.db.models import Q, Max, Min

import json
import os
import barcode
from barcode.writer import ImageWriter
from functools import reduce
from django.forms.formsets import BaseFormSet, formset_factory
from django.shortcuts import render_to_response

from archive.users.models import ArcUser
from archive.settings.base import STATIC_FILES
from archive.utils.logger import log_debug, log_info, log_error, log_warning
from archive.utils.helpers import djqs_from_search
from archive.api.models import Task, Mission
from archive.api.tasks import run_mission_task

from archive.utils.decorators import allow_access, set_created, set_modified

from .models import Row, Rack, RackGroup, ItemType, Item, ClientType, Client, \
                    Slot, Shelf, Document, DocumentType, Department, Command, \
                    Settings, RetentionPolicy, PERIOD_COLOR
from .forms import RackGroupForm, ItemForm, ItemTypeForm, RowForm, \
                   DocumentForm, DocumentTypeForm, CommandForm, \
                   SettingsForm, Section, \
                   DepartmentForm, ClientForm, ClientTypeForm, RetentionPolicyForm

def get_context(self_class, myfilter=None, offset=None, limit=None, **kwargs):
    context = super(self_class.__class__, self_class).get_context_data(**kwargs)
    self_class.search = self_class.request.GET.get('s', None)

    if self_class.search:
        self_class.search = self_class.search.replace(' ' , '')
        print('LOG: %s "%s": SEARCH: %s MYFILTER: %s' % (self_class.__class__, inspect.stack()[0][3], self_class.search, myfilter))
        if myfilter:
            if self_class.model == Mission:
                myfilter['task__search__contains'] = self_class.search.lower()
            else:
                myfilter['search__contains'] = self_class.search.lower()
            objects_all = self_class.model.objects.filter(**myfilter)
        else:
            print('LOG: %s "%s": %s MODEL: %s' % (self_class.__class__, inspect.stack()[0][3], self_class.search, self_class.model))
            if self_class.model == Mission:
                objects_all = self_class.model.objects.filter(task__search__contains=self_class.search.lower())
            else:
                objects_all = self_class.model.objects.filter(search__contains=self_class.search.lower())
        #objects_all = djqs_from_search(self_class.model, self_class.search)
    else:
        if myfilter:
            objects_all = self_class.model.objects.filter(**myfilter)
        else:
            objects_all = self_class.model.objects.all()
    if limit:
        if not offset:
            offset = 0
        context['object_list'] = objects_all[offset:limit]
    else:
        context['object_list'] = objects_all
    return context

class BaseStorage(LoginRequiredMixin, ListView):
    login_url = '/login/'

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        if not hasattr(request, 'user'):
            print("LOG: ERROR: missing 'user' attr in request\n") 
            logout(request)
        return super(BaseStorage, self).dispatch(request, *args, **kwargs)

class WaitinglistList(BaseStorage, ListView):
    model = Mission
    template_name = 'storage/waitinglist/waitinglist.html'

class DestroylistList(BaseStorage, ListView):
    model = Mission
    template_name = 'storage/destroylist.html'

#################################################################
class ItemOutDocs(BaseStorage, ListView):
    model = Mission
    template_name = 'storage/task/details.html'

    def get_context_data(self, **kwargs):
        myfilter = {'name__in': ['OUT', 'REMOVE'], 'status': 'inprogress', 'owner': self.request.user}
        context = get_context(self, myfilter)
        missions = context['object_list'] 
        if missions and missions.count() and missions.filter(task__status='done').count() > 0:
            context['has_done'] = True

        if missions.filter(task__status='inprogress').count() > 0:
            if missions.filter(task__status='inprogress').first().task_set.filter(status='inprogress').count() > 0:
                context['next_task'] = missions.filter(task__status='inprogress').first().task_set.filter(status='inprogress').first().id
        return context

    @allow_access([1, 2])
    def get(self, request, *args, **kwargs):
        return super(self.__class__, self).get(request, *args, **kwargs)

class ItemOutSelect(BaseStorage, ListView):
    model = Mission
    template_name = 'storage/task/doc_select.html'
    doc_search = ''
    item_search = ''

    def get_queryset(self):
        return super(self.__class__, self).get_queryset().filter(name__in=['OUT', 'REMOVE'], status='inprogress', owner=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)

        if 'i' in self.request.GET:
            self.item_search = self.request.GET.get('i', '')
            self.template_name = 'storage/task/item_select.html'
        elif 'all' in self.request.GET:
            self.item_search = self.request.GET.get('all', '')
            self.template_name = 'storage/task/select_item_for_docs.html'
        elif self.request.GET.get('s', None):
            self.doc_search = self.request.GET.get('s', '')
        print("LOG: %s: CONTEXT: SEARCH: DOC: %s ITEM: %s" % (self.__class__, self.doc_search, self.item_search))

        if 'all' in self.request.GET:
            items = Item.objects.filter(enabled=True).order_by('number')
            docs = []
        elif self.get_queryset().count() < 1:
            docs = Document.objects.filter(item__released=False).values('id', 'number', 'name').order_by('number')
            items = Item.objects.filter(released=False).order_by('number')
        else:
            tasks = self.get_queryset().values('task__item')
            ids = [d['task__item'] for d in tasks if d['task__item']]
            print("LOG: %s: CONTEXT: ITEMS:%s" % (self.__class__, ids))
            docs = Document.objects.filter(item__released=False).exclude(item__in=ids).values('id', 'number', 'name').order_by('number')
            items = Item.objects.filter(released=False).exclude(id__in=ids).order_by('number')

        itemnum_list = []
        item_list = []
        docnum_list = []
        doc_list = []

        for doc in docs:
            if self.doc_search:
                if not (self.doc_search in str(doc['number']) or self.doc_search in doc['name'] or self.doc_search in doc['number']):
                    continue
            doc_list.append((str(doc['id']), doc['name']))
            docnum_list.append((str(doc['id']), doc['number']))

        for item in items:
            if self.item_search:
                if not (self.item_search in str(item.number) or self.item_search in item.name):
                        continue
            itemnum_list.append((str(item.id), item.number))
            item_list.append((str(item.id), item.name, item))

        if not self.doc_search:
            doc_list.insert(0, (-1, '-------'))
            docnum_list.insert(0, (-1, '-------'))

        if not self.item_search:
            itemnum_list.insert(0, (-1, '-------'))
            item_list.insert(0, (-1, '-------'))

        context['docnums'] = docnum_list
        context['docs'] = doc_list
        context['itemnums'] = itemnum_list
        context['items'] = item_list
        context['elements_count'] = len(item_list)
        #print("LOG: %s: CONTEXT: %s" % (self.__class__, context))

        return context

class ItemOut(BaseStorage, ListView):
    model = Mission
    template_name = 'storage/task/outlist.html'
    success_url = reverse_lazy('storage:itemout')

    def get_queryset(self):
        return super(self.__class__, self).get_queryset().filter(name__in=['OUT', 'REMOVE'], status='inprogress', owner=self.request.user)

    @allow_access([1, 2, 3])
    def get(self, request, *args, **kwargs):
        return super(self.__class__, self).get(request, *args, **kwargs)

    @allow_access([1, 2, 3], 'storage:itemout')
    def post(self, request, *args, **kwargs):
        print("LOG: %s %s %s" % (self.__class__, inspect.stack()[0][3].upper(), request.POST))
        if self.request.POST.get('remove_tasks', None):
            missions = self.get_queryset()

            if missions and missions.count():
                for mission in missions:
                    if mission.task_set.filter(status='done').count() > 0:
                        continue
                    else:
                        tasks = mission.task_set.all()
                        for task in tasks:
                            task.delete()
                        mission.delete()
            return HttpResponseRedirect(reverse_lazy('storage:itemout'))
        elif self.request.POST.get('print_pdf', None):
            return HttpResponseRedirect(reverse_lazy('reports:outtasks_pdf'))
        return HttpResponseRedirect(reverse_lazy('storage:itemout_create'))

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)
        missions = self.get_queryset()

        if missions and missions.count() and missions.filter(task__status='done').count() > 0:
            context['has_done'] = True

        if missions.filter(task__status='inprogress').count() > 0:
            if missions.filter(task__status='inprogress').first().task_set.filter(status='inprogress').count() > 0:
                context['next_task'] = missions.filter(task__status='inprogress').first().task_set.filter(status='inprogress').first().id
        return context

class ItemOutCreate(CreateView):
    model = Mission
    template_name = 'storage/task/create_itemout.html'
    doc_search = ''
    item_search = ''
    fields = '__all__'

    def get_queryset(self):
        return super(ItemOutCreate, self).get_queryset().filter(name__in=['OUT', 'REMOVE'], status='inprogress', owner=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(ItemOutCreate, self).get_context_data(**kwargs)
        context['error_message'] = self.request.GET.get('error', '')
        self.item_list = []
        doc_list = []
        docnum_list = []
        itemnum_list = []

        missions = self.get_queryset()
        tasks = [t['task'] for t in missions.values('task') if t['task']]
        print("LOG: ITEMOUT: CONTEXT: MISSIONS:", missions.count(), " TASKS:", len(tasks))

        self.person_id = self.request.GET.get('person', None)
        self.item_id = self.request.GET.get('item', None)
        self.doc_id = self.request.GET.get('doc', None)

        if self.item_id:
            context['item_id'] = self.item_id

        if self.doc_id:
            context['doc_id'] = self.doc_id

        if self.person_id:
            context['person_id'] = self.person_id

        if missions.count() < 1 or len(tasks) < 1:
            if self.item_id:
                docs = Document.objects.filter(item__released=False, item__id=self.item_id).values('id', 'number', 'name')
            else:
                docs = Document.objects.filter(item__released=False).values('id', 'number', 'name')

            for doc in docs:
                doc_list.append((str(doc['id']), doc['name']))
                docnum_list.append((str(doc['id']), doc['number']))
                print("LOG: ITEMOUT: CONTEXT: ALL DOCS:", doc)

            items = Item.objects.filter(released=False).values('id', 'number', 'name')

            for item in items:
                itemnum_list.append((str(item['id']), item['number']))
                self.item_list.append((str(item['id']), item['name'] if item['name'] else u'-= Без имени =-'))
                print("LOG: ITEMOUT: CONTEXT: ALL ITEMS:", item)
        else:
            items = self.get_queryset().values('task__item')
            ids = [d['task__item'] for d in items if d['task__item']]
            print("LOG: %s: CONTEXT: SELECTED ITEMS: %s" % (self.__class__, ids))

            if self.item_id:
                docs = Document.objects.filter(item__released=False, item__id=self.item_id).values('id', 'number', 'name')
            else:
                docs = Document.objects.filter(item__released=False).values('id', 'number', 'name')

            for doc in docs:
                doc_list.append((str(doc['id']), doc['name']))
                docnum_list.append((str(doc['id']), doc['number']))

            items = Item.objects.filter(released=False).exclude(id__in=ids).values('id', 'number', 'name')

            for item in items:
                itemnum_list.append((str(item['id']), item['number']))
                self.item_list.append((str(item['id']), item['name'] if item['name'] else u'-= Без имени =-'))
                print("LOG: ITEMOUT: CONTEXT: SELECTED ITEMS:", ids)

        if not self.doc_search:
            doc_list.insert(0, (-1, '-------'))
            docnum_list.insert(0, (-1, '-------'))

        if not self.item_search:
            itemnum_list.insert(0, (-1, '-------'))
            self.item_list.insert(0, (-1, '-------'))

        context['docnums'] = docnum_list
        context['docs'] = doc_list
        context['itemnums'] = itemnum_list
        context['items'] = self.item_list

        self.per_list = []

        pers = DefaultUser.objects.all()

        if pers.count() > 0:
            self.per_list = [(str(per.id), per) for per in pers]
            self.per_list.insert(0, (-1, '-------'))

        context['pers'] = self.per_list

        return context

    @allow_access([1, 2, 3], 'storage:itemout')
    def post(self, request, *args, **kwargs):
        if not 'add_item' in request.POST:
            return HttpResponseRedirect(self.get_update_url())
        print("LOG: TASK: ItemOutCreate POST", request.POST)

        try:
            self.item_id = int(request.POST.get('item', None))
        except Exception as e:
            self.item_id = None 

        try:
            self.doc_id = int(request.POST.get('doc', None))
        except Exception as e:
            self.doc_id = None 

        if not self.doc_id and not self.item_id:
            print("LOG: ITEMOUT POST ERROR: DOC_ID: ", self.doc_id, " ITEM_ID: ",  self.item_id)
            return HttpResponseRedirect(self.get_update_url(error_message=u'Необходимо выбрать документ или ЕХ'))

        try:
            person = int(self.request.POST.get('person'))
            if person == -1:
                return HttpResponseRedirect(self.get_update_url(error_message=u'Необходимо выбрать Пользователя'))
        except Exception as e:
            person = None
            print("LOG: ITEMOUT POST PERSON ERROR: ", e)
            return HttpResponseRedirect(self.get_update_url(error_message=u'Необходимо выбрать Пользователя'))

        self.comment = request.POST.get('comment', None)

        if request.POST.get('remove_item', None):
            mission_name = 'REMOVE'
        else:
            mission_name = 'OUT'

        # check if mission exists, create new if not
        missions = Mission.objects.filter(name=mission_name, status='inprogress', owner=self.request.user)

        if missions and missions.count() > 0:
            mission = missions.last()
        else:
            mission = Mission.objects.create(name=mission_name, owner=request.user, status='inprogress')

        # assert if document Item exists
        if self.doc_id:
            items = Item.objects.filter(document__id=self.doc_id) 
        elif self.item_id:
            items = Item.objects.filter(pk=self.item_id) 
        else:
            items = None

        if not items or items.count() < 1:
            print("LOG: INTEMOUT POST: items not found ( id :", self.item_id, " )")
            return HttpResponseRedirect(self.get_update_url(error_message=u'Документ или ЕХ не найден'))
        item = items.last()

        tasks = Task.objects.filter(mission=mission, item=item, status='inprogress')

        if tasks.count() > 0:
            print("LOG: INTEMOUT POST: task already exits")
            return HttpResponseRedirect(self.get_success_url())

        status = 'inprogress'

        # check Item's allocation 
        slots = Slot.objects.filter(item=item)

        if slots and slots.count() > 0:
            slot = slots.last()

        task = Task.objects.create(mission=mission, item=item, status=status, owner_id=person, description=self.comment) 

        return HttpResponseRedirect(self.get_success_url(mission_name=mission_name))

    def get_success_url(self, doc=None, mission_name=None):
        q=None
        if mission_name == 'REMOVE':
            q = '?remove_item'
        doclist = ''
        search = ''

        return reverse_lazy('storage:itemout')

    def get_update_url(self, error_message=None):
        return reverse_lazy('storage:itemout_create') + '?d=&%s' % urlencode({'error': error_message}) if error_message else ''


#################################################################
class ItemInDocs(BaseStorage, ListView):
    model = Mission
    template_name = 'storage/task/details.html'

    def get_context_data(self, **kwargs):
        myfilter = {'name':'IN', 'status': 'inprogress', 'owner': self.request.user}
        context = get_context(self, myfilter)
        missions = context['object_list'] 
        if missions and missions.count() and missions.filter(task__status='done').count() > 0:
            context['has_done'] = True

        if missions.filter(task__status='inprogress').count() > 0:
            if missions.filter(task__status='inprogress').first().task_set.filter(status='inprogress').count() > 0:
                context['next_task'] = missions.filter(task__status='inprogress').first().task_set.filter(status='inprogress').first().id
        return context

    @allow_access([1, 2])
    def get(self, request, *args, **kwargs):
        return super(self.__class__, self).get(request, *args, **kwargs)

class ItemInSelect(BaseStorage, ListView):
    model = Mission
    template_name = 'storage/task/doc_select.html'
    doc_search = ''
    item_search = ''
    person_search = ''

    def get_queryset(self):
        return super(self.__class__, self).get_queryset().filter(name='IN', status='inprogress', owner=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)

        if 'i' in self.request.GET:
            self.item_search = self.request.GET.get('i', '')
            self.template_name = 'storage/task/item_select.html'

        elif 'p' in self.request.GET:
            self.person_search = self.request.GET.get('p', '')
            self.template_name = 'storage/task/person_select.html'

        elif self.request.GET.get('s', None):
            self.doc_search = self.request.GET.get('s', '')

        print("LOG: %s: CONTEXT: SEARCH: DOC: %s ITEM: %s PERSON: %s [%s]" % \
                (self.__class__, self.doc_search, self.item_search, self.person_search, \
                self.template_name ))

        if self.get_queryset().count() < 1:
            docs = Document.objects.filter(item__released=True).values('id', 'number', 'name').order_by('number')
            items = Item.objects.filter(released=True).values('id', 'number', 'name').order_by('number')
        else:
            tasks = self.get_queryset().values('task__item')
            ids = [d['task__item'] for d in tasks if d['task__item']]
            print("LOG: %s: CONTEXT: ITEMS:%s" % (self.__class__, ids))
            docs = Document.objects.filter(item__released=True).exclude(item__in=ids).values('id', 'number', 'name').order_by('number')
            items = Item.objects.filter(released=True).exclude(id__in=ids).values('id', 'number', 'name').order_by('number')

        itemnum_list = []
        item_list = []
        docnum_list = []
        doc_list = []
        pers_list = []

        for doc in docs:
            if self.doc_search:
                if self.doc_search in str(doc['id']) or self.doc_search in doc['name'] or self.doc_search in doc['number']:
                    doc_list.append((str(doc['id']), doc['name']))
                    docnum_list.append((str(doc['id']), doc['number']))
            else:
                doc_list.append((str(doc['id']), doc['name']))
                docnum_list.append((str(doc['id']), doc['number']))

        for item in items:
            if self.item_search:
                if self.item_search in str(item['number']) or self.item_search in item['name']:
                    itemnum_list.append((str(item['id']), item['number']))
                    item_list.append((str(item['id']), item['name'] if item['name'] else u'-= Без имени =-'))
            else:
                itemnum_list.append((str(item['id']), item['number']))
                item_list.append((str(item['id']), item['name'] if item['name'] else u'-= Без имени =-'))

        if not self.doc_search:
            doc_list.insert(0, (-1, '-------'))
            docnum_list.insert(0, (-1, '-------'))

        if not self.item_search:
            itemnum_list.insert(0, (-1, '-------'))
            item_list.insert(0, (-1, '-------'))

        pers = DefaultUser.objects.all().values('id', 'arcuser__role', 'username').order_by('arcuser__role')

        if pers.count() > 0:
            for per in pers:
                if self.person_search:
                    if self.person_search in per['username']:
                        pers_list.append((per['id'], per['username']))
                else:
                    pers_list.append((per['id'], per['username']))

        if not self.person_search:
            pers_list.insert(0, (-1, '-------'))

        context['pers'] = pers_list
        context['docnums'] = docnum_list
        context['docs'] = doc_list
        context['itemnums'] = itemnum_list
        context['items'] = item_list
        #print("LOG: %s: CONTEXT: %s" % (self.__class__, context))

        return context


class ItemIn(ListView):
    model = Mission
    template_name = 'storage/task/inlist.html'
    success_url = reverse_lazy('storage:itemin')

    def get_queryset(self):
        return super(self.__class__, self).get_queryset().filter(name='IN', status='inprogress', owner=self.request.user)

    @allow_access([1, 2, 3])
    def get(self, request, *args, **kwargs):
        return super(self.__class__, self).get(request, *args, **kwargs)

    @allow_access([1, 2, 3], 'storage:itemin')
    def post(self, request, *args, **kwargs):
        print("LOG: %s %s %s" % (self.__class__, inspect.stack()[0][3].upper(), request.POST))
        if self.request.POST.get('remove_tasks', None):
            missions = self.get_queryset()

            if missions and missions.count():
                for mission in missions:
                    if mission.task_set.filter(status='done').count() > 0:
                        continue
                    else:
                        tasks = mission.task_set.all()
                        for task in tasks:
                            task.delete()
                        mission.delete()
            return HttpResponseRedirect(reverse_lazy('storage:itemin'))
        elif self.request.POST.get('print_pdf', None):
            return HttpResponseRedirect(reverse_lazy('reports:intasks_pdf'))
        return HttpResponseRedirect(reverse_lazy('storage:itemin_create'))

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)
        missions = self.get_queryset()

        if missions and missions.count() and missions.filter(task__status='done').count() > 0:
            context['has_done'] = True

        if missions.filter(task__status='inprogress').count() > 0:
            if missions.filter(task__status='inprogress').first().task_set.filter(status='inprogress').count() > 0:
                context['next_task'] = missions.filter(task__status='inprogress').first().task_set.filter(status='inprogress').first().id
        return context


class ItemInCreate(CreateView):
    model = Mission
    template_name = 'storage/task/create_itemin.html'
    row_id = None
    fields = '__all__'

    def get_queryset(self):
        return super(ItemInCreate, self).get_queryset().filter(name='IN', status='inprogress', owner=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(ItemInCreate, self).get_context_data(**kwargs)
        context['error_message'] = self.request.GET.get('error', '')

        self.row_id = self.request.GET.get('row', None)
        self.section_id = self.request.GET.get('section', None)
        self.shelf_id = self.request.GET.get('shelf', None)
        self.slot_id = self.request.GET.get('slot', None)
        self.item_id = self.request.GET.get('item', None)
        self.doc_id = self.request.GET.get('doc', None)
        self.person_id = self.request.GET.get('person', None)
        self.rackgroup_id = self.request.GET.get('rackgroup', None)

        if '-1' == self.rackgroup_id:
            self.rackgroup_id = None

        if '-1' == self.row_id:
            self.row_id = None
        if '-1' == self.section_id:
            self.section_id = None
        if '-1' == self.shelf_id:
            self.shelf_id = None
        if '-1' == self.slot_id:
            self.slot_id = None

        if self.item_id:
            context['item_id'] = self.item_id

        if self.doc_id:
            context['doc_id'] = self.doc_id

        if self.person_id:
            context['person_id'] = self.person_id

        self.sections_set = []
        self.shelf_list = []
        itemnum_list = []
        self.item_list = []
        docnum_list = []
        self.doc_list = []
        self.per_list = []

        missions = self.get_queryset()
        tasks = [t['task'] for t in missions.values('task') if t['task']]
        print("LOG: %s %s MISSIONS: %s TASKS: %s" % (self.__class__, inspect.stack()[0][3].upper(), missions.count(), len(tasks)))

        if missions.count() < 1 or len(tasks) < 1:
            if self.item_id:
                docs = Document.objects.filter(item__released=True, item__id=self.item_id).values('id', 'number', 'name')
            else:
                docs = Document.objects.filter(item__released=True).values('id', 'number', 'name')

            for doc in docs:
                docnum_list.append((str(doc['id']), doc['number']))
                self.doc_list.append((str(doc['id']), doc['name']))
                print("LOG: ITEMIN: CONTEXT: ALL DOCS:", doc)

            items = Item.objects.filter(released=True).values('id', 'number', 'name')

            for item in items:
                self.item_list.append((str(item['id']), item['name'] if item['name'] else u'-= Без имени =-'))
                itemnum_list.append((str(item['id']), item['number']))
                print("LOG: ITEMIN: CONTEXT: ALL ITEMS:", item)
        else:
            items = self.get_queryset().values('task__item')
            ids = [d['task__item'] for d in items if d['task__item']]
            print("LOG: %s: CONTEXT: SELECTED DOCS: %s" % (self.__class__, ids))

            if self.item_id:
                docs = Document.objects.filter(item__released=True, item__id=self.item_id).values('id', 'number', 'name')
            else:
                docs = Document.objects.filter(item__released=True).values('id', 'number', 'name')

            for doc in docs:
                self.doc_list.append((str(doc['id']), doc['name']))
                docnum_list.append((str(doc['id']), doc['number']))

            items = Item.objects.filter(released=True).exclude(id__in=ids).values('id', 'number', 'name')

            for item in items:
                self.item_list.append((str(item['id']), item['name'] if item['name'] else u'-= Без имени =-'))
                itemnum_list.append((str(item['id']), item['number']))

        itemnum_list.insert(0, (-1, '-------'))
        self.item_list.insert(0, (-1, '-------'))
        docnum_list.insert(0, (-1, '-------'))
        self.doc_list.insert(0, (-1, '-------'))
        context['itemnums'] = itemnum_list
        context['docnums'] = docnum_list
        context['docs'] = self.doc_list
        context['items'] = self.item_list

        if self.item_id and (not self.rackgroup_id or not self.row_id or not self.section_id or not self.shelf_id or not self.slot_id):
            items = Item.objects.filter(id=self.item_id)
            print("LOG: %s item_id: %s" % (self.__class__, self.item_id))
            if items and items.count() > 0:
                item = items.first()

                if not self.rackgroup_id:
                    if item.slot_set.all().first():
                        self.rackgroup_id = item.slot_set.all().first().shelf.section.row.rack.rack_group.id 
                        print("LOG: %s rackgroup_id: %s" % (self.__class__, self.rackgroup_id))

                        if not self.row_id:
                            self.row_id = item.slot_set.all().first().shelf.section.row.id 
                            print("LOG: %s row_id: %s" % (self.__class__, self.row_id))

                        if not self.section_id:
                            self.section_id = item.slot_set.all().first().shelf.section.id 
                            print("LOG: %s section_id: %s" % (self.__class__, self.section_id))

                        if not self.shelf_id:
                            self.shelf_id = item.slot_set.all().first().shelf.id 
                            print("LOG: %s shelf_id: %s" % (self.__class__, self.shelf_id))

                        if not self.slot_id:
                            self.slot_id = item.slot_set.all().first().number
                            print("LOG: %s slot_id: %s" % (self.__class__, self.slot_id))

        rack_group_set = [(str(rg.id), rg) for rg in RackGroup.objects.filter(enabled=True)]
        rack_group_set.insert(0, (-1, '-------'))
        context['rackgroups'] = forms.ChoiceField(
                                    required=False,
                                    choices=rack_group_set,
                                )

        if self.rackgroup_id:
            context['rackgroup_id'] = str(self.rackgroup_id)
            self.rows_set = [(str(row.id), row, row.get_row_number, row.get_pass_number) for row in Row.objects.filter(rack__rack_group__id=self.rackgroup_id)]
        else:
            self.rows_set = []#(str(row.id), row, row.get_row_number, row.get_pass_number) for row in Row.objects.all()]

        self.rows_set.insert(0, (-1, '-------'))
        self.rows = forms.ChoiceField(
            required=False,
            choices=self.rows_set,
        )
        context['rows'] = self.rows

        if self.row_id:
            context['row_id'] = str(self.row_id)
            rows = Row.objects.filter(id=self.row_id)

            if rows.count() > 0:
                row = rows.last()
                sections = Section.objects.filter(row=row)

                if sections.count() > 0:
                    self.sections_set = [(str(sec.id), sec) for sec in sections]
                    self.sections_set.insert(0, (-1, '-------'))

                    if self.section_id:
                        shelves = Shelf.objects.filter(section_id=self.section_id)

                        if shelves.count() > 0:
                            for shelf in shelves:
                                self.shelf_list.append((str(shelf.id), shelf))

                                if self.item_id and self.shelf_id and self.shelf_id != '-1':
                                    slots = Slot.objects.filter(item__id=self.item_id, shelf__id=shelf.id)

                                    if not self.slot_id and slots and slots.count() > 0: 
                                        self.slot_id = slots.first().number

                            #self.shelf_list = [(str(shelf.id), shelf) for shelf in shelves]
                            self.shelf_list.insert(0, (-1, '-------'))

        self.sections = forms.ChoiceField(
            required=False,
            choices=self.sections_set,
        )
        context['sections'] = self.sections

        if self.section_id:
            context['section_id'] = str(self.section_id)

        self.shelf = forms.ChoiceField(
            required=False,
            choices=self.shelf_list,
        )
        context['shelf'] = self.shelf

        if self.shelf_id:
            context['shelf_id'] = str(self.shelf_id)

        if self.slot_id:
            context['slot_id'] = self.slot_id

        pers = DefaultUser.objects.all()

        if pers.count() > 0:
            self.per_list = [(str(per.id), per) for per in pers]
            self.per_list.insert(0, (-1, '-------'))

        context['pers'] = self.per_list

        return context

    @allow_access([1, 2, 3], 'storage:itemin')
    def post(self, request, *args, **kwargs):
        if not 'add_item' in request.POST:
            return HttpResponseRedirect(self.get_update_url())
        print("LOG: %s %s %s" % (self.__class__, inspect.stack()[0][3], request.POST))

        try:
            self.item_id = int(request.POST.get('item', None))
        except Exception as e:
            self.item_id = None 

        try:
            self.doc_id = int(request.POST.get('doc', None))
        except Exception as e:
            self.doc_id = None 

        if not self.doc_id and not self.item_id:
            print("LOG: ITEMIN POST ERROR: DOC_ID: ", self.doc_id, " ITEM_ID: ",  self.item_id)
            return HttpResponseRedirect(self.get_update_url(error_message=u'Необходимо выбрать документ или ЕХ'))

        try:
            person = int(self.request.POST.get('person'))
            if person == -1:
                return HttpResponseRedirect(self.get_update_url(error_message=u'Необходимо выбрать Пользователя'))
        except Exception as e:
            person = None
            print("LOG: ITEMIN POST PERSON ERROR: ", e)
            return HttpResponseRedirect(self.get_update_url(error_message=u'Необходимо выбрать Пользователя'))

        self.comment = request.POST.get('comment', None)

        # check if mission exists, create new if not
        if self.get_queryset().count() < 1:
            mission = Mission.objects.create(name='IN', owner=request.user, status='inprogress')
        else:
            mission = self.get_queryset().last()

        # assert if document Item exists
        if self.doc_id:
            items = Item.objects.filter(document__id=self.doc_id) 
        elif self.item_id:
            items = Item.objects.filter(pk=self.item_id) 
        else:
            items = None

        if not items or items.count() < 1:
            print("LOG: INTEMIN POST: items not found ( id :", self.item_id, " )")
            return HttpResponseRedirect(self.get_update_url(error_message=u'Документ или ЕХ не найден'))
        item = items.last()

        tasks = Task.objects.filter(mission=mission, item=item, status='inprogress')

        if tasks.count() > 0:
            print("LOG: INTEMIN POST: task already exits")
            return HttpResponseRedirect(self.get_success_url())

        #status = 'done'
        status = 'inprogress'

        try:
            self.row_id = int(request.POST.get('row', None))
        except:
            self.row_id = None
            pass
        try:
            self.section_id = int(request.POST.get('section', None))
        except:
            self.section_id = None
            pass
        try:
            self.shelf_id = int(request.POST.get('shelf', None))
        except:
            self.shelf_id = None
            pass
        try:
            self.slot_id = int(request.POST.get('slot', '').replace(' ', ''))
        except:
            self.slot_id = None
            pass

        if self.row_id and self.section_id and self.shelf_id:
            try:
                row = Row.objects.get(id=self.row_id)
                section = Section.objects.get(row=row, id=self.section_id)
                shelf = Shelf.objects.get(section=section, id=self.shelf_id)
            except Exception as e:
                print("LOG: INTEMIN POST: SLOT DONE: ", e)

            if not shelf:
                raise ValidationError(e)
            slots_reserved = Slot.objects.filter(item=item)

            slots = Slot.objects.filter(shelf=shelf, number=self.slot_id)

            if not slots or slots.count() < 1: # not created yet
                for slot_reserved in slots_reserved:
                    print("LOG: ITEMIN POST: RESERVED SLOT UPDATE: ", slot_reserved.number, " [", self.slot_id, "] ")
                    if slot_reserved.number != self.slot_id:
                        slot_reserved.state = 0
                        slot_reserved.item = None
                        slot_reserved.save(update_fields=['state', 'item'])
                try:
                    slot = Slot.objects.create(item=item, shelf=shelf, number=self.slot_id, state=1)
                except:
                    e = u'Ошибка размещения в ячейке %s' % (self.slot_id)
                    return HttpResponseRedirect(self.get_update_url(error_message=e))
            else:
                print("LOG: ITEMIN POST: SLOT: ", item, " [", item.id, "]")
                slots = Slot.objects.filter(\
                        Q(shelf=shelf, number=self.slot_id, item=item) | \
                        Q(shelf=shelf, number=self.slot_id, item__isnull=True))

                if not slots or slots.count() < 1:
                    error_message=u'Место id:%s на полке %s занято ' % (self.slot_id, shelf)
                    print("LOG: INTEMIN POST: SLOT: ", error_message)
                    return HttpResponseRedirect(self.get_update_url(error_message=error_message))
                slot = slots.last()
                print("LOG: ITEMIN POST: SLOT UPDATE: ", slot.state, " [", slot.state != 1, "] ", slot.item, " [", slot.item != item, "]")

                if slot.state != 1 or slot.item != item:

                    for slot_reserved in slots_reserved:
                        print("LOG: ITEMIN POST: RESERVED SLOT UPDATE: ", slot_reserved.number, " [", self.slot_id, "] ")
                        if slot_reserved.number != self.slot_id:
                            slot_reserved.state = 0
                            slot_reserved.item = None
                            slot_reserved.save(update_fields=['state', 'item'])
                    slot.state = 1
                    slot.item = item 
                    slot.save(update_fields=['state', 'item'])
        else:
            return HttpResponseRedirect(self.get_update_url(error_message=u'Не указан один из элементов - Ряд: %s Секция: %s Полка: %s' \
                    % (self.row_id, self.section_id, self.shelf_id)))

        try:
            task = Task.objects.create(mission=mission, item=item, status=status, owner_id=person, description=self.comment) 
        except Exception as e:
            print("LOG: INTEMIN POST: TASK ERROR: ", e)
            return HttpResponseRedirect(self.get_update_url(error_message=u'Ошибка при создании задачи на возврат / размещение'))

        return HttpResponseRedirect(self.get_success_url())

    def get_update_url(self, error_message=None):
        return reverse_lazy('storage:itemin_create') + '?d=' + ('&row=%s' % (self.row_id) if self.row_id else '') + \
                                '&%s' % urlencode({'error': error_message}) if error_message else ''

    def get_success_url(self):
        return reverse_lazy('storage:itemin')

#################################################################
class InTaskDelete(DeleteView):
    model = Task
    template_name = 'storage/task/delete.html'
    success_url = reverse_lazy('storage:itemin')

    class Meta:
        fields = '__all__'

#################################################################
class TaskDelete(DeleteView):
    model = Task
    template_name = 'storage/task/delete.html'
    success_url = reverse_lazy('storage:itemout')

    class Meta:
        fields = '__all__'

#################################################################
class RackList(ListView):
    paginate_by = 10
    model = Row
    template_name = 'storage/rack/list.html'

    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        settings = Settings.objects.all().first()
        try:
            self.paginate_by = int(settings.paginate_by)
        except:
            pass

    @allow_access([1, 2])
    def get(self, request, *args, **kwargs):
        return super(RackList, self).get(request, *args, **kwargs)


class RackDocs(BaseStorage, ListView):
    paginate_by = 10
    model = Row
    template_name = 'storage/rack/details.html'

    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        settings = Settings.objects.all().first()
        try:
            self.paginate_by = int(settings.paginate_by)
        except:
            pass

    def get_context_data(self, **kwargs):
        return get_context(self)

    @allow_access([1, 2])
    def get(self, request, *args, **kwargs):
        return super(RackDocs, self).get(request, *args, **kwargs)


class RackCreate(CreateView):
    model = Row
    form_class = RowForm
    template_name = 'storage/rack/create.html'
    success_url = reverse_lazy('storage:racks')

    def get_queryset(self):
        return super(RackCreate, self).get_queryset().order_by('rack__unit', 'position')

    def get_context_data(self, **kwargs):
        context = super(RackCreate, self).get_context_data(**kwargs)
        context['error_message'] = self.request.GET.get('error', '')
        return context

    @allow_access([1], 'storage:racks')
    def post(self, request, *args, **kwargs):
        try:
            row_form = RowForm(request.POST)

            if not row_form.is_valid():
                return super(RackCreate, self).post(request, *args, **kwargs)

            data = row_form.cleaned_data
            print("LOG: STORAGE: RACK_CREATE FORM_VALID: UNIT:", data.get('unit', None), "RACK_GROUP:", data.get('rack_group', None))

            racks = Rack.objects.filter(rack_group=data.get('rack_group', None), unit=data.get('unit', None))
            can_create = False
            create_rack = None

            with transaction.atomic():
                if racks and racks.exists():
                    role_racks = racks.filter(role=data['role'])
                    print("LOG: STORAGE: RACK_CREATE RACK_COUNT:", role_racks.count())

                    if role_racks and role_racks.exists():
                        for role_rack in role_racks:
                            row = role_rack.row_set.all().exclude(position=data.get('position', None)).first()

                            if row:
                                create_rack = role_rack
                                print("LOG: STORAGE: RACK_CREATE ROLE_RACK:", role_rack, " ROW:", row)
                                break

                        if not create_rack:
                            error_message = u'Ошибка создания Ряда. Уже существует Ряд, где Группа %s Номер %s Тип %s Позиция %s' % (data.get('rack_group', None), data.get('unit', None), data.get('role', None), data.get('position', None))
                            raise ValidationError(error_message)

                    else:
                        error_message = u'Ошибка создания Ряда. Уже существует Ряд, где Группа %s Номер %s Тип %s' % (data.get('rack_group', None), data.get('unit', None), data.get('role', None))
                        raise ValidationError(error_message)
                else:
                    create_rack = Rack.objects.create(rack_group=data.get('rack_group', None), unit=data.get('unit', None))

                if create_rack:
                    row = row_form.save(commit=False)
                    row.rack = create_rack

                    if data.get('row_name', None):
                        row.description = data.get('row_name', None)
                    row.save()
                    print("LOG: STORAGE: RACK_CREATE ROW CREATED:", row)

                    for i in range(1, data.get('section_count', 1) + 1):
                        try:
                            section = Section.objects.create(row=row, number=i, rack=create_rack)
                            print("LOG: STORAGE: RACK_CREATE SECTION CREATED:", section)
                        except Exception as e:
                            section = None
                            print("LOG: STORAGE: RACK_CREATE SECTION CREATE ERROR:", e)

                        if not section:
                            continue

                        for i in range(1, data.get('shelf_count', 1) + 1):
                            try:
                                shelf = Shelf.objects.create(section=section, number=i)
                                print("LOG: STORAGE: RACK_CREATE SHELF CREATED:", shelf)
                            except Exception as e:
                                print("LOG: STORAGE: RACK_CREATE SHELF CREATE ERROR:", e)
                return HttpResponseRedirect(self.get_success_url())

        except ValidationError as e:
            return HttpResponseRedirect(reverse_lazy('storage:rack_create') + '?%s' % urlencode({'error': error_message}))

    def get_success_url(self):
        return reverse_lazy('storage:racks')

    def get_initial(self):
        print("LOG: STORAGE: RACK_CREATE INITIAL", self.request.user)
        return {'user': self.request.user }


class RackEdit(UpdateView):
    model = Row
    form_class = RowForm
    template_name = 'storage/rack/edit.html'
    success_url = reverse_lazy('storage:racks')

    def get_context_data(self, **kwargs):
        context = super(RackEdit, self).get_context_data(**kwargs)
        context['error_message'] = self.request.GET.get('error', '')
        self.object = self.get_object()
        return context

    @allow_access([1], 'storage:racks')
    def post(self, request, *args, **kwargs):
        row_form = RowForm(request.POST)

        if not row_form.is_valid():
            return super(RackCreate, self).post(request, *args, **kwargs)
        row = self.get_object()

        if not row:
            print("LOG: STORAGE: RACK_EDIT ERROR: OBJECT MISSING")
            return super(RackEdit, self).post(request, *args, **kwargs)
        data = row_form.cleaned_data
        rack = row.rack

        # all that fits number range 
        section_nums = []

        for i in range(1, data.get('section_count', 1) + 1):
            section_nums.append(i)
        sections = row.section_set.all().exclude(number__in=section_nums)

        for section in sections:
            section.delete()

        #sections = row.section_set.filter(number__in=section_nums)
        sections = row.section_set.all()

        for section in sections:
            if section.number in section_nums:
                section_nums.remove(section.number)
                print("LOG: STORAGE: RACK_EDIT: SECTION %s EXISTS" % (section.number))

        for i in section_nums:
            try:
                section_check = Section.objects.create(row=row, number=i, rack=rack)
                print("LOG: STORAGE: RACK_CREATE SECTION CREATED: %s [%s]" % (section, i))
            except Exception as e:
                section_check = None
                print("LOG: STORAGE: RACK_CREATE SECTION CREATE ERROR:", e)

        sections = row.section_set.all()

        for section in sections:
            shelf_nums = []

            for i in range(1, data.get('shelf_count', 1) + 1):
                shelf_nums.append(i)

            shelfs = section.shelf_set.all().exclude(number__in=shelf_nums)

            for shelf in shelfs:
                shelf.delete()
            shelfs = section.shelf_set.all()

            for shelf in shelfs:
                if shelf.number in shelf_nums:
                    shelf_nums.remove(shelf.number)

            for i in shelf_nums:
                try:
                    shelf = Shelf.objects.create(section=section, number=i)
                    print("LOG: STORAGE: RACK_CREATE SHELF CREATED:", shelf)
                except Exception as e:
                        print("LOG: STORAGE: RACK_CREATE SHELF CREATE ERROR:", e)
        return super(RackEdit, self).post(request, *args, **kwargs)


class RackDelete(DeleteView):
    model = Row
    template_name = 'storage/rack/delete.html'
    success_url = reverse_lazy('storage:racks')

    class Meta:
        fields = '__all__'


#################################################################
class RackGroupDocs(BaseStorage, ListView):
    paginate_by = 10 
    model = RackGroup
    template_name = 'storage/rackgroup/details.html'

    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        settings = Settings.objects.all().first()
        try:
            self.paginate_by = int(settings.paginate_by)
        except:
            pass

    def get_context_data(self, **kwargs):
        return get_context(self)


class RackGroupList(ListView):
    paginate_by = 10 
    model = RackGroup
    template_name = 'storage/rackgroup/list.html'

    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        settings = Settings.objects.all().first()
        try:
            self.paginate_by = int(settings.paginate_by)
        except:
            pass

    @allow_access([1, 2])
    def get(self, request, *args, **kwargs):
        return super(RackGroupList, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(RackGroupList, self).get_context_data(**kwargs)
        context['error_message'] = self.request.GET.get('error', '')
        return context

class RackGroupEdit(UpdateView):
    model = RackGroup
    template_name = 'storage/rackgroup/edit.html'
    form_class = RackGroupForm
    success_url = reverse_lazy('storage:rackgroups')

class RackGroupDelete(DeleteView):
    model = RackGroup
    template_name = 'storage/rackgroup/delete.html'
    success_url = reverse_lazy('storage:rackgroups')

    def get_context_data(self, **kwargs):
        context = super(RackGroupDelete, self).get_context_data(**kwargs)

        # check presense before deletion
        pk = self.kwargs.get('pk', None)

        if pk:
            exists = Rack.objects.filter(rack_group__id=pk).exists()
            if exists:
                context['error_message'] = u'Удаляемая группа используется в ряде'
        return context


    class Meta:
        fields = '__all__'

class RackGroupCreate(CreateView):
    model = RackGroup
    template_name = 'storage/rackgroup/create.html'
    form_class = RackGroupForm
    success_url = reverse_lazy('storage:rackgroups')

    def get_context_data(self, **kwargs):
        context = super(RackGroupCreate, self).get_context_data(**kwargs)
        context['error_message'] = self.request.GET.get('error', '')
        return context

    @set_created
    def form_valid(self, form):
        try:
            response = super(RackGroupCreate, self).form_valid(form)
        except Exception as e:
            error_message = str(e)
            return HttpResponseRedirect(reverse_lazy('storage:rackgroup_create') + '?%s' % urlencode({'error': error_message}))
        return response

#################################################################
class SettingsList(ListView):
    model = Settings
    template_name = 'storage/settings/list.html'
    success_url = reverse_lazy('storage:settings')

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)
        context['error_message'] = self.request.GET.get('error', '')
        return context

    @allow_access([1])
    def get(self, request, *args, **kwargs):
        return super(SettingsList, self).get(request, *args, **kwargs)

    @allow_access([1, 2, 4], 'storage:settings')
    def post(self, request, *args, **kwargs):
        print("LOG: %s POST: %s" % (self.__class__, request.POST))
        if 'reindex' in request.POST:
            models = [Document, Item, Row, Client, ClientType, RackGroup, ItemType, DocumentType, RetentionPolicy, ArcUser, Task]
            for model in models:
                print("LOG: %s %s_COUNT: %s\n" % (self.__class__, model.__name__.upper(), model.objects.all().count()))
                for item in model.objects.all():
                    item.search = ""
                    item.save(update_fields=['search'])

        try:
            pass
        except Exception as e:
            pass
        return HttpResponseRedirect(self.success_url)

class SettingsEdit(UpdateView):
    model = Settings
    template_name = 'storage/settings/edit.html'
    form_class = SettingsForm
    success_url = reverse_lazy('storage:settings')

    @allow_access([1])
    def get(self, request, *args, **kwargs):
        return super(SettingsEdit, self).get(request, *args, **kwargs)

    @allow_access([1, 2, 3], 'storage:settings')
    def post(self, request, *args, **kwargs):
        return super(SettingsEdit, self).post(request, *args, **kwargs)


#################################################################
class CommandDocs(BaseStorage, ListView):
    model = Command 
    template_name = 'storage/command/details.html'

    @allow_access([1])
    def get(self, request, *args, **kwargs):
        return super(CommandDocs, self).get(request, *args, **kwargs)

class CommandList(ListView):
    model = Command 
    template_name = 'storage/command/list.html'

    @allow_access([1])
    def get(self, request, *args, **kwargs):
        return super(CommandList, self).get(request, *args, **kwargs)

class CommandCreate(CreateView):
    model = Command 
    template_name = 'storage/command/create.html'
    form_class = CommandForm
    success_url = reverse_lazy('storage:command')

    def get_context_data(self, **kwargs):
        context = super(CommandCreate, self).get_context_data(**kwargs)
        context['error_message'] = self.request.GET.get('error', '')
        return context

    @allow_access([1])
    def get(self, request, *args, **kwargs):
        return super(CommandCreate, self).get(request, *args, **kwargs)

    @allow_access([1], 'storage:command')
    def post(self, request, *args, **kwargs):
        return super(CommandCreate, self).post(request, *args, **kwargs)

class CommandEdit(UpdateView):
    model = Command 
    template_name = 'storage/command/edit.html'
    form_class = CommandForm
    success_url = reverse_lazy('storage:command')

    @allow_access([1])
    def get(self, request, *args, **kwargs):
        return super(CommandEdit, self).get(request, *args, **kwargs)

    @allow_access([1], 'storage:command')
    def post(self, request, *args, **kwargs):
        return super(CommandEdit, self).post(request, *args, **kwargs)

class CommandDelete(DeleteView):
    model = Command 
    template_name = 'storage/command/delete.html'
    success_url = reverse_lazy('storage:command')

    class Meta:
        fields = '__all__'


#################################################################
class BarCode(ListView):
    model = Rack
    template_name = 'storage/barcode/barcode.html'
    code = None

    def get_queryset(self):
        if 'code' in self.kwargs:
            self.code = self.kwargs['code']

    def calculate_checksum(self, ean):
        assert len(ean) == 12, "EAN must be a list of 12 numbers"
        sum_ = lambda x, y: int(x) + int(y)
        evensum = reduce(sum_, ean[::2])
        oddsum = reduce(sum_, ean[1::2])
        return (10 - ((evensum + oddsum * 3) % 10)) % 10

    def get_12_array(self, code):
        numbers = []

        while code != 0:
            code, r = divmod(code, 10)
            numbers.insert(0, r)

        if len(numbers) > 12:
            numbers = numbers[:12]

        if len(numbers) < 12:
            for i in range(0, 12 - len(numbers)):
                numbers.insert(0, 0)
        return numbers

    #def __init__(self, code=None, **kwargs):
    #    super(BarCode, self).__init__(**kwargs)

    #def get(self, request, code=None, *args, **kwargs):
    def get(self, request, *args, **kwargs):
        try:
            print ("LOG BARCODE : %s" % (self.code))

            if None == self.code or len(self.code) == 0:
                self.code = 390123
            numbers = self.get_12_array(int(self.code))
            numbers.append(self.calculate_checksum(numbers))
            print ("LOG NUMBERS: %s" % (numbers))
            string = ''.join(map(str, numbers))
            print ("LOG BARCODE: %s" % (string))
            #string = str(code).encode("utf-8").decode("utf-8")
            EAN = barcode.get_barcode_class('ean13')
            #ean = EAN(u'5901234123457', writer=ImageWriter())
            ean = EAN(string, writer=ImageWriter())
            f = open(os.path.join(STATIC_FILES, 'barcode.png'), 'wb+')
            ean.write(f)
            pass
        except Exception as e:
            print ("LOG BARCODE ERROR: %s" % (e))
            pass
        #return super(BarCode, self).get(request, code, *args, **kwargs)
        return super(BarCode, self).get(request, *args, **kwargs)

#################################################################
class ItemTypeDelete(DeleteView):
    model = ItemType
    template_name = 'storage/item_type/delete.html'
    success_url = reverse_lazy('storage:item_types')

    class Meta:
        fields = '__all__'

class ItemTypeDocs(BaseStorage, ListView):
    paginate_by = 10 
    model = ItemType
    template_name = 'storage/item_type/details.html'

    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        settings = Settings.objects.all().first()
        try:
            self.paginate_by = int(settings.paginate_by)
        except:
            pass

    @allow_access([1])
    def get(self, request, *args, **kwargs):
        return super(ItemTypeDocs, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        return get_context(self)


class ItemTypeList(ListView):
    paginate_by = 10 
    model = ItemType
    template_name = 'storage/item_type/list.html'

    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        settings = Settings.objects.all().first()
        try:
            self.paginate_by = int(settings.paginate_by)
        except:
            pass

    def get_context_data(self, **kwargs):
        context = super(ItemTypeList, self).get_context_data(**kwargs)
        context['error_message'] = self.request.GET.get('error', '')
        return context

    @allow_access([1])
    def get(self, request, *args, **kwargs):
        return super(ItemTypeList, self).get(request, *args, **kwargs)

class ItemTypeEdit(UpdateView):
    form_class = ItemTypeForm
    model = ItemType
    template_name = 'storage/item_type/edit.html'
    success_url = reverse_lazy('storage:item_types')

    def get_context_data(self, **kwargs):
        context = super(ItemTypeEdit, self).get_context_data(**kwargs)
        context['error_message'] = self.request.GET.get('error', '')
        return context

    @allow_access([1])
    def get(self, request, *args, **kwargs):
        return super(ItemTypeEdit, self).get(request, *args, **kwargs)

    @allow_access([1], 'storage:item_types')
    def post(self, request, *args, **kwargs):
        return super(ItemTypeEdit, self).post(request, *args, **kwargs)

    @set_modified
    def form_valid(self, form):
        response = super(ItemTypeEdit, self).form_valid(form)
        return response

class ItemTypeCreate(CreateView):
    model = ItemType
    template_name = 'storage/item_type/create.html'
    form_class = ItemTypeForm
    success_url = reverse_lazy('storage:item_types')

    def get_context_data(self, **kwargs):
        context = super(ItemTypeCreate, self).get_context_data(**kwargs)
        context['error_message'] = self.request.GET.get('error', '')
        return context

    @allow_access([1])
    def get(self, request, *args, **kwargs):
        return super(ItemTypeCreate, self).get(request, *args, **kwargs)

    @allow_access([1], 'storage:item_types')
    def post(self, request, *args, **kwargs):
        return super(ItemTypeCreate, self).post(request, *args, **kwargs)

    @set_created
    def form_valid(self, form):
        try:
            response = super(ItemTypeCreate, self).form_valid(form)
        except Exception as e:
            error_message = str(e)
            return HttpResponseRedirect(reverse_lazy('storage:item_type_create') + '?%s' % urlencode({'error': error_message}))
        return response


#################################################################
class ItemDocs(BaseStorage, ListView):
    paginate_by = 10
    model = Item 
    template_name = 'storage/item/details.html'

    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        settings = Settings.objects.all().first()
        try:
            self.paginate_by = int(settings.paginate_by)
        except:
            pass

    def get_context_data(self, **kwargs):
        print("\nMOD: %s %s\n" % (self.request.GET.get('mod', None), self.request))
        myfilter = None 
        if self.request.GET.get('mod', None) != None:
            myfilter = {'released': True, 'owner': self.request.user}
        offset = 0
        return get_context(self, myfilter, offset, self.paginate_by)

class ItemList(ListView):
    paginate_by = 10
    model = Item
    template_name = 'storage/item/list.html'

    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        settings = Settings.objects.all().first()
        try:
            self.paginate_by = int(settings.paginate_by)
        except:
            pass

class ItemCreate(CreateView):
    model = Item
    form_class = ItemForm
    template_name = 'storage/item/create.html'
    success_url = reverse_lazy('storage:items')

    @allow_access([1, 4], 'storage:items')
    def post(self, request, *args, **kwargs):
        return super(ItemCreate, self).post(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        count = ItemType.objects.all().count()

        if count <= 0:
            error_message = u'Необходимо создать Тип ЕХ'
            return HttpResponseRedirect(reverse_lazy('storage:item_types') + '?%s' % urlencode({'error': error_message}))
        return super(ItemCreate, self).get(request, *args, **kwargs)

    @set_created
    def form_valid(self, form):
        try:
            response = super(ItemCreate, self).form_valid(form)
        except Exception as e:
            print("LOG ERROR: ITEM_CREATE: %s"% e)
            error_message = e
            return HttpResponseRedirect(reverse_lazy('storage:item_create') + '?%s' % urlencode({'error': error_message}))
        return response

    def get_initial(self):
        print("LOG: STORAGE: ITEM_CREATE INITIAL", self.request.user)
        return {'user': self.request.user }


class ItemEdit(UpdateView):
    model = Item
    form_class = ItemForm
    template_name = 'storage/item/edit.html'
    success_url = reverse_lazy('storage:items')
    permissions = 'storage.item.edit'

    def get_context_data(self, **kwargs):
        context = super(ItemEdit, self).get_context_data(**kwargs)
        context['error_message'] = self.request.GET.get('error', '')
        return context

    @allow_access([1], 'storage:items')
    def post(self, request, *args, **kwargs):
        return super(ItemEdit, self).post(request, *args, **kwargs)

    @set_modified
    def form_valid(self, form):
        try:
            response = super(ItemEdit, self).form_valid(form)
        except Exception as e:
            error_message = str(e)
            return HttpResponseRedirect(reverse_lazy('storage:item_edit') + '?%s' % urlencode({'error': error_message}))
        return response

class ItemDelete(DeleteView):
    model = Item
    template_name = 'storage/item/delete.html'
    success_url = reverse_lazy('storage:items')

    class Meta:
        fields = '__all__'

class ItemDetails(ListView):
    model = Item
    form_class = ItemForm
    template_name = 'storage/item/details.html'
    success_url = reverse_lazy('storage:items')

    def get_context_data(self, **kwargs):
        context = super(ItemDetails, self).get_context_data(**kwargs)
        return context

    class Meta:
        fields = '__all__'

class ItemAccount(ListView):
    model = Document
    #form_class = DocumentForm
    template_name = 'storage/item/account.html'
    success_url = reverse_lazy('storage:items')

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk', None)
        tasks = Task.objects.filter(item__id=pk).order_by('created')
        context['tasks'] = tasks
        return context

    def get_queryset(self):
        pk = self.kwargs.get('pk', None)
        return super(self.__class__, self).get_queryset().filter(item__id=pk)

    class Meta:
        fields = '__all__'

#################################################################
class ClientDocs(BaseStorage, ListView):
    paginate_by = 10
    model = Client
    template_name = 'storage/client/details.html'

    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        settings = Settings.objects.all().first()
        try:
            self.paginate_by = int(settings.paginate_by)
        except:
            pass

    def get_context_data(self, **kwargs):
        return get_context(self)

class ClientCreate(CreateView):
    model = Client
    template_name = 'storage/client/create.html'
    form_class = ClientForm
    success_url = reverse_lazy('storage:clients')

    @allow_access([1, 4], 'storage:clients')
    def post(self, request, *args, **kwargs):
        return super(ClientCreate, self).post(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ClientCreate, self).get_context_data(**kwargs)
        context['error_message'] = self.request.GET.get('error', '')
        return context

class ClientList(ListView):
    paginate_by = 10
    model = Client
    template_name = 'storage/client/list.html'

    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        settings = Settings.objects.all().first()
        try:
            self.paginate_by = int(settings.paginate_by)
        except:
            pass

class ClientEdit(UpdateView):
    model = Client
    form_class = ClientForm
    template_name = 'storage/client/edit.html'
    success_url = reverse_lazy('storage:clients')

class ClientDelete(DeleteView):
    model = Client 
    template_name = 'storage/client/delete.html'
    success_url = reverse_lazy('storage:clients')

#################################################################
class DocumentDocs(BaseStorage, ListView):
    model = Document 
    template_name = 'storage/document/details.html'

    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        settings = Settings.objects.all().first()
        try:
            self.paginate_by = int(settings.paginate_by)
        except:
            pass

    def get_context_data(self, **kwargs):
        return get_context(self)

class DocumentList(ListView):
    paginate_by = 10
    model = Document 
    template_name = 'storage/document/list.html'

    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        settings = Settings.objects.all().first()
        try:
            self.paginate_by = int(settings.paginate_by)
        except:
            pass

    def get_context_data(self, **kwargs):
        context = super(DocumentList, self).get_context_data(**kwargs)
        return context

class DocumentCreate(CreateView):
    model = Document
    template_name = 'storage/document/create.html'
    form_class = DocumentForm
    success_url = reverse_lazy('storage:documents')

    def get(self, request, *args, **kwargs):
        count = DocumentType.objects.all().count()

        if count <= 0:
            error_message = u'Необходимо создать Тип Документа'
            return HttpResponseRedirect(reverse_lazy('storage:document_types') + '?%s' % urlencode({'error': error_message}))
        return super(self.__class__, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)
        context['error_message'] = self.request.GET.get('error', '')
        return context

    @allow_access([1, 4], 'storage:document')
    def post(self, request, *args, **kwargs):
        count = DocumentType.objects.all().count()

        if count <= 0:
            return HttpResponseRedirect(reverse_lazy('storage:document_type_create'))
        return super(self.__class__, self).post(request, *args, **kwargs)

    @set_created
    def form_valid(self, form):
        try:
            response = super(self.__class__, self).form_valid(form)
        except Exception as e:
            error_message = str(e)
            return HttpResponseRedirect(reverse_lazy('storage:document_create') + '?%s' % urlencode({'error': error_message}))
        return response

    def get_initial(self):
        print("LOG: STORAGE: DOCUMENT_CREATE INITIAL", self.request.user)
        return {'user': self.request.user }


class DocumentEdit(UpdateView):
    form_class = DocumentForm
    model = Document
    template_name = 'storage/document/edit.html'
    success_url = reverse_lazy('storage:documents')
    permissions = 'storage.item.edit'

    def get_context_data(self, **kwargs):
        context = super(DocumentEdit, self).get_context_data(**kwargs)
        context['error_message'] = self.request.GET.get('error', '')
        return context

    @allow_access([1], 'storage:documents')
    def post(self, request, *args, **kwargs):
        return super(DocumentEdit, self).post(request, *args, **kwargs)

    @set_modified
    def form_valid(self, form):
        try:
            response = super(DocumentEdit, self).form_valid(form)
        except Exception as e:
            error_message = str(e)
            return HttpResponseRedirect(reverse_lazy('storage:document_edit') + '?%s' % urlencode({'error': error_message}))
        return response

class DocumentDelete(DeleteView):
    model = Document
    template_name = 'storage/document/delete.html'
    success_url = reverse_lazy('storage:documents')
#################################################################

class ItemTypeDelete(DeleteView):
    model = ItemType
    template_name = 'storage/item_type/delete.html'
    success_url = reverse_lazy('storage:item_types')

    class Meta:
        fields = '__all__'

class MyWaitingList(ListView):
    model = Rack 
    template_name = 'storage/waitinglist/my_waitinglist.html'

class MyIssuedItemList(ListView):
    model = Item
    template_name = 'storage/item/my_issued_item_list.html'

    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        settings = Settings.objects.all().first()
        try:
            self.paginate_by = int(settings.paginate_by)
        except:
            pass

    def get_queryset(self):
        return super(self.__class__, self).get_queryset().filter(released=True, owner=self.request.user).order_by('id')

#################################################################
class DepartmentDocs(BaseStorage, ListView):
    paginate_by = 10
    model = Department 
    template_name = 'storage/department/details.html'

    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        settings = Settings.objects.all().first()
        self.paginate_by = int(settings.paginate_by)
        try:
            self.paginate_by = int(settings.paginate_by)
        except:
            pass

    @allow_access([1, 2])
    def get(self, request, *args, **kwargs):
        return super(DepartmentDocs, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        return get_context(self)

class DepartmentList(ListView):
    paginate_by = 10
    model = Department 
    template_name = 'storage/department/list.html'

    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        settings = Settings.objects.all().first()
        try:
            self.paginate_by = int(settings.paginate_by)
        except:
            pass

    @allow_access([1, 2])
    def get(self, request, *args, **kwargs):
        return super(DepartmentList, self).get(request, *args, **kwargs)

class DepartmentCreate(CreateView):
    model = Department
    template_name = 'storage/department/create.html'
    form_class = DepartmentForm
    success_url = reverse_lazy('storage:departments')

    def get_context_data(self, **kwargs):
        context = super(DepartmentCreate, self).get_context_data(**kwargs)
        context['error_message'] = self.request.GET.get('error', '')
        return context

class DepartmentEdit(UpdateView):
    model = Department
    form_class = DepartmentForm
    template_name = 'storage/department/edit.html'
    success_url = reverse_lazy('storage:departments')

class DepartmentDelete(DeleteView):
    model = Department
    template_name = 'storage/department/delete.html'
    success_url = reverse_lazy('storage:departments')

    def get_context_data(self, **kwargs):
        context = super(DepartmentDelete, self).get_context_data(**kwargs)

        # check presense before deletion
        pk = self.kwargs.get('pk', None)

        if pk:
            exists = Item.objects.filter(department__id=pk).exists()
            if exists:
                context['error_message'] = u'Удаляемый отдел используется в единице хранения'
        return context

#################################################################
class ClientTypeList(ListView):
    paginate_by = 10
    model = ClientType
    template_name = 'storage/client_type/list.html'

    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        settings = Settings.objects.all().first()
        try:
            self.paginate_by = int(settings.paginate_by)
        except:
            pass

    @allow_access([1, 2])
    def get(self, request, *args, **kwargs):
        return super(ClientTypeList, self).get(request, *args, **kwargs)

class ClientTypeCreate(CreateView):
    model = ClientType
    template_name = 'storage/client_type/create.html'
    form_class = ClientTypeForm
    success_url = reverse_lazy('storage:client_types')

    def get_context_data(self, **kwargs):
        context = super(ClientTypeCreate, self).get_context_data(**kwargs)
        context['error_message'] = self.request.GET.get('error', '')
        return context


class ClientTypeEdit(UpdateView):
    model = ClientType
    form_class = ClientTypeForm
    template_name = 'storage/client_type/edit.html'
    success_url = reverse_lazy('storage:client_types')

class ClientTypeDelete(DeleteView):
    model = ClientType
    template_name = 'storage/client_type/delete.html'
    success_url = reverse_lazy('storage:client_types')

    def get_context_data(self, **kwargs):
        context = super(ClientTypeDelete, self).get_context_data(**kwargs)

        # check presense before deletion
        pk = self.kwargs.get('pk', None)

        if pk:
            exists = Client.objects.filter(client_type__id=pk).exists()
            if exists:
                context['error_message'] = u'Удаляемый тип группы используется в группах'
        return context

class ClientTypeDocs(BaseStorage, ListView):
    paginate_by = 10
    model = ClientType
    template_name = 'storage/client_type/details.html'

    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        settings = Settings.objects.all().first()
        try:
            self.paginate_by = int(settings.paginate_by)
        except:
            pass

    def get_context_data(self, **kwargs):
        return get_context(self)

#################################################################

class FillingRuleList(ListView):
    model = Rack 
    template_name = 'storage/filling_rule/list.html'

class FileTypeList(ListView):
    model = Rack 
    template_name = 'storage/file_type/list.html'

class PriorityList(ListView):
    model = Rack 
    template_name = 'storage/priority/list.html'

#################################################################
class DocumentTypeDocs(BaseStorage, ListView):
    paginate_by = 10 
    model = DocumentType
    template_name = 'storage/document_type/details.html'

    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        settings = Settings.objects.all().first()
        try:
            self.paginate_by = int(settings.paginate_by)
        except:
            pass

    def get_context_data(self, **kwargs):
        return get_context(self)

class DocumentTypeList(ListView):
    paginate_by = 10 
    model = DocumentType
    template_name = 'storage/document_type/list.html'

    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        settings = Settings.objects.all().first()
        try:
            self.paginate_by = int(settings.paginate_by)
        except:
            pass

    @allow_access([1, 2])
    def get(self, request, *args, **kwargs):
        return super(DocumentTypeList, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DocumentTypeList, self).get_context_data(**kwargs)
        context['error_message'] = self.request.GET.get('error', '')
        return context


class DocumentTypeCreate(CreateView):
    model = DocumentType
    template_name = 'storage/document_type/create.html'
    form_class = DocumentTypeForm
    success_url = reverse_lazy('storage:document_types')

    def get_context_data(self, **kwargs):
        context = super(DocumentTypeCreate, self).get_context_data(**kwargs)
        context['error_message'] = self.request.GET.get('error', '')
        return context

class DocumentTypeEdit(UpdateView):
    form_class = DocumentTypeForm
    model = DocumentType
    template_name = 'storage/document_type/edit.html'
    success_url = reverse_lazy('storage:document_types')

class DocumentTypeDelete(DeleteView):
    model = DocumentType
    template_name = 'storage/document_type/delete.html'
    success_url = reverse_lazy('storage:document_types')

    def get_context_data(self, **kwargs):
        context = super(DocumentTypeDelete, self).get_context_data(**kwargs)

        # check presense before deletion
        pk = self.kwargs.get('pk', None)

        if pk:
            exists = Document.objects.filter(document_type__id=pk).exists()
            if exists:
                context['error_message'] = u'Удаляемый тип документа используется в документах'
        return context

    class Meta:
        fields = '__all__'

#################################################################
class RetentionPolicyList(ListView):
    paginate_by = 10
    model = RetentionPolicy
    template_name = 'storage/retention_policy/list.html'

    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        settings = Settings.objects.all().first()
        try:
            self.paginate_by = int(settings.paginate_by)
        except:
            pass

    @allow_access([1, 2])
    def get(self, request, *args, **kwargs):
        return super(RetentionPolicyList, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = get_context(self)
        context['colors'] = PERIOD_COLOR
        return context

class RetentionPolicyDocs(BaseStorage, ListView):
    paginate_by = 10
    model = RetentionPolicy 
    template_name = 'storage/retention_policy/details.html'

    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        settings = Settings.objects.all().first()
        try:
            self.paginate_by = int(settings.paginate_by)
        except:
            pass

    def get_context_data(self, **kwargs):
        context = get_context(self)
        context['colors'] = PERIOD_COLOR
        return context

class RetentionPolicyCreate(CreateView):
    model = RetentionPolicy
    template_name = 'storage/retention_policy/create.html'
    form_class = RetentionPolicyForm
    success_url = reverse_lazy('storage:retention_policies')

    def get_context_data(self, **kwargs):
        context = super(RetentionPolicyCreate, self).get_context_data(**kwargs)
        context['error_message'] = self.request.GET.get('error', '')
        return context

class RetentionPolicyEdit(UpdateView):
    form_class = RetentionPolicyForm 
    model = RetentionPolicy 
    template_name = 'storage/retention_policy/edit.html'
    success_url = reverse_lazy('storage:retention_policies')

class RetentionPolicyDelete(DeleteView):
    model = RetentionPolicy
    template_name = 'storage/retention_policy/delete.html'
    success_url = reverse_lazy('storage:retention_policies')

    def get_context_data(self, **kwargs):
        context = super(RetentionPolicyDelete, self).get_context_data(**kwargs)

        # check presense before deletion
        pk = self.kwargs.get('pk', None)

        if pk:
            exists = Item.objects.filter(retention_policy__id=pk).exists()
            if exists:
                context['error_message'] = u'Удаляемый период используется в ЕХ'
        return context
    class Meta:
        fields = '__all__'

#################################################################
class ItemTypeDelete(DeleteView):
    model = ItemType
    template_name = 'storage/item_type/delete.html'
    success_url = reverse_lazy('storage:item_types')

    class Meta:
        fields = '__all__'

    def get_context_data(self, **kwargs):
        context = super(ItemTypeDelete, self).get_context_data(**kwargs)

        # check presense before deletion
        pk = self.kwargs.get('pk', None)

        if pk:
            exists = Item.objects.filter(item_type__=pk).exists()
            if exists:
                context['error_message'] = u'Удаляемый тип используется в ЕХ'
        return context


class CostCenterList(ListView):
    model = Rack 
    template_name = 'storage/cost_center/list.html'


