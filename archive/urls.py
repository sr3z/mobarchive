# coding: utf-8

from django.contrib.auth.views import logout, login
from django.contrib import admin
from django.conf.urls import include, url
from .utils.decorators import anonymous_required
from archive.users.views import arc_login, arc_logout

urlpatterns = [
    url(r'^admin/', admin.site.urls),
]

urlpatterns += (
    url(r'^storage/', include('archive.storage.urls', namespace="storage")),
    url(
        r'^login/$',
        anonymous_required(login),
        {'template_name': "homepage/registration/login.html"},
        name="login",
    ),
    url(
        r'^arc_login/$',
        arc_login,
        name="arc_login",
    ),
    url(r'^logout/$', arc_logout, name="logout"),
    url(r'^api/', include('archive.api.urls', namespace="api")),
    url(r'^users/', include('archive.users.urls', namespace="users")),
    url(r'^reports/', include('archive.reports.urls', namespace='reports')),
    url(r'^', include('archive.homepage.urls')),
    url(r'^', include('archive.homepage.urls', namespace="registration_complete")),
)
