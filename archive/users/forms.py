# coding: utf-8

import inspect
from django import forms
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _
from django.template.loader import render_to_string
from django.apps import apps

from django.contrib.auth.forms import PasswordChangeForm
from registration.forms import RegistrationForm

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from archive.users.models import ArcUser, DefaultUser

from .layout import get_layout
from .models import USER_ROLE


class UserChangePasswordForm(forms.ModelForm):
    error_css_class = 'has-error'
    error_messages = {'password_incorrect':
                        'Старый пароль неверен. Попробуйте еще раз.'}

    new_password1 = forms.CharField(required=True,
                            label=_("Password"),
                            widget=forms.PasswordInput(attrs={
                                'class': 'form-control'}),
                            error_messages={
                                'required': 'Пароль может быть пустым'})
    new_password2 = forms.CharField(required=True,
                            label=_("Password (again)"),
                            widget=forms.PasswordInput(attrs={
                                'class': 'form-control'}),
                            error_messages={
                                'required': 'Пароль может быть пустым'})
    def clean_password2(self):
        password1 = self.cleaned_data['new_password1']
        password2 = self.cleaned_data['new_password2']
        if password1 != password2:  
            raise forms.ValidationError(_(u'Пароли неодинаковы'))
        return password2

    class Meta:
        model = ArcUser
        exclude = ('id', 'role', 'last_url', 'user', 'department', 'send_command', 'created_by', 'modified_by')
        fields_required = ['new_password1', 'new_password2']

class DeleteUserForm(forms.ModelForm):
    is_active = forms.ChoiceField(label=u'Состояние',
                choices=[(True, 'Активен'), (False, 'Не активен')])

    class Meta:
        model = DefaultUser
        exclude = '__all__'

class UserEditForm(forms.ModelForm):
    username = forms.RegexField(
                                regex=r'^[\w.@+-]+$',
                                max_length=30,
                                label=_("Username"),
                                error_messages={'invalid': _("This value may contain only letters, numbers and @/./+/-/_ characters.")})
    try:
        Department = apps.get_model('storage', 'Department')

        department = forms.ChoiceField(label=u'Отдел',
            choices=[(-1, '-----')]+[(id, name) for id, name in Department.objects.filter(enabled=True).values_list('id', 'name')])
        #print("LOG: Department: ", department.choices)
    except:
        #print("LOG: ERROR: No Department in storage")
        pass

    role = forms.ChoiceField(label=u'Роль',
        choices=[role for role in USER_ROLE],
    )

    is_active = forms.ChoiceField(label=u'Состояние',
        choices=[(True, 'Активен'), (False, 'Не активен')]
    )
    user_role = None

    def __init__(self, *args, **kwargs):
        super(UserEditForm, self).__init__(*args, **kwargs)
        role = self.initial.get('role', 0)
        curr_role = self.initial.get('curr_role', 0)
        print("LOG: UserEditForm: CURR_ROLE: %s ROLE: %s" % (curr_role, role))
        self.fields['role'].initial = role

    def clean_role(self):
        role = self.cleaned_data['role']
        #if not role or role == 0 or role == '0':
        #    print ("LOG: UserCreateForm ERROR: ROLE:", role)
        #    raise forms.ValidationError(_(u'Роль отсутствует. Поле обязательное'))
        self.user_role = role
        return role

    def save(self):
        user = super(UserEditForm, self).save()

        #if not self.instance or not self.instance.pk:
        #    return user
        curr_role = self.initial.get('curr_role', 0)
        print("LOG: %s %s CURR_ROLE: %s USER_ROLE: %s" % (self.__class__, inspect.stack()[0][3].upper(), curr_role, self.user_role))

        if not curr_role in [1]:
            print ("LOG: UserEditForm FORBIDDEN EDIT ROLE:", curr_role)
            return user

        arcusers = ArcUser.objects.filter(user=user)

        if arcusers and arcusers.exists():
            print("LOG: %s %s SAVE USER_ROLE: %s" % (self.__class__, inspect.stack()[0][3].upper(), self.user_role))
            arcuser = arcusers.first()
            arcuser.set_role(self.user_role)
        return user

    class Meta:
        model = DefaultUser
        exclude = ('password', 'email', 'last_login', 'is_superuser', 'is_staff', 'date_joined', 'groups', 'user_permissions')


class UserCreateForm(RegistrationForm):
    username = forms.RegexField(
                                regex=r'^[\w.@+-]+$',
                                max_length=30,
                                label=_("Username"),
                                error_messages={'invalid': _("This value may contain only letters, numbers and @/./+/-/_ characters.")})
    first_name = forms.CharField(label=_(u"Имя"), required=False)
    last_name = forms.CharField(label=_(u"Фамилия"), required=False)
    password2 = forms.CharField(widget=forms.PasswordInput,
                                label=_("Password (again)"))
    department = forms.ChoiceField(label=u'Отдел',
        choices=[])

    role = forms.ChoiceField(label=u'Роль', required=True,
        choices=[role for role in USER_ROLE]
    )
    error_css_class = 'errors'
    email = None
    user_role = None

    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)

        self.fields['password1'].min_length = 3

        self.helper = FormHelper()
        self.helper.attrs = {'enctype': 'application/x-www-form-urlencoded'}
        self.helper.form_method = 'POST'
        self.helper.add_input(Submit('submit', 'Зарегистрироваться'))
        try:
            Department = apps.get_model('storage', 'Department')
            self.fields['department'].choices=[(-1, '-----')]+[(id, name) for id, name in Department.objects.filter(enabled=True).values_list('id', 'name')]
            print("LOG: Department: ", self.fields['department'].choices)
        except Exception as e:
            print("LOG: ERROR: No Department in storage:", e)

        for field in self.fields:
            self.fields[field].required = False

    def clean_username(self):
        username = self.cleaned_data['username']

        try:
            users = DefaultUser._default_manager.filter(username=username)
            if users and users.exists():
                raise forms.ValidationError(u'Пользователь с именем %s уже существует' % username)
        except Exception as e:
            print("LOG: ERROR: registration_form: %s" % (e))
            raise forms.ValidationError(e)

        if not username:
            raise forms.ValidationError(u'Поле обязательное')
        return username

    def clean_email(self):
        email = self.cleaned_data['email']
        return email

    def clean_password2(self):
        password1 = self.cleaned_data['password1']
        password2 = self.cleaned_data['password2']

        if password1 != password2:  
            raise forms.ValidationError(_(u'Пароли неодинаковы'))
        return password2

    def clean_password(self):
        password = self.cleaned_data['password1']

        if not password:
            raise forms.ValidationError(u'Поле обязательное')
        return password

    def clean_role(self):
        role = self.cleaned_data['role']
        if not role or role == 0 or role == '0':
            print ("LOG: UserCreateForm ERROR: ROLE:", role)
            #raise forms.ValidationError(_(u'Роль отсутствует. Поле обязательное'))
        self.user_role = role
        return role

    def send_activation_email(self, site):
        pass

    def save(self):
        user = super(UserCreateForm, self).save()
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.save(update_fields=['first_name', 'last_name'])

        role = self.user_role
        arcusers = ArcUser.objects.filter(user=user)

        if arcusers and arcusers.exists():
            arcuser = arcusers.first()
            arcuser.role = role
            arcuser.save(update_fields=['role'])
        return user

    class Meta(RegistrationForm.Meta):
        fields = [
            DefaultUser.USERNAME_FIELD,
            'password1',
            'password2'
        ]
        required_css_class = 'required'
        fields_required = ['username', 'password1', 'password2', 'role']

