# coding: utf-8
from django.utils.translation import ugettext_lazy as _
from django.http import Http404
from crispy_forms.layout import Layout, Submit, Row, Div, HTML, Column, Field

def get_layout(group, agreement):
    first = (
        #TwoColumnRow('username', 'email'),
        TwoColumnRow('username'),
        TwoColumnRow('password1', 'password2'),
        Row(HTML=_(
            "Your password must NOT contain any part of your logon name, "
            "your first and last name or any simple combinations like '12345'")),
        Row(Row(css_class='separator')),
        TwoColumnRow('first_name', 'last_name'),
    )
    second_affiliates = (
        Row(Row(css_class='separator')),
        TwoColumnRow('phone', 'skype'),
        TwoColumnRow('jabber', 'icq'),
    )
    second_merchants = (
        Row(Row(css_class='separator')),
        TwoColumnRow('phone', 'skype'),
    )
    end = (
        Column('check'),
        Row(Row(css_class='separator')),
        Row(Div(Div(HTML(agreement), css_class='inner'), css_class='agreement-info')),
        SingleColumnRow('tos', css_class='row-checkbox'),
        Row(Submit('', value=_('Registration')))
    )
    layouts = {
        'Affiliate': first + second_affiliates + end,
        'Merchant': first + second_merchants + end
    }
    if not group in layouts:
        raise Http404
    return Layout(*layouts.get(group))


class FormField(Field):
    template = 'users/field.html'


class SingleColumnRow(Row):

    def __init__(self, field, **kwargs):
        super(SingleColumnRow, self).__init__(FormField(field), **kwargs)


class TwoColumnRow(Row):

    def __init__(self, first=None, second=None, **kwargs):
        fields = []
        if first is not None:
            fields.append(Div(FormField(first), css_class="left-col"))
        if second is not None:
            fields.append(Div(FormField(second), css_class="right-col"))
        super(TwoColumnRow, self).__init__(*fields, **kwargs)
