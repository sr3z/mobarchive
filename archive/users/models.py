# -*- coding: utf-8 -*-

import inspect
from django.db import models, transaction
from django.contrib.auth.models import User as DefaultUser, Group
from django.db.models import Manager
from django.utils.translation import ugettext_lazy as _
from django.dispatch import receiver
from django.db.models.signals import post_save
from archive.storage.models import Department
from django.conf import settings

from archive.utils.models import TimeStampedModelIndexed, BaseParameter
from registration.models import RegistrationManager

USER_ROLE = (
    (0, u'Не авторизован'),
    (1, u'Администратор'),
    (2, u'Архивариус'),
    (3, u'Оператор'),
    (4, u'Редактор'),
    (5, u'Пользователь'),
)

class Customer(TimeStampedModelIndexed):
    name = models.CharField(_(u'Имя'), max_length=100)

    def __unicode__(self):
        return '%s %s' % (self.created, self.name)

class ArcUser(TimeStampedModelIndexed):
    user = models.OneToOneField(DefaultUser, null=True, default=None)
    role = models.PositiveSmallIntegerField(
        choices=USER_ROLE,
        default=0,
        blank=True, null=True,
    )
    department = models.ForeignKey(Department,
                                   on_delete=models.SET_NULL,
                                   blank=True, null=True,
                                   verbose_name=_(u'Отдел'))
    send_command = models.PositiveSmallIntegerField(
        default=1,
        verbose_name=_(u'Выполнять')
    )
    last_url = models.CharField(_(u'Наименование'), max_length=512, default='/storage/item/',
                                   blank=True, null=True,
            )

    search = models.TextField(_(u'Поиск'), null=True, blank=True)

    def create_index(self):
        search = ''
        try:
            if self.user.username:
                search = search + self.user.username.replace(' ', '')
            if self.user.first_name:
                search = search + self.user.first_name.replace(' ', '')
            if self.user.last_name:
                search = search + self.user.last_name.replace(' ', '')
            if self.user.is_active:
                search = search + 'Активен'
            else:
                search = search + 'Не активен'
            if self.role:
                role = [r for r in USER_ROLE if str(r[0]) == str(self.role)]
                if role and len(role) > 0 and len(role[0]) > 1:
                    search = search + role[0][1]
            if self.department:
                search = search + self.department.__str__().replace(' ' , '') 
        except Exception as e:
            print("LOG: %s %s ERROR: %s" % (self.__class__, inspect.stack()[0][3].upper(), e))
            pass

        if search and len(search) > 1:
            self.search = search.lower().replace(' ' , '')
        else:
            self.search = ''

        if settings.DEBUG_MESSAGES:
            print("LOG: %s SEARCH: %s" % (self.__class__, self.search))
        return self.search

    def save(self, *args, **kwargs):
        self.create_index()
        super(self.__class__, self).save(*args, **kwargs)

    def set_role(self, role=None):
        if not role:
            self.role = 0
            return
        valid_items = [item for item in USER_ROLE if str(item[0]) == str(role)]

        if valid_items and len(valid_items):
            self.role = role

            if 1 == role:
                self.last_url = '/users/'
                self.save(update_fields=['last_url'])
        else:
            self.role = 0
        self.save(update_fields=['role', 'search'])

    def get_role_name(self):
        return '%s' % ([r for r in USER_ROLE if str(r[0]) == str(self.role)][0][1])

    def __unicode__(self):
        return '%s' % (self.user.username)

    @receiver(post_save, sender=DefaultUser)
    def create_user_user(sender, instance, created, **kwargs):
        print("LOG: INSTANCE", instance, "CREATED:", created, "KWARGS:", kwargs)
        if created:
            user = ArcUser.objects.filter(user=instance)
            if instance:
                print("LOG: INSTANCE USER")
                u = ArcUser.objects.create(user=instance, role=0)

    class Meta:
        permissions = [
            ("user.edit", _("Редактирование пользователей")),
        ]

class License(TimeStampedModelIndexed):
    code = models.CharField(_(u'Код'), max_length=100)

    def __unicode__(self):
        return '%s' % (self.created)

