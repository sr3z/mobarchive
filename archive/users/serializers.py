# -*- coding: utf-8 -*-
from django.contrib.auth.hashers import make_password
from django.db import transaction

from dynamic_rest.serializers import DynamicModelSerializer, DynamicRelationField
from rest_framework.serializers import CharField

from archive.storage.serializers import RackGroupSerializer
from .models import User, Group


class UserSerializerMinimal(DynamicModelSerializer):
    '''
    minimal user serializer
    '''

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name')


class UserSerializerWithoutGroups(DynamicModelSerializer):
    '''
    user without groups serializer
    '''

    class Meta:
        model = User
        exclude = (
            'date_joined',
            'is_active',
            'is_superuser',
            'last_login',
            'user_permissions',
            'password',
            'groups',
        )


class GroupSerializer(DynamicModelSerializer):
    '''
    group serializer
    '''

    user_set = UserSerializerWithoutGroups(many=True, embed=True, read_only=True)
    rack_groups = RackGroupSerializer(many=True, embed=True, read_only=True)

    class Meta:
        model = Group
        exclude = (
            'permissions',
        )
        read_only_fields = (
            'date_created',
            'date_updated',
        )


class UserSerializer(UserSerializerWithoutGroups):
    '''
    user serializer
    '''

    groups = GroupSerializer(many=True, embed=True, read_only=True)


    class Meta(UserSerializerWithoutGroups.Meta):
        exclude = UserSerializerWithoutGroups.Meta.exclude[:-1]


class CreateUserSerializer(UserSerializer):
    '''
    user serializer
    '''

    groups = GroupSerializer(many=True, embed=True, read_only=True)
    password = CharField(write_only=True)

    def validate(self, attrs):
        attrs = super(CreateUserSerializer, self).validate(attrs)
        if 'password' in attrs:
            attrs['password'] = make_password(attrs['password'])
        return attrs

    class Meta(UserSerializer.Meta):
        exclude = UserSerializer.Meta.exclude[:-1]


