# coding: utf-8
from django.conf.urls import include, url
from django.views.generic import TemplateView
from archive.users.views import ARCRegistrationView, exec_rule, license, UserCreate, UserChangePassword, UserEdit, UserDelete, UserList, UserDocs

urlpatterns = (
    url(r'^$', UserList.as_view(), name='users'),
    url(r'^docs/', UserDocs.as_view(), name='user_search'),
    url(r'^register/$', ARCRegistrationView.as_view(), name='register'),
    url(r'^create/$',
        UserCreate.as_view(), name='create'),

    url(r'^exec_rule/(?P<rule>\d+)/$',
        exec_rule, name='exec_rule'),
    url(r'^profile/(?P<pk>\d*)?',
        UserList.as_view(), name='user_profile'),
    url(r'^change_password/(?P<pk>\d+)/$',
        UserChangePassword.as_view(), name='change_password'),
    url(r'^edit/(?P<user>\d+)/(?P<pk>\d+)/$',
        UserEdit.as_view(), name='user_edit'),
    url(r'^delete/(?P<pk>\d+)/$',
        UserDelete.as_view(), name='user_delete'),
    url(r'^license/create/$',
        license, name='license_create'),
)
