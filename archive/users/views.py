# -*- coding: utf-8 -*-

import inspect
from django.utils.http import urlencode
from django.core.exceptions import ValidationError
from django.views.generic import DeleteView, ListView, UpdateView, DetailView
from django.core.management.base import BaseCommand
from django.core.urlresolvers import reverse_lazy
from django.views.generic.base import TemplateView
from django.http import HttpResponse, HttpResponseRedirect
from registration.backends.default.views import RegistrationView as BaseRegistrationView
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User as DefaultUser, Group
from django.views.generic import CreateView, DetailView

from os.path import join as pathjoin
from archive.utils.decorators import anonymous_required, allow_access, set_created, set_modified
from archive.utils.helpers import reverse_absolute_uri, get_remote_addr, djqs_from_search
from archive.users.models import License, ArcUser
from archive.storage.models import Item, Document
from .forms import UserChangePasswordForm, DeleteUserForm, UserEditForm, UserCreateForm


def arc_login(request):
    users = ArcUser.objects.filter(user=request.user)

    if users and users.exists():
        user_path = users.last().last_url
        print ("LOG: arc_login USERS_PATH:", user_path)
        return HttpResponseRedirect(user_path)
    return HttpResponseRedirect("/")

def arc_logout(request):
    path = request.GET.get('path', None)
    user_path = None
    users = ArcUser.objects.filter(user=request.user)

    if users and users.exists():
        user = users.last()
        user.last_url = path
        user.save(update_fields=['last_url'])
        user_path = user.last_url

    print ("LOG: request arc_logout", "PATH:", path, "USERS_PATH:", user_path)
    logout(request)
    return HttpResponseRedirect("/")


def exec_rule(request, rule):
    if request.method != 'GET':
        return HttpResponseRedirect("/")
    print("LOG: EXEC_RULE: GET:", rule)
    remote_addr = get_remote_addr(request)
    user = None

    try:
        user = ArcUser.objects.get(user=request.user)
    except Exception as e:
        return HttpResponse({'status': 'ERROR', 'remote_addr': remote_addr, 'message': 'Авторизованный пользователь не найден'}, status=201)

    if not user:
        print("LOG: EXEC_RULE: GET:", rule)
        return HttpResponse({'status': 'ERROR', 'remote_addr': remote_addr, 'message': 'Авторизованный пользователь не найден'}, status=201)

    try:
        user.send_command = int(rule)
        user.save(update_fields=['send_command'])
    except Exception as e:
        return HttpResponse({'status': 'ERROR', 'remote_addr': remote_addr, 'message': 'Авторизованный пользователь не найден'}, status=201)
    print("LOG: EXEC_RULE: OK")
    return HttpResponse({'uuid': {'value': 'OK'}})

def license(request):
    llist = ['11111111111111111111']
    if request.method != 'POST':
        return HttpResponseRedirect("/")
    print("LOG: LICENSE: POST:", request.POST)
    try:
        code = request.POST.get('license', '')
    except Exception as e:
        return HttpResponseRedirect("/")
    code = code.replace('-', '').replace(' ' , '')

    if code and len(code) > 0 and code in llist:
        License.objects.create(code=code)

    return HttpResponseRedirect("/")


class UserCreate(CreateView):
    model = DefaultUser
    form_class = UserCreateForm
    template_name = 'users/create.html'
    success_url = reverse_lazy('users:users')

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)
        context['action'] = u'Создать пользователя'
        return context

    @allow_access([1], 'users:users')
    def post(self, request, *args, **kwargs):
        post = super(self.__class__, self).post(request, *args, **kwargs)
        print('LOG: %s "%s": %s' % (self.__class__, inspect.stack()[0][3], request.POST))
        try:
            name = request.POST.get('username', '')
            new_user = ArcUser.objects.filter(user__username=name).first()
            if new_user:
                new_user.search = ''
                new_user.save(update_fields=['search'])
        except Exception as e:
            print('      LOG: %s "%s" ERROR: %s' % (self.__class__, inspect.stack()[0][3], e))
        return post

class UserList(ListView):
    model = ArcUser
    template_name = 'users/list.html'
    pk = None

    def get_queryset(self):
        if 'pk' in self.kwargs:
            self.pk = self.kwargs['pk']

        users = ArcUser.objects.filter(user=self.request.user)

        if users and users.exists() and users.last().role in [1]:
            pass
        else:
            if not self.pk or (self.pk and self.pk != self.request.user.id):
                self.pk = self.request.user.id
                print("LOG: USER: ", str(self.__class__), "GETUSER_ROLE:", users.last().role, " FORCE PK:", self.pk)

        if self.pk:
            return DefaultUser.objects.filter(pk=self.pk).order_by('arcuser__role', 'id')
        else:
            return DefaultUser.objects.all().select_related('arcuser').order_by('arcuser__role', 'id')

    @allow_access([1, 2, 3, 4, 5])
    def get(self, request, *args, **kwargs):
        return super(UserList, self).get(request, *args, **kwargs)

class UserEdit(UpdateView):
    model = DefaultUser
    template_name = 'users/create.html'
    form_class = UserEditForm
    success_url = reverse_lazy('users:users')

    @allow_access([1], 'users:users')
    def post(self, request, *args, **kwargs):
        return super(UserEdit, self).post(request, *args, **kwargs)

    def get_initial(self):
        pk = self.kwargs['pk']
        curr_user = self.kwargs['user']
        currusers = ArcUser.objects.filter(user__pk=curr_user)
        arcusers = ArcUser.objects.filter(user__pk=pk)

        if arcusers and arcusers.exists() and currusers and currusers.exists():
            return { 'role': arcusers.first().role,
                     'curr_role': currusers.first().role,
                   }

class UserChangePassword(UpdateView):
    model = DefaultUser
    template_name = 'users/change_password.html'
    form_class = UserChangePasswordForm
    success_url = reverse_lazy('users:users')

    @allow_access([1], 'users:users')
    def post(self, request, pk, *args, **kwargs):
        passw1 = request.POST.get('new_password1', None)
        passw2 = request.POST.get('new_password2', None)

        if passw1 and passw2 and passw1 == passw2:
            users = DefaultUser.objects.filter(pk=pk)
            if users and users.count() == 1:
                user = users[0]
                user.set_password(passw1)
                user.save()
            else:
                error_message = u'Ошибка смены пароля, пользователь не найден'
                return HttpResponseRedirect(reverse_lazy('users:change_password', args=(pk, )) + '?%s' % urlencode({'error': error_message}))
        else:
            error_message = u'Ошибка смены пароля, пароли не совпадают'
            return HttpResponseRedirect(reverse_lazy('users:change_password', args=(pk, )) + '?%s' % urlencode({'error': error_message}))
        return HttpResponseRedirect(self.success_url)

    def form_invalid(self, form):
        error_message = ''
        err_json = json.loads(form.errors.as_json())
        print("LOG:", err_json)

        error_message = err_json
        return HttpResponseRedirect(reverse_lazy('users:change_password') + '?%s' % urlencode({'error': error_message}))

    def get_context_data(self, **kwargs):
        context = super(UserChangePassword, self).get_context_data(**kwargs)
        context['error_message'] = self.request.GET.get('error', '')
        return context

class UserDelete(DeleteView):
    model = DefaultUser
    template_name = 'users/delete.html'
    form_class = DeleteUserForm
    success_url = reverse_lazy('users:users')

    @allow_access([1], 'users:users')
    def post(self, request, *args, **kwargs):
        return super(UserDelete, self).post(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(UserDelete, self).get_context_data(**kwargs)

        # check presense before deletion
        pk = self.kwargs.get('pk', None)

        if pk:
            exists = Document.objects.filter(owner=pk).exists()
            if exists:
                context['error_message'] = u'Удаляемый пользователь используется в документе'
            exists = Item.objects.filter(owner=pk).exists()
            if exists:
                context['error_message'] = u'Удаляемый пользователь используется в ЕХ'
        return context

class Command(BaseCommand):
    from django.http import HttpRequest

    help = "Read all available users and all available not expired sessions. Then logout from each session."

    def handle(self, **options):
        now = django.utils.timezone.now()
        request = HttpRequest()

        sessions = Session.objects.filter(expire_date__gt=now)

        print(('LOG: Found %d not-expired session(s).' % len(sessions)))

        for session in sessions:
            username = session.get_decoded().get('_auth_user_id')
            request.session = self.init_session(session.session_key)

            logout(request)
            print(('LOG: Successfully logout %r user.' % username))

        print('LOG: All OK!')

    @staticmethod
    def init_session(session_key):
        """
        Initialize same session as done for ``SessionMiddleware``.

        engine = import_module(settings.SESSION_ENGINE)
        return engine.SessionStore(session_key)
        """

class RegistrationDisallowedView(TemplateView):
    def get_template_names(self):
        if 'account_type' in self.kwargs:
            return pathjoin('registration/complete/', self.kwargs['account_type'] + '.html')
        else:
            return super(RegistrationDisallowedView, self).get_template_names()


class ARCRegistrationView(BaseRegistrationView):
    template_name = 'users/registration/registration.html'
    form_class = UserCreateForm
    group = None
    success_url = '/'

    def get_success_url(self, user):
        print("LOG: SUCCESS URL:", self.success_url)

        if self.success_url:
            return self.success_url
        else:
            return super(ARCRegistrationView, self).get_success_url(user)

    def form_valid(self, form):
        print("LOG: REQUEST USER:", self.request.POST['username'], "\n")

        user = form.save()
        user = authenticate(username=self.request.POST['username'],password=self.request.POST['password1'])
        self.request.session['_auth_user_id'] = user.pk
        self.request.session['_auth_user_backend'] = user.backend

        super(ARCRegistrationView, self).form_valid(form)
        user.backend = 'django.contrib.auth.backends.ModelBackend'
        login(self.request, user)
        return HttpResponseRedirect("/")

    def get_form_kwargs(self, request=None, form_class=None):
        kwargs = super(ARCRegistrationView, self).get_form_kwargs()
        return kwargs

    def register(self, request, **cleaned_data):
        try:
            new_user = super(ARCRegistrationView, self).register(request, **cleaned_data)
        except:
            return
            pass
        new_user.is_active = True
        new_user.save(update_fields=['is_active'])
        print("LOG: REGISTER USER:", new_user)
        return new_user

get_disallowed_url = lambda account_type: reverse_lazy('registration_disallowed', kwargs={'account_type': account_type})


class UserDocs(ListView):
    model = DefaultUser
    template_name = 'users/details.html'

    def get_context_data(self, **kwargs):
        context = super(UserDocs, self).get_context_data(**kwargs)

        self.search = self.request.GET.get('s', None)

        if self.search:
            self.search = self.search.replace(' ' , '')
            print('LOG: %s "%s": %s' % (self.__class__, inspect.stack()[0][3], self.search))
            idx = ArcUser.objects.filter(search__contains=self.search.lower()).values('user__id')
            context['object_list'] = DefaultUser.objects.filter(id__in=idx).order_by('arcuser__role', 'id')
        else:
            context['object_list'] = DefaultUser.objects.all().order_by('arcuser__role', 'id')
        return context

