# coding: utf-8
import datetime

from dateutil.relativedelta import relativedelta


# Size units
KILOBYTE = 1024
MEGABYTE = KILOBYTE * 1024

# Time units
MINUTE = 60
HOUR = MINUTE * 60
DAY = HOUR * 24
ONE_DAY = datetime.timedelta(days=1)
ONE_WEEK = datetime.timedelta(weeks=1)
ONE_MONTH = relativedelta(months=1)
ONE_YEAR = relativedelta(years=1)
