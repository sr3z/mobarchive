# coding: utf-8
import calendar
import datetime
from datetime import timedelta
from random import randrange


from dateutil.relativedelta import relativedelta
from dateutil.rrule import rrule, DAILY
from .constants import DAY


def days_before(days_num):
    """
    Return date which was N days ago
    """
    return _get_relative_date('days', days_num)


def days_after(days_num):
    """
    Return date which will be N days later from today
    """
    return _get_relative_date('days', -days_num)


def months_before(months_num):
    """
    Return date which was N months ago
    """
    return _get_relative_date('months', months_num)


def months_after(months_num):
    """
    Return date which will be N month later from today
    """
    return _get_relative_date('months', -months_num)


def years_before(years_num):
    """
    Return date which was N years ago
    """
    return _get_relative_date('years', years_num)


def years_after(years_num):
    """
    Return date which will be N years later from today
    """
    return _get_relative_date('years', -years_num)


def _get_relative_date(units, value):
    """
    Function to calculate relative dates
    :param units: days, months, years
    :param value: number of given units
    :return: some date
    """
    return datetime.date.today() - relativedelta(**{units: value})


def get_range(date_from, date_to):
    return get_min_datetime(date_from), get_max_datetime(date_to)


def get_min_datetime(date_value):
    return datetime.datetime.combine(date_value, datetime.time.min)


def get_max_datetime(date_value):
    return datetime.datetime.combine(date_value, datetime.time.max)


def get_last_day_of_month(date_value):
    last_day = calendar.monthrange(date_value.year, date_value.month)[1]
    return get_max_datetime(date_value.replace(day=last_day))


def get_age_from_birth_date(date_value):
    return relativedelta(datetime.date.today(), date_value).years


def get_current_hour(date_value):
    return datetime.datetime.now().hour # int(datetime.date.today().strftime('%H'))


def seconds_to_datestring(seconds):
    """
    Converts given number of seconds to pretty date string
    :param seconds:
    :return:
    """
    # TODO. Refactor
    if seconds is not None:
        minutes, seconds = divmod(seconds, 60)
        hours, minutes = divmod(minutes, 60)
        days, hours = divmod(hours, 24)
        out = []
        if days:
            out.append('%dd' % days)
        if hours or days:
            out.append('%dh' % hours)
        if minutes or hours or days:
            out.append('%dm' % minutes)
        out.append('%ds' % seconds)
        return ' '.join(out)
    return ''


def daterange(start, end=None, reverse=False):
    if end is None:
        end = datetime.date.today()
    if start > end:
        start, end = end, start
        reverse = True
    result = (dt.date() for dt in rrule(DAILY, dtstart=start, until=end))
    if reverse:
        return reversed(list(result))
    return result


def random_date(start, end):
    """
    This function will return a random datetime between two datetime
    objects.
    """
    delta = end - start
    int_delta = (delta.days * DAY) + delta.seconds
    if not int_delta:
        return start
    random_second = randrange(abs(int_delta))
    return min(start, end) + timedelta(seconds=random_second)


def get_weekdays_choices():
    """
    Return list of choices for form with specific values to store in DB.
    :return:
    """
    WEEK_LENGTH = 7
    return tuple(zip(list(range(WEEK_LENGTH)),
                      [day_name for day_name in calendar.day_name]))
