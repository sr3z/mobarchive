# coding: utf-8
from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.shortcuts import redirect
from django.utils.http import urlencode
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect
from archive.users.models import ArcUser

IE_HEADER = 'CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"'


def add_ie_headers(method):
    """
    Adds IE headers, which are required to set cookies in iframe.
    :param method: should return HttpResponse instance
    :return:
    """

    def wrapper(self, request, *args, **kwargs):
        result = method(self, request, *args, **kwargs)
        result['P3P'] = IE_HEADER
        return result

    return wrapper


class classproperty(object):
    """
    Provides @property to @classmethod
    """
    def __init__(self, fget):
        self.fget = fget

    def __get__(self, owner, cls):
        return self.fget(cls)


def anonymous_required(func):
    """
    function redirects to a specified URL if authenticated.
    Can be useful if you wanted to prevent authenticated users from
    accessing signup pages etc.

    Example Usage

        anonymous_required(contrib.auth.views.login),
        or
        @anonymous_required
    """
    def as_view(request, *args, **kwargs):
        redirect_to = request.GET.get(REDIRECT_FIELD_NAME, settings.LOGIN_REDIRECT_URL)
        if request.user.is_authenticated():
            return redirect(redirect_to)
        response = func(request, *args, **kwargs)
        return response
    return as_view

def set_modified(func):
    """
    set modified_by user info
    """
    def wrapper(self, form):
        response = func(self, form)

        obj = form.save()
        obj.modified_by = self.request.user.id
        obj.save(update_fields=['modified_by'])
        return response

    return wrapper

def set_created(func):
    """
    set created_by user info
    """
    def wrapper(self, form):
        response = func(self, form)

        obj = form.save()
        obj.created_by = self.request.user.id
        obj.modified_by = self.request.user.id
        obj.save(update_fields=['created_by', 'modified_by'])
        return response

    return wrapper

def allow_access(roles=[], resource=None):
    """
    check access to resource and redirect if forbidden
    """
    def check_access(function):
        def wrapper(self, request, *args, **kwargs):
            try:
                user = ArcUser.objects.get(user=request.user)
            except:
                user = None

            if user and user.role in roles:
                print("LOG: STORAGE: ", str(function), "GETUSER_ROLE:", user.role)
            else:
                print("LOG: STORAGE: ", str(function), "FORBIDDEN, REDIRECT TO /, GETUSER_ROLE:", user.role if hasattr(user, 'role') else 'None')
                if resource:
                    error_message = 'Роль пользователя не позволяет редактировать запись'
                    return HttpResponseRedirect(reverse_lazy(resource) + '?%s' % urlencode({'error': error_message}))
                else:
                    return HttpResponseRedirect("/")
            return function(self, request, *args, **kwargs)
        return wrapper
    return check_access
