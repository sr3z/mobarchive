# -*- coding: utf-8 -*-

from django.utils.encoding import force_text


class APIException(Exception):
    '''
    api exception class
    '''

    def __init__(self, msg, description='', data={}):
        '''
        api exception init
        '''

        self.msg = force_text(msg)
        self.description = force_text(description)
        self.data = data


    def __str__(self):
        '''
        api exception presentation
        '''

        return str('ApiException: {msg} : {description} : {data}'.format(**self.__dict__)).encode('utf-8')
