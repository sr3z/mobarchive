# coding: utf-8
from itertools import zip_longest

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Div, ButtonHolder, Submit
from django import forms
from django.utils.functional import cached_property

from utils.constants import ONE_DAY
from utils.datetools import get_range
from utils.logger import log_warning


class FilterForm(forms.Form):
    """
    This is the classic implementation of FilterForm concept
    """
    columns = None
    columns_main = None
    model = None
    static_filters = {}
    default_ordering = '-id'
    ordering_mapping = {}  # Manual introspection replacement
    filter_mapping = {}

    def make_list_from_grid(self):
        output = []
        for row in zip_longest(*self.columns):
            out = []
            for value in row:
                out.append(
                    'PLACEHOLDER' if value not in self.fields else self[value]
                )
            output.append(out)
        return output

    @cached_property
    def cols(self):
        return self.make_list_from_grid()

    @cached_property
    def cols_main(self):
        output = []
        for field_name in self.columns_main:
            if field_name in self.fields:
                output.append(self[field_name])
        return output

    def check(self, value):
        return value in self.cleaned_data and bool(self.cleaned_data[value])

    def make_query(self):
        """
        This neat function returns queryset with all matched (by form)
        instances of given (by lead_class_name) model
        """
        qs = self.model.objects.filter(**self.filters)
        ordering = self.data.get("order", self.default_ordering)
        if isinstance(ordering, str):
            # TODO. fix with orders which contains dash not in a first position
            if ordering.replace('-', '') in self.ordering_mapping:
                if ordering.startswith('-'):
                    prefix = '-'
                else:
                    prefix = ''
                qs = qs.order_by(prefix + self.ordering_mapping[ordering.replace('-', '')])
            elif ordering in self.available_ordering:
                qs = qs.order_by(ordering)
            else:
                log_warning(
                    'Invalid ordering: %s (%s)' % (ordering, self.model)
                )
        elif isinstance(ordering, (tuple, list)):
            ordering = [i for i in ordering if i in self.available_ordering]
            qs = qs.order_by(*ordering)
        return qs

    @property
    def available_ordering(self):
        # TODO. Add introspection mechanism
        positive_ordering = self.model._meta.get_all_field_names()
        negative_ordering = ['-' + value for value in positive_ordering]
        return positive_ordering + negative_ordering

    @property
    def filters(self):
        filters = self.get_custom_filters()
        filters.update(self.static_filters)
        return filters

    def get_custom_filters(self):
        """
        Construct custom filters from mapping
        """
        filters_dict = {}
        data = self.cleaned_data
        for key, value in self.filter_mapping.items():
            if self.check(value):
                if key == 'created__range':
                    filtering_value = (get_range(data[value][0], data[value][1]))
                elif key == 'created__lt':
                    filtering_value = data[value] + ONE_DAY
                else:
                    filtering_value = data[value]
                if isinstance(filtering_value, list):
                    # Not use filtering with empty array value
                    if not any(filtering_value):
                        continue
                    if not key.endswith('__in'):
                        key += '__in'
                filters_dict[key] = filtering_value
        return filters_dict


class ProxyForm(forms.Form):
    """
    Form with hidden inputs, uses to simulate behavior of real external form.
    """

    def __init__(self, *args, **kwargs):
        super(ProxyForm, self).__init__(*args, **kwargs)
        for k, v in args[0].items():
            self.fields[k] = forms.CharField(initial=v, required=False, widget=forms.widgets.HiddenInput())


class CrispyModalForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CrispyModalForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_method = "POST"
        self.helper.enctype = "multipart/form-data"
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-sm-2"
        self.helper.field_class = "col-sm-10"
        self.helper.layout.append(Div(
            ButtonHolder(
                Submit('submit', 'Save', css_class='btn btn-success'),
                css_class='col-sm-offset-2 col-sm-10'
            ),
            css_class='form-group'
        ))


class CrispyModalAjaxForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CrispyModalAjaxForm, self).__init__(*args, **kwargs)

        update_popover(self)

        self.helper = FormHelper(self)
        self.helper.form_method = "POST"
        self.helper.enctype = "multipart/form-data"
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-sm-offset-1 col-sm-3 label-white"
        self.helper.field_class = "col-sm-7"
        self.helper.layout.append(Div(
            ButtonHolder(
                Submit('submit', 'Save', css_class='btn btn-warning btn-wide btn-custom'),
                css_class='col-sm-offset-4 col-sm-8'
            ),
            css_class='form-group'
        ))


def update_popover(self):
    for field in self.fields:
        help_text = self.fields[field].help_text
        self.fields[field].help_text = None
        if help_text != '':
            self.fields[field].widget.attrs\
                .update({'class': 'has-popover', 'data-content': help_text, 'data-placement': 'right', 'data-container': 'body'})
