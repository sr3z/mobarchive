# coding: utf-8
import datetime

from django.conf import settings
from django.core.urlresolvers import reverse
from django.template.loader import render_to_string

if settings.USE_ELASTIC == True:
    from haystack.query import SearchQuerySet

import re
from .datetools import get_age_from_birth_date


birth_number_re = re.compile(r'^(?P<birth_date>\d{6})/?(?P<unique_id>\d{3,4})$')


def parse_birth_number(value):
    match = birth_number_re.match(value)
    if match is None:
        return None, None
    return match.groupdict()['birth_date'], match.groupdict()['unique_id']


def parse_int(val):
    """
    Convert string to integer otherwise None
    :param val:
    :return:
    """
    res = ''
    if isinstance(val, str):
        for c in val.strip():
            if c.isdigit():
                res += c
            else:
                break
        return int(res) if res else None
    elif isinstance(val, int):
        return val


def get_age_from_birth_number(value):
    birth_date = get_birth_date_from_birth_number(value)
    if birth_date is None:
        return None
    return get_age_from_birth_date(birth_date)


def reverse_absolute_uri(request, name, kwargs=None):
    return request.build_absolute_uri(reverse(name, kwargs=kwargs))

def get_remote_addr(request):
    return request.META.get('HTTP_X_REAL_IP', request.META.get('REMOTE_ADDR'))


def closest(collection, target):
    return min(collection, key=lambda x: abs(target - x))


def closest_up(collection, target):
    if target in collection:
        return target
    else:
        return min(collection, key=lambda x: target / x)

def queryset_gen(search_results):
    if settings.USE_ELASTIC:
        search_qs = search_results
        for item in search_qs:
            yield item.object
    else:
        pass
        

def djqs_from_search(model, search_q):
    if settings.USE_ELASTIC:
        search_results = SearchQuerySet().models(model).filter(content__contains=search_q)
        if search_results.count() > 0:
            search_results = queryset_gen(search_results)
            object_ids = []
            try:
                object_ids = [ i.pk for i in search_results ]
            except:
                object_ids = []
            return model.objects.filter(pk__in=object_ids)
        else:
            return model.objects.none()
    else:
        return model.objects.none()
