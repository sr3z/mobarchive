# -*- coding: utf-8 -*-
from archive.users.models import License, ArcUser

def license(request):
    license = True
    return {
        'LICENSE': license,
        'search_placeholder': u'Искать',
    }
