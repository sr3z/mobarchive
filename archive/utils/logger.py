# coding: utf-8
import logging
import traceback

from django.conf import settings
from django.core.mail import mail_admins
from django.utils.encoding import force_text
from celery import task

import os
from .helpers import get_remote_addr


def log_message(level, msg, log_name=None, logger_name='plugins', **kwargs):
    # Initialize log and mapping for logging methods
    logger = logging.getLogger(logger_name)
    LOG_MAPPING = {
        "INFO": logger.info,
        "DEBUG": logger.debug,
        "WARNING": logger.warning,
        "ERROR": logger.error,
        "CRITICAL": logger.critical,
        "EXCEPTION": logger.exception,
    }
    # Print log message to stdout
    if log_name is not None:
        msg = "[%s] %s" % (log_name, msg)
    LOG_MAPPING[level](msg)
    # Add message to DB, or print exception if saving fails
    try:
        pass
        #LogItem.objects.create(level=level, msg=force_text(msg), **kwargs)
    except BaseException as error:
        message = "Logrotator item not saved: %s", error
        logger.exception(message)


def log_exception(extra_msg=None, fail_silently=False, **kwargs):
    """
    Uses for exceptions logging
    """
    message = traceback.format_exc()
    # Remove redundant information from traceback message
    message = message.replace('Traceback (most recent call last):\n  ', '')
    log_message('EXCEPTION', message, **kwargs)
    if extra_msg is not None:
        log_message('EXCEPTION', extra_msg, **kwargs)
    if not fail_silently and not settings.DEBUG and os.environ['DJANGO_SETTINGS_MODULE'] != 'settings.test_sqlite':
        subject = 'ERROR: ' + parse_exception_name(message)
        mail_admins_celery.delay(subject, message)
        if settings.JABBER_NOTIFICATION_ENABLED:
            jabber_admins(message)


###############################################################################
### SHORTCUTS
###############################################################################
def log_info(*args, **kwargs):
    log_message('INFO', *args, **kwargs)


def log_debug(*args, **kwargs):
    log_message('DEBUG', *args, **kwargs)


def log_warning(*args, **kwargs):
    log_message('WARNING', *args, **kwargs)


def log_error(*args, **kwargs):
    log_message('ERROR', *args, **kwargs)


def log_critical(*args, **kwargs):
    log_message('CRITICAL', *args, **kwargs)


def log_dispatch(method):
    """
    Logs input request and returned content.
    :param method:
    :return:
    """

    def wrapper(self, request, *args, **kwargs):
        if 'stop_hitting_me' not in request.COOKIES:
            ua = request.META.get('HTTP_USER_AGENT', '')
            log_debug(
                '(%s) Request to %s from %s with:\nGET: %s\nPOST: %s\nUSER_AGENT: %s' %
                (self.__class__.__name__, request.path, get_remote_addr(request),
                 '\n'.join(['\t' + k + ': ' + v for k, v in request.GET.items()]),
                 '\n'.join(['\t' + k + ': ' + v for k, v in request.POST.items()]),
                 ua)
            )
        result = method(self, request, *args, **kwargs)
        if hasattr(result, 'rendered_content'):
            content = result.rendered_content
        else:
            content = result.content
        if 'stop_hitting_me' not in request.COOKIES:
            log_debug('(%s) Response content to %s: %s' % (
                self.__class__.__name__, get_remote_addr(request), content)
            )

        return result

    return wrapper


def parse_exception_name(msg):
    trace_lines = msg.split('\n')
    if len(trace_lines) > 1:
        return trace_lines[-2]
    return trace_lines[0]
