# -*- coding: utf-8 -*-


from django.db import models
from django.utils.translation import ugettext_lazy as _
from model_utils.fields import AutoCreatedField, AutoLastModifiedField
from django.contrib.auth.models import User as DefaultUser, Group


class BaseParameter(models.Model):
    name = models.CharField(_(u'Наименование'), max_length=100, unique=True)
    description = models.TextField(_(u'Описание'), null=True, blank=True)
    enabled = models.BooleanField(default=True, verbose_name=_(u'Состояние'))

    class Meta:
        abstract = True


class TimeStampedModelIndexed(models.Model):
    created = AutoCreatedField(_('Создан'), db_index=True)
    created_by = models.IntegerField(_(u'№ пользователя'), default=-1)
    modified = AutoLastModifiedField(_('Изменен'))
    modified_by = models.IntegerField(_(u'№ пользователя'), default=-1)

    class Meta:
        abstract = True
