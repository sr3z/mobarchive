# coding: utf-8
import random
import re

from django import template
from django.contrib.gis.geoip2 import GeoIP2
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.template.loader import render_to_string
from django.utils.encoding import iri_to_uri
from classytags.arguments import Argument
from classytags.core import Tag, Options


register = template.Library()

email_re = re.compile(r'^(\S+)@(\S+)$')


@register.filter
def shade_data(data):
    if isinstance(data, str):
        emailmatch = email_re.search(data)
        if emailmatch:
            user, domain = emailmatch.groups()
            return shade(user) + '@' + shade(domain)
    return shade(data)


def shade(string):
    """
    Get first element of the string, x[0]
    then add len(x)-2 (first&last) times '*',
    and add original latest element of string x[-1]
    """
    if string is None or not isinstance(string, str):
        return string
    string_len = len(string)
    if string_len < 2:
        return string
    return string[:1] + "*" * (string_len - 2) + string[-1:]


@register.simple_tag
def random_color():
    return "%06x" % random.getrandbits(24)


@register.simple_tag
def cur(request, pattern):
    """
    Same as active but for new templates
    """
    if re.search(pattern, request.path):
        return 'cur'
    return ''


@register.filter
def phone_with_dash(phone):
    if len(phone) == 9:
        return '+420%s' % phone
    elif len(phone) == 12:
        return '+%s' % phone
    else:
        return phone


@register.filter
def as_country_by_ip(ip):
    return GeoIP2().country_name_by_addr(ip) or '-'


@register.filter
def as_form(form):
    return render_to_string(
        'utils/forms/form_as_form.html',
        dict(form=form)
    )

@register.filter
def as_form_search(form):
    return render_to_string(
        'utils/forms/form_as_form_search.html',
        dict(form=form)
    )

@register.filter
def as_filterform(form):
    return render_to_string(
        'utils/forms/form_as_filterform.html',
        dict(form=form)
    )


@register.filter
def as_tableform(form):
    return render_to_string(
        'utils/forms/form_as_tableform.html',
        dict(form=form)
    )

class Orderable(Tag):
    """
    Allows to simply defines orderable columns. With proper images and links.
    NOTE. Param field_name have to be defined as ordered in corresponded django-filter.FilterSet subclass.
    Syntax:
        {% orderable 'id' %}
        {% orderable 'id' 'Displaying column name' %}
    """
    name = 'orderable'
    options = Options(
        Argument('field_name'),
        Argument('verbose_field_name', required=False, resolve=True),
        Argument('force_sort_from_url', required=False, resolve=False)
    )

    def render_tag(self, context, field_name, verbose_field_name, force_sort_from_url=False):
        request = context['request']
        if force_sort_from_url or not 'object_list' in context:
            sorting_direction = self.get_sorting_direction_from_url(request, field_name)
        else:
            sorting_direction = self.get_sorting_direction_from_queryset(context['object_list'], field_name)

        sort_value = '-' + field_name if sorting_direction == 'up' else field_name

        template_context = {
            'url': request.path + '?' + self.url_replace(request, 'o', sort_value),
            'image': self.get_link_image(sorting_direction),
            'verbose_field_name': verbose_field_name or field_name.title()
        }
        return '<a href="%(url)s" class="sort">%(image)s%(verbose_field_name)s</a>' % template_context

    def get_sorting_direction_from_queryset(self, queryset, field_name):
        ordering = queryset.query.order_by
        if field_name in ordering:
            direction = 'up'
        elif '-' + field_name in ordering:
            direction = 'down'
        else:
            direction = None
        return direction

    def get_sorting_direction_from_url(self, request, field_name):
        """
        Fallback algorithm. Uses on pages where is no object_list in context. Direction arrows will be shown only if
        proper url params exists.
        :param request:
        :param field_name:
        :return:
        """
        querystring = iri_to_uri(request.META.get('QUERY_STRING', ''))
        if 'o=' in querystring:
            if 'o=' + field_name in querystring:
                direction = 'up'
            elif 'o=-' + field_name in querystring:
                direction = 'down'
            else:
                direction = None
        else:
            direction = None
        return direction

    def get_link_image(self, direction):
        """
        Returns img tag with arrow.
        :param direction:
        :return:
        """
        if direction is None:
            image = ''
        else:
            image = '<img src="%(url)s" alt="sort %(direction)s" title="sort %(direction)s">' % {
                'url': static('img/sort-alt-%s.png' % direction),
                'direction': direction
            }
        return image

    @staticmethod
    def url_replace(request, name, value):
        """
        Replaces GET param with some value and return urlencoded string
        :param request:
        :param name:
        :param value:
        :return:
        """
        params = request.GET.copy()
        params[name] = value
        return params.urlencode()


register.tag(Orderable)
