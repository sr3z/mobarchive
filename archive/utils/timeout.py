from functools import wraps
import errno
import os
import signal
from django.conf import settings
from utils.logger import log_exception, log_info, log_debug, log_warning

class TimeoutError(Exception):
    pass

def timeout(func):

    def _handle_timeout(signum, frame):
        raise TimeoutError(os.strerror(errno.ETIME))

    def wrapper(self, *args, **kwargs):
        seconds = 60

        if 'timeout' in kwargs:
            seconds = kwargs['timeout']
            log_info("PERFORMANCE SECONDS: %s" % (seconds))
        signal.signal(signal.SIGALRM, _handle_timeout)
        signal.alarm(seconds)
        result = None

        try:
            result = func(self, *args, **kwargs)
        finally:
            signal.alarm(0)
        return result

    return wrapper

