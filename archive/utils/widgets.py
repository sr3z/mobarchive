# coding: utf-8
import datetime

from django import forms
from django.conf import settings
from django.forms.widgets import CheckboxInput, ClearableFileInput
from django.utils.safestring import mark_safe
from django.utils.html import escape, conditional_escape


class ClearableImageInput(ClearableFileInput):
    template_with_initial = '%(input)s %(clear_template)s<br />%(initial)s'

    def render(self, name, value, attrs=None):
        substitutions = {
            'initial_text': self.initial_text,
            'input_text': self.input_text,
            'clear_template': '',
            'clear_checkbox_label': self.clear_checkbox_label,
        }
        template = '%(input)s'
        substitutions['input'] = super(ClearableFileInput, self).render(name, value, attrs)

        if value and hasattr(value, "url"):
            template = self.template_with_initial
            substitutions['initial'] = (
                '<a href="%s"><img src="%s" /></a>' % (
                    escape(value.url), escape(value.url)
                )
            )
            if not self.is_required:
                checkbox_name = self.clear_checkbox_name(name)
                checkbox_id = self.clear_checkbox_id(checkbox_name)
                substitutions['clear_checkbox_name'] = conditional_escape(checkbox_name)
                substitutions['clear_checkbox_id'] = conditional_escape(checkbox_id)
                substitutions['clear'] = CheckboxInput().render(checkbox_name, False, attrs={'id': checkbox_id})
                substitutions['clear_template'] = self.template_with_clear % substitutions

        return mark_safe(template % substitutions)


class MultiSelectWidget(forms.SelectMultiple):
    """
    jQuery UI Multiselect
    """
    class Media:
        css = {
            'all': (
                'css/jquery.multiselect.css',
                'css/jquery.multiselect.filter.css',
            )
        }
        js = (
            'js/lib/jquery.multiselect.min.js',
            'js/lib/jquery.multiselect.filter.min.js',
        )

    def __init__(self, language=None, attrs=None):
        self.enable_filter = attrs.get('enable_filter', False) if attrs else False
        self.header_text = attrs.get('header_text', False) if attrs else False
        self.language = language or settings.LANGUAGE_CODE[:2]
        super(MultiSelectWidget, self).__init__(attrs=attrs)

    def render(self, name, value, attrs=None):
        rendered = super(MultiSelectWidget, self).render(name, value, attrs)
        filter_str = '.multiselectfilter();' if self.enable_filter else ''
        if self.header_text is False:
            header_str = '{ header: false }'
        elif self.header_text is True:
            header_str = '{ header: true }'
        else:
            header_str = '{ header: \'' + self.header_text + '\'}'
        return rendered + mark_safe('''<script type="text/javascript">
            $(document).ready(function() {
                var elem = $('#id_%(name)s');
                if (elem.multiselect) {
                    elem.multiselect(%(header)s)%(filter)s
                } else {
                    console.log('jQuery UI Multiselect library not defined!');
                }
            });
            </script>''' % {'name':name, 'header': header_str, 'filter': filter_str})


class BotWidget(forms.HiddenInput):
    """
    Bot check widget
    """
    def __init__(self, language=None, attrs=None):
        self.language = language or settings.LANGUAGE_CODE[:2]
        super(BotWidget, self).__init__(attrs=attrs)

    def render(self, name, value, attrs=None):
        rendered = super(BotWidget, self).render(name, value, attrs)
        day = str(datetime.date.today().day)
        return rendered + mark_safe('''<script type="text/javascript">
            $(document).ready(function() {
                setTimeout(function() {
                    var $elem = $('#id_%(name)s');
                    $elem.parent().parent().submit(function() {
                        $elem.val('%(day)s')
                    })
                }, 1000);
            });
            </script>''' % {'name':name, 'day':day})
