
import os
from celery import Celery
from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'archive.settings.base')
app = Celery('archive',
                broker='redis://localhost:6379',
                backend='redis://localhost:6379',
                include=['archive.api.tasks'])

app.conf.update(
    result_expires=3600,
)

if __name__ == '__main__':
    app.start()
