#! /bin/bash

trap ctrl_c INT
#trap 'abort' 0

set -e

function ctrl_c() {
  echo "** Trapped CTRL-C"
  exit
}

#abort()
#{
#  echo >&2 '
#  ***************
#  *** ABORTED ***
#  ***************
#  '
#  echo "An error occurred. Exiting..." >&2
#  exit 1
#}

echo
echo "STORAGE makemigrations .."
python manage.py makemigrations storage
echo "STORAGE migrate .."
python manage.py migrate

echo
echo "AUTH .."
python manage.py migrate auth

echo
echo "USERS .."
python manage.py makemigrations users
python manage.py migrate

echo
echo "API .."
python manage.py makemigrations api
python manage.py migrate

echo
echo "UTILS .."
python manage.py makemigrations utils
python manage.py migrate

echo
echo "REPORT .."
python manage.py makemigrations reports
python manage.py migrate
