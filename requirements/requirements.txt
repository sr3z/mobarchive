amqp==2.1.4
appdirs==1.4.3
appnope==0.1.0
asn1crypto==0.22.0
attrs==16.3.0
Automat==0.5.0
backports.shutil-get-terminal-size==1.0.0
billiard==3.5.0.2
celery==4.0.2
cffi==1.10.0
constantly==15.1.0
cryptography==1.8.1
dateutils==0.6.6
decorator==4.0.11
Django==1.10
django-activeurl==0.1.9
django-appconf==1.0.2
django-braces==1.11.0
django-classy-tags==0.8.0
django-cors-headers==2.0.2
django-cors-middleware==1.3.1
django-crispy-forms==1.6.1
django-extensions==1.7.8
django-filter==1.0.2
django-guardian==1.4.6
django-model-utils==2.6.1
django-registration==2.2
djangorestframework==3.3.3
dynamic-rest==1.6.3
enum34==1.1.6
freeze==1.0.10
frozendict==1.2
geoip2==2.4.2
idna==2.5
incremental==16.10.1
inflection==0.3.1
ipaddress==1.0.18
ipython==5.3.0
ipython-genutils==0.2.0
kombu==4.0.2
lxml==3.3.0
Markdown==2.6.8
maxminddb==1.3.0
meld3==1.0.2
olefile==0.44
packaging==16.8
pexpect==4.2.1
pickleshare==0.7.4
prompt-toolkit==1.0.14
psycopg2==2.7.1
ptyprocess==0.5.1
pyasn1==0.2.3
pyasn1-modules==0.0.8
pyBarcode==0.7
pycparser==2.17
pycrypto==2.6.1
Pygments==2.2.0
pyOpenSSL==16.2.0
pyparsing==2.2.0
python-dateutil==2.6.0
python-nmap==0.6.0
pytz==2017.2
redis==2.10.5
requests==2.13.0
service-identity==16.0.0
simplegeneric==0.8.1
six==1.10.0
South==1.0.2
timeout-decorator==0.3.3
traitlets==4.3.2
Unipath==1.1
vine==1.1.3
wcwidth==0.1.7
zope.interface==4.3.3
django-extensions==1.7.8
openpyxl==2.4.5
# report generation packages start
# requires wkhtmltopdf installed in system
pdfkit==0.6.1
Jinja2==2.9.5
# report generation packages end
#django-haystack==2.6.1
#elasticsearch==5.4.0
#django-haystack-elasticsearch==0.1.0
#django-easy-pdf>=0.2.0.dev1
#WeasyPrint>=0.34
# following command needed to configure html to pdf:
# sudo aptitude install libfontconfig
# and https://wkhtmltopdf.org/downloads.html
wkhtmltopdf==0.2
django-wkhtmltopdf==3.1.0
xhtml2pdf==0.2b1
