from socket import *
import sys
import time
from codecs import decode

def hex_command(data):
    #data_str = '[ {} ]'.format(' ][ '.join(map(lambda i: str(i), data)))
    # print data_str
    map_data = ['{:02x}'.format(c) for c in data]
    #data_str_hex = '[ {} ]'.format(' ][ '.join(map(lambda c: '\\x{}'.format(c), map_data)))
    #print data_str_hex
    data_send = decode(''.join(map_data), 'hex_codec')
    print('DATA2:', data)
    return data_send

BUFFER_SIZE = 1024
RESPONSE_TIMEOUT = 3
host = '127.0.0.1'
port = 5000
port2 = 5001
protocol = 'udp'

if __name__ == "__main__":
    if len(sys.argv) > 1:
        if sys.argv[1]:
            host = sys.argv[1]

        if len(sys.argv) > 2:
            if sys.argv[2]:
                param2 = sys.argv[2]
                port = int(param2)

        if len(sys.argv) > 3:
            if sys.argv[3]:
                protocol = sys.argv[3]

        print("Using host:", host, " port:", port)
    else:
        print("Using default host:", host, " port:", port)

TIMEOUT=5
addr = (host,port)
addr2 = (host,port2)

if 'tcp' == protocol:
    s = socket(AF_INET, SOCK_STREAM)
    print('BIND: ', addr)
    s.bind(addr)
    s.listen(1)

    while True:
        conn, addr = s.accept()
        message = conn.recv(BUFFER_SIZE)

        if not message or len(message) < 4:
            print('OOPS! RECV: MESSAGE:', message, 'LEN:', len(message))
            continue
        data_recv = list(map(ord, message))
        message1 = hex_command(data_recv)
        print('RECV: DATA: ', list(message1), 'MESSAGE:', message)

        print('SLEEPING ', RESPONSE_TIMEOUT) 
        time.sleep(RESPONSE_TIMEOUT)
        parts = message.split(';')
        if ';58;' in message:
            response = ';'.join(parts[:3])
        elif ';54;' in message:
            parts[2] = '3'
            response = ';'.join(parts)
        else:
            response = ';'.join(parts)
        data_list = list(map(ord, response))
        message1 = hex_command(data_list)
        print('RESPONSE: DATA: ', list(message1), 'MESSAGE:', response)

        message1 = hex_command(data_list)
        print('SENDING: MESSAGE: ', message1)
        conn.send(message1)
        conn.close()

    exiti(0)

udp_socket = socket(AF_INET, SOCK_DGRAM)
udp_socket2 = socket(AF_INET, SOCK_DGRAM)

print('BIND: ', addr)
udp_socket.bind(addr)
print('BIND 2: ', addr2)
udp_socket2.bind(addr2)

'''
data = '\x02\x02\x00\x00' # open pssage
data = '\x01\x01\x00\x00' # stellage position 
data = '\x0b\x00\x00\x00'
data = '\x03\x00\x00\x00' # block mobile
data = '\x04\x00\x00\x00' # unblock mobile
data = '\x06\x00\x00\x00' # mode vent
'''

while True:
    print("LISTEN:")
    message = udp_socket.recvfrom(BUFFER_SIZE)
    client = message[1]
    data_recv = list(map(ord, str(message[0])))
    data1 = [int(data_recv[0]), int(data_recv[1]), int(data_recv[2]), int(data_recv[3])]
    print('BYTE[0]', data_recv[0], data_recv[0] == 11)

    if data_recv[0] == 11: # '\x0b':
        data2 = [int(data_recv[0]), int(data_recv[1]), int(1), 255]
    else:
        data2 = [int(data_recv[0]), int(data_recv[1]), int(data_recv[2]), 255]
    print('RECV: DATA: ', list(data1), 'MESSAGE:', message)

    print('SLEEPING ', RESPONSE_TIMEOUT) 
    time.sleep(RESPONSE_TIMEOUT)

    message1 = hex_command(data1)
    print('SENDING FROM 5001: MESSAGE: ', message1)
    udp_socket2.sendto(message1, client)

    message2 = hex_command(data2)
    print('SENDING FROM 5001: MESSAGE 2ND: ', message2)
    udp_socket2.sendto(message2, client)

udp_socket.close()
print("EXITING")
