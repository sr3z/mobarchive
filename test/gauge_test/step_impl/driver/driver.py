from selenium import webdriver
from distutils.spawn import find_executable


class DriverFactory(object):
    driver = None

    def __init__(self, *args):
        chromedriver_path = find_executable('chromedriver')
        print('\nchromedriver_path: ', chromedriver_path)

        if not chromedriver_path or len(chromedriver_path) == 0:
            self.driver = webdriver.Chrome(executable_path='/usr/local/chromedriver')
        else:
            self.driver = webdriver.Chrome(executable_path='chromedriver')
