# coding: utf-8
from getgauge.python import step, before_spec, after_spec, after_step, before_suite
from getgauge.python import DataStoreFactory
import time

from .settings import Settings
from step_impl.driver.driver import DriverFactory
from step_impl.pages.main_page import MainPage
from step_impl.shell import shell

page = None
factory = None

# --------------------------
# Gauge step implementations
# --------------------------

@after_step
def after_step_hook(arg=None):
    count = DataStoreFactory.spec_data_store().get('count')

    if count:
        count = count + 1
    else:
        count = 1
    DataStoreFactory.spec_data_store().put('count', count)

    try:
        result = DataStoreFactory.spec_data_store().get('result')
        print('result: ', result, '\n')
    except Exception as e:
        print(e, '\n')
        result = None
    DataStoreFactory.spec_data_store().put('result', None)

    red="\033[0;91m";
    grn="\033[0;32m";
    yel="\033[1;33m";
    blu="\033[1;36m";
    wht="\033[0m";

    if result:
        print(red, '\nFAILED [', count,']\n', wht)
    else:
        print(yel, '\nPASSED [', count,']\n', wht)

@before_spec
def before_spec_hook(arg=None):
    print("BEFORE SPEC START")
    factory = DriverFactory()
    print("DRIVER:", factory.driver, "\n")
    page = MainPage(factory.driver)
    print("PAGE:", page, "\n")
    DataStoreFactory.spec_data_store().put('page', page)
    count = DataStoreFactory.spec_data_store().put('count', 0)
    print("BEFORE SPEC DONE")

@after_spec
def after_spec_hook(arg=None):
    print("AFTER SPEC START")
    page = DataStoreFactory.spec_data_store().get('page')

    if page:
        page.driver.quit()
    print("AFTER SPEC DONE")

@before_spec("<clean_db>")
def before_spec_hook_tagged(arg=None):
    print("BEFORE SPEC TAGGED START")
    print("BEFORE SPEC TAGGED DONE")

@step("Register <name>")
def register(name):
    email = name + '@visitweb.com'
    password = '123456'
    print('\n=== Register "' + name +  '" ', email, '\n')
    page.register(name, password, email)

@step("Logout")
def logout():
    DataStoreFactory.spec_data_store().put('result', 'invalid')
    page = DataStoreFactory.spec_data_store().get('page')

    if page:
        result = page.logout_if_logged()
        DataStoreFactory.spec_data_store().put('result', result)

@step("Login <name>")
def login(name):
    DataStoreFactory.spec_data_store().put('result', 'invalid')
    password = name
    print('\n=== Login "' + name + '"', '\n')

    page = DataStoreFactory.spec_data_store().get('page')

    if page:
        result = page.login(name, password)
        DataStoreFactory.spec_data_store().put('result', result)

@step("Create new unit <name>")
def create_new_unit(name):
    DataStoreFactory.spec_data_store().put('result', 'invalid')
    unit = name
    print("Creatining new %s", unit)

    page = DataStoreFactory.spec_data_store().get('page')

    if page:
        result = page.create_new_unit(unit)
        DataStoreFactory.spec_data_store().put('result', result)

@step("Add campain <name> <campaign_type> <url> <image_url>")
def add_camp(name, campaign_type=None, url='', image_url=''):
    print('\n=== Add campain "' + name + '"', '\n')
    password = '123456'
    email = name + '@visitweb.com'
    
    if 'None' == campaign_type or '0' == campaign_type:
        campaign_type = None

    page.login(name, password)
    page.visit()
    page.visit('v2/campaigns.php')
    page.click_by_href("campaigns_edit.php?new")

    if not 'http://' in url and not 'https://' in url:
        url = 'http://' + url
    page.add_campaign("ads_one", url, email, campaign_type, image_url)

@step("Add site <name> <add_replace> <url>")
def add_site(name, add_replace, url):
    print('\n=== Add site "' + name +  '"', '\n')
    email = name + '@visitweb.com'
    password = '123456'

    page.login(name, password)

    page.visit()
    page.visit("v2/webmaster_edit.php")
    page.click_by_href("webmaster_edit.php?new")

    if not 'http://' in url and not 'https://' in url:
        url = 'http://' + url
    page.add_site(url, Settings.WEBM_URL, int(add_replace), email)

@step("Check blocks <url> <num>")
def ads_one_user(url, num):
    shell.check_blocks('one_user')

@step("Check view <url> <views_count> <links_count>")
def check_view(url, views_count, links_count):
    print('\n=== Check view "', url, '"\n')
    page.check_view(url, int(views_count), int(links_count))

@step("Check WEBM click <url>")
def check_clicks(url):
    print('\n=== Check WEBM click ', url,'\n')
    WEBM_CLICK = 2
    result = page.check_clicks(url, WEBM_CLICK)
    DataStoreFactory.spec_data_store().put('result', result)

@step("Check DOUBLE click <url>")
def check_clicks(url):
    print('\n=== Check DOUBLE click ', url,'\n')
    DOUBLE_CLICK = 1
    result = page.check_clicks(url, DOUBLE_CLICK)
    DataStoreFactory.spec_data_store().put('result', result)

@step("Check click <url> block of <num> banners")
def check_clicks(url, num):
    print('\n=== Check click ', url,'\n')
    result = page.check_clicks(url)
    DataStoreFactory.spec_data_store().put('result', result)

@step("Make_screentshot")
def make_screenshot():
    page.screenshot()

@step("Set cost <email> <cost>")
def set_cost(email, cost):

    if not '@' in email:
        email = email + '@visitweb.com'

@step("Update browser <email> <type>")
def update_browser(email, b_type):
    if not b_type or b_type == '' or b_type == 'None':
        b_type = None

    if not '@' in email:
        email = email + '@visitweb.com'

@step("Clean ads")
def sleep_one_min():
    print('Clean ads DONE')
    pass

@step("Wait for ads generated")
def wait_for_ads():
    pass

    ret = 0
    count = 0
    dB = None

    while ret < 1:
        print('Sleep 10 seconds ..')
        time.sleep(10)
        count = count + 1

        if count > 6:
            break
        dB = None
    print('sleep done ..')

