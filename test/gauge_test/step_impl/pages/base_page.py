# coding: utf-8
import os
from selenium.webdriver.common.by import By
from step_impl.settings import Settings

class BasePage(object):
    URL = Settings.APP_ENDPOINT
    ADMIN_URL = '{}admin/'.format(URL)
    driver = None

    def __init__(self, driver):
        self.driver = driver

        if None == self.URL:
            self.URL = ''
        print("URL: ", self.URL)

    def doubleclick(self, element):
        from selenium.webdriver.common.action_chains import ActionChains

        elem = self.driver.find_element(*element)
        actionChains = ActionChains(self.driver)
        actionChains.double_click(elem).perform()

    def click(self, element):
        self.driver.find_element(*element).click()

    def setall(self, element, value):
        els = self.driver.find_elements(*element)

        for el in els:
            el.clear()
            el.send_keys(value)
            print("ELEMENT: ", element, " VALUE: ", value)

    def set(self, element, value):
        self.driver.find_element(*element).clear()
        self.driver.find_element(*element).send_keys(value)
        print("ELEMENT: ", element, " VALUE: ", value)

    def get(self, element):
        return self.driver.find_element(*element).text

    # def screenshot(self):
    #     self.driver.save_screenshot('./screenshot.png')

    def switch_to__iframe(self):
#        iframe = (By.TAG_NAME, "iframe")
        iframe = self.driver.find_element_by_tag_name("iframe")
        self.driver.switch_to_frame(iframe)
