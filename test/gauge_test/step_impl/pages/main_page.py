# coding: utf-8
import datetime
import time
import random 
from urllib.parse import urlparse

from step_impl.settings import Settings

from getgauge.python import DataStoreFactory
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException

from step_impl.pages.base_page import BasePage

DOUBLE_CLICK = 1
WEBM_CLICK = 2


class MainPageLocators:
    PASSWORD = (By.XPATH, "//input[contains(@type, 'password')]")
    USER_NAME = (By.NAME, 'username')
    LOGOUT = (By.XPATH, "//input[contains(@value, 'Выйти')]")
    LOGIN = (By.XPATH, "//input[contains(@onclick, 'goto_login()')]")

    ERROR = (By.XPATH, "//div[contains(@class, 'error res-div')]")
    SUBMIT = (By.XPATH, "//input[contains(@type, 'submit')]")
    NEXT = (By.XPATH, "//a[text()='Далее']")
    NEXT2 = (By.XPATH, "//a[contains(@class, 'confirm_targeting')]")
    NEXT3 = (By.XPATH, "//a[contains(@class, 'confirm_tracking')]")
    DONE = (By.XPATH, "//a[contains(@class, 'confirm_ads')]")
    LOAD = (By.XPATH, "//div[text()='Загрузить с компьютера']")
    ADS_TEXT = (By.ID, 'text_ads_1')
    IMAGE_URL= (By.ID, 'img_link')
    LINK = (By.ID, 'upload_type-link')
    SAVE_IMAGE = (By.ID, 'btn_add_new_ads')
    PAUSE = (By.XPATH, "//button[contains(@class, 'pause_play_ads')]")
    CONFIRM_PASSWORD = (By.ID, 'user_password_confirmation')
    EMAIL = (By.NAME, 'email')
    ADD_SITE = (By.XPATH, "//a[contains(@href, '/sites.php')]")
    ADD_CAMPAIN = (By.XPATH, "//a[contains(@href, '/camps.php')]")
    CAMPAIGN_NAME = (By.NAME, 'name')
    URL = (By.NAME, 'url')
    STAT_URL = (By.NAME, 'stat_url')
    URL_LINK = (By.NAME, 'url-link')
    CREATE_BLOCK = (By.ID, 'create-block')
    SUBMIT_BLOCK = (By.ID, 'add-blocks-submit')
    SITE_NEXT = (By.XPATH, "//button[contains(@onclick, 'blocks')]")
    SITE_NEXT2 = (By.XPATH, "//button[contains(@onclick, 'code')]")
    SITE_DONE = (By.XPATH, "//button[contains(@onclick, 'finish')]")
    CODE_BLOCK = (By.XPATH, "//button[contains(@class, 'fa btn btn-icon btn-icon-caption --m-l-xs fa-code')]")
    CODE_PLAIN = (By.XPATH, "//a[contains(@href, '#code-plain')]")
    CODE_CLOSE = (By.XPATH, "//span[text()='×']")
    #CAMPAIGN_TYPE  = (By.XPATH, "//p[contains(@class, 'CaptionCont SelectBox')]")
    CAMPAIGN_TYPE  = (By.XPATH, "//span[text()='CPC']")
    TYPE  = (By.XPATH, "//li[contains(@data-val, 'clu')]")

class MainPage(BasePage):
    URL = BasePage.URL
    db = None

    def __init__(self, driver):
        print("MainPage INIT ..")
        super(MainPage, self).__init__(driver=driver)

    def fill(self):
        user_name = 'test_web'
        password = '123456'
        self.setall(MainPageLocators.USER_NAME, user_name)
        self.setall(MainPageLocators.PASSWORD, password)
        self.click(MainPageLocators.SUBMIT)
        DataStoreFactory.scenario_data_store().put('current_user', user_name)

    def add_campaign(self, campaign_name = "ads_one", url = 'visitweb.com', email = None, clickunder = None, image_url = ''):
        assert('campaigns_edit.php?new' in self.driver.current_url)

        #step 1
        self.set(MainPageLocators.URL, url)

        if clickunder:
            self.set(MainPageLocators.CAMPAIGN_NAME, campaign_name + '_clickunder')

            self.click(MainPageLocators.CAMPAIGN_TYPE)
            self.click(MainPageLocators.TYPE)
        else:
            self.set(MainPageLocators.CAMPAIGN_NAME, campaign_name)
        self.click(MainPageLocators.NEXT)
        
        #step 2
        elem = self.driver.find_element_by_tag_name('body')
        elem.send_keys(Keys.PAGE_DOWN)

        self.click(MainPageLocators.NEXT2)

        #step 3
        self.set(MainPageLocators.URL_LINK, url)

        self.click(MainPageLocators.NEXT3)

        if not clickunder:
            #step 4
            self.click(MainPageLocators.LOAD)

            self.click(MainPageLocators.LINK)

            self.set(MainPageLocators.ADS_TEXT, campaign_name + '_' + email)

            self.set(MainPageLocators.IMAGE_URL, image_url)
            
            self.click(MainPageLocators.SAVE_IMAGE)
            time.sleep(3)
            self.click(MainPageLocators.SAVE_IMAGE)
            time.sleep(3)
            self.click(MainPageLocators.PAUSE)
            self.click(MainPageLocators.DONE)

        #self.make_screenshot('.3')

    def register(self, user_name = None, password_in = None, email_in = None, ignore_if_exists = 0):
        self.logout_if_logged()

        url = '{}reg.php/'.format(self.URL)
        self.visit(url)
        time.sleep(1)

        if self.URL == self.driver.current_url:
            print("NOT LOGGED OUT !!")
            self.logout_if_logged()
            self.visit(url)
            time.sleep(1)

        assert(url == self.driver.current_url)

        if None != password_in:
            password = password_in
        else:
            password = '123456'

        self.setall(MainPageLocators.USER_NAME, user_name)
        self.setall(MainPageLocators.PASSWORD, password)
        self.set(MainPageLocators.EMAIL, email_in)

        #self.make_screenshot()

        buttons = self.driver.find_elements_by_tag_name('input')

        for but in buttons:
            value = but.get_attribute("value")

            if value:
                if 'Зарегистрироваться' in value:
                    but.click()
                    break

        time.sleep(1)
        assert self.find_href('logout') == True
        DataStoreFactory.scenario_data_store().put('current_user', user_name)

    def login(self, user_name = None, password_in = None):
        #self.make_screenshot(".1")
        self.logout_if_logged()

        url = '{}login/'.format(self.URL)
        self.visit(url)
        time.sleep(1)

        if None != password_in:
            password = password_in
        else:
            password = '111'

        self.set(MainPageLocators.USER_NAME, user_name)

        passwd = self.driver.find_element_by_xpath("//input[contains(@value, 'Password')]")
        passwd.click()
        time.sleep(1)
        self.set(MainPageLocators.PASSWORD, password)

        self.click(MainPageLocators.SUBMIT)

        time.sleep(1)
        #self.make_screenshot(".3")

        try:
            errors = self.get(MainPageLocators.ERROR).encode('utf8')
        except NoSuchElementException:
            errors = None

        if errors:
            print(('\n', errors, '\n'))
        assert None == errors

        '''
        try:
            self.get(MainPageLocators.LOGOUT)
            logout = True
        except Exception as e:
            print('\nERROR', e, '\n')
            logout = None
        print('\nLOGOUT', logout, '\n')

        if logout:
            return None
        else:
            return 'not found'
        '''

    def logout_if_logged(self):
        
        #self.visit('logout/')
        #time.sleep(1)

        try:
            #logout = self.get(MainPageLocators.LOGOUT)
            logout = self.driver.find_element_by_xpath("//input[@value= 'Выйти'][@type= 'submit']")
        except NoSuchElementException:
            logout = None
            print("Input button not found") 
        except Exception as e:
            print(e)
            assert(True==False)

        if logout:
            print("Logged, logging out ..")
            logout.click()
            time.sleep(1)
            login = self.driver.find_element_by_xpath("//input[@value= 'Войти'][@type= 'button']")
            if login:
                print("The logged was successful")
        else:
            print("Not logged ..")

    def create_new_unit(self, unit):
        if unit == "document type":
            try:
                self.driver.find_element_by_id("five").click()
                #self.driver.find_element_by_css_selector("[//a[@href='/storage/document_types/']").click()
                #self.driver.find_element_by_xpath("//a[@href='/storage/document_types/']").click()
                #elem = self.driver.find_element_by_link_text("Тип документа")
                #elem.click()

                time.sleep(2)
                self.driver.get("http://archive.test:8090/storage/document_types/")
                time.sleep(2)

                self.driver.find_element_by_xpath("//input[@value='Создать новый'][@type='submit']").click()

                print("Logged in document type ...")

                #name = self.driver.find_element_by_id("id_name")
                #description = self.driver.find_element_by_id("id_description")

                time.sleep(1)
                #name = self.driver.find_element_by_xpath("//input[@id='id_name'][@type='text']").click()
                #name.click()
                description = self.driver.find_element_by_xpath("//textarea[@id='id_description'][@name='description']").click()
                time.sleep(1)
                self.set(description, "something text text")

                time.sleep(1)
                button = self.driver.find_element_by_xpath("//input[@value='Сохранить'][@type='submit']").click()

            except NoSuchElementException:
                logout = None
                print("Input button not found") 
                assert(True==False)
            except Exception as e:
                print(e)
                assert(True==False)

    def visit(self, url=None):
        if not url:
            url = self.URL
        elif url and not 'http' in url:
            url = '{}'.format(self.URL) + url
        print(('URL: ', url))

        self.driver.get(url)

        #self.make_screenshot()

    def fill_field(self, id, value):
        field = (By.ID, id)
        self.set(field, value)

    def click_by_class(self, contents):
        button = (By.CLASS_NAME, contents)
        self.click(button)

    def click_by_name(self, contents):
        button = (By.NAME, contents)
        self.click(button)

    def find_href(self, contents):
        if None == contents:
            print("Incorrect href !!")
            return

        hrefs = self.driver.find_elements_by_tag_name('a')

        for a in hrefs:
            href = a.get_attribute("href")

            if href:
                if contents in href:
                    return True
        return False

    def click_by_href(self, contents):
        if None == contents:
            print("Incorrect href !!")
            return

        hrefs = self.driver.find_elements_by_tag_name('a')

        for a in hrefs:
            href = a.get_attribute("href")

            if href:
                if contents in href:
                    a.click()
                    #self.make_screenshot()
                    return

    def make_screenshot(self, option = ""):
        import os

        time.sleep(1)

        url = self.driver.current_url
        print(("URL: ", url, " PATH: ", urlparse(url).path))
        filename = 'screenshot.visit.' + urlparse(url).path.replace('/', '.') + option + '.png'

        while '..' in filename:
            filename = filename.replace('..', '.')

        print(filename)
        location =  os.path.dirname(os.path.realpath(__file__))
        filename = (location + '/../../' + filename)

        print(filename)
        self.driver.execute_script("document.body.style.zoom='50%'")
        self.driver.save_screenshot(filename)
        self.driver.execute_script("document.body.style.zoom='100%'")

    def check_view(self, url = None, expected_views_count = 0, expected_links_count = 0):
        assert(url != None)
        self.visit(url)
        time.sleep(1)
        #self.make_screenshot('.check_view.' + str(expected_links_count))

        print(('\nVIEWS COUNT: ', views))
        print(('\nEXPECTED VIEWS COUNT: ', expected_views_count, '\n'))

        links = self.driver.find_elements_by_xpath("//table[@class='visitweb_ad']")
        print(('\nLINKS COUNT: ', len(links)))
        print(('\nEXPECTED LINKS COUNT: ', expected_links_count, '\n'))

    def check_clicks(self, url = None, click_type = 0):
        from selenium.webdriver.common.action_chains import ActionChains

        assert(url != None)
        self.visit(url)
        time.sleep(1)
        #self.make_screenshot()

        links = self.driver.find_elements_by_xpath("//table[@class='visitweb_ad']")
        print(('\nLINKS COUNT: ', len(links)))

        link_to_click = None

        for link in links:
            hrefs = link.find_elements_by_tag_name("a")
            j = 0

            for href in hrefs:
                if href and hasattr(href, 'text') and len(href.text.strip()): 
                    print(("=", href.text, "=\n"))
                if j > 0:
                    link_to_click = href
                    break;
                j = j + 1
            break

        if None == link_to_click:
            return 1


        if not click_type:
            time.sleep(2)
            link_to_click.click()
        else:
            if click_type & WEBM_CLICK:
                print('WEBM_CLICK\n')
                link_to_click.click()

            elif click_type & DOUBLE_CLICK:
                print('DOUBLE_CLICK\n')
                actionChains = ActionChains(self.driver)
                actionChains.double_click(link_to_click).perform()

        time.sleep(2)

        if ret != 1:
            return 1

        result = 0

        if not click_type:
            expected = 2
            print(('\nEXPECTED ', expected,': ', ret, '\n'))
            result = not (ret == expected)

            print(('\nUSER ', ret, '\n'))
        else:
            if click_type & DOUBLE_CLICK:
                result = not (ret < 1)
                print(('\nEXPECTED < 1: ', ret, '\n'))

            elif click_type & WEBM_CLICK:
                print(('\nEXPECTED < 1: ', ret, '\n'))
                result = not (ret < 1)

        print(('RESULT: ', result, '\n'))
        return int(result)

