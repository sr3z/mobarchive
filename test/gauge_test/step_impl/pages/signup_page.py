# coding: utf-8
import datetime
import time

from getgauge.python import DataStoreFactory
from selenium.webdriver.common.by import By

from step_impl.pages.base_page import BasePage


class SignUpPageLocators:
    SUBMIT = (By.XPATH, "//input[contains(@type, 'submit')]")
    CONFIRM_PASSWORD = (By.ID, 'user_password_confirmation')
    PASSWORD = (By.NAME, 'pass')
    EMAIL = (By.NAME, 'email')
    USER_NAME = (By.NAME, 'login')


class SignUpPage(BasePage):
    URL = '{}reg.php/'.format(BasePage.URL)

    def __init__(self, driver):
        self.driver = driver

        if None == self.URL:
            BasePage.URL = 'https://m.test.ihmur.pw/'
            self.URL = '{}reg.php/'.format(BasePage.URL)

    def signup(self):
        self.visit()

        user_name = 'test_web'
        password = '123456'
        email = 'test@visitweb.com'

        self.set(SignUpPageLocators.PASSWORD, password)
        self.set(SignUpPageLocators.USER_NAME, user_name)
        self.set(SignUpPageLocators.EMAIL, email)

        self.click(SignUpPageLocators.SUBMIT)
        time.sleep(1)
        DataStoreFactory.scenario_data_store().put('current_user', user_name)

    def visit(self):
        print(('URL: ', self.URL))
        self.driver.get(self.URL)

    def fill_field(self, name, value):
        field = (By.NAME, name)
        self.set(field, value)

