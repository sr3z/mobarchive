# coding: utf-8
import os

class Settings():
    TARGET_URL = 'http://ya.ru'
    WEBM_URL = 'https://macbookpro.sl/'
    SERVER_NAME = 'archive.test:8090'
    APP_ENDPOINT = 'http://' + SERVER_NAME + '/'
    MYSQL_HOST = None
    EMAIL = 'ads@visitweb.com'
    EMAIL_WEBM = 'webm@visitweb.com'
    IMAGE_URL = ''
    INDEX = '/var/www/local/index.html'
