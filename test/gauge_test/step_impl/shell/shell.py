import spur
import time
import re

from step_impl.settings import Settings

port = 22
username = "root"

class Ssh():
    def __init__(self, command = None):
        shell = spur.SshShell(
            hostname=Settings.SERVER_NAME,
            port=port,
            username=username,
            missing_host_key = spur.ssh.MissingHostKey.accept,
        )
        command_parsed = command.split()
        print(('COMMAND: ', command_parsed))

        with shell:
            result = shell.run(command_parsed)
            print((result.output))

def check_blocks(case_name = None):
    assert(case_name != None)

    if case_name == 'one_user':
        file_ = open(Settings.INDEX, 'r')
        text = file_.read()
        file_.close()

        regexp = re.compile("src=\"" + Settings.APP_ENDPOINT + "v/(.*) type.*" )
        match = regexp.findall(text)

        assert(match != None)

        print(match)

        assert(len(match) > 1)
