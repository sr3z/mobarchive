from socket import *
import sys
import time
import timeout_decorator
from timeout_decorator.timeout_decorator import TimeoutError
from collections import Iterable, OrderedDict
from codecs import decode

def hex_command(data):
    #data_str = '[ {} ]'.format(' ][ '.join(map(lambda i: str(i), data)))
    # print data_str
    map_data = ['{:02x}'.format(c) for c in data]
    #data_str_hex = '[ {} ]'.format(' ][ '.join(map(lambda c: '\\x{}'.format(c), map_data)))
    #print data_str_hex
    data_send = decode(''.join(map_data), 'hex_codec')
    print('DATA2:', data)
    return data_send

AISLE=1

if __name__ == "__main__":
    src_host = '172.17.6.81'
    src_host = '192.168.1.103'
    src_host = '127.0.0.1'

    dest_host = '192.168.1.103'

    if len(sys.argv) > 1:
        if sys.argv[1]:
            src_host = sys.argv[1]

        if len(sys.argv) > 2:
            if sys.argv[2]:
                dest_host = sys.argv[2]

        if len(sys.argv) > 3:
            if sys.argv[3]:
                AISLE = int(sys.argv[3])
        print("Using src:", src_host, " dest:", dest_host, 'AISL:', AISLE)
    else:
        print("Using default src:", src_host, " dest:", dest_host)
        #sys.exit()

TIMEOUT=60
port_src = 5000
port_dest = 5000

port2 = 5001

addr = (src_host,port_src)
addr2 = (src_host,port2)
controller = (dest_host,port_dest)

@timeout_decorator.timeout(TIMEOUT)
def wait_for_response(udp_socket):
    print("LISTEN AT:", addr2)
    message = udp_socket.recvfrom(1024)
    print('RECV: data: ', message)
    message = udp_socket.recvfrom(1024)
    print('RECV: data: ', message)
    print('')
    result = ord(list(message[0])[2])
    print("BYTE[3]:", result)
    print('')
    return int(result)

udp_socket = socket(AF_INET, SOCK_DGRAM)
udp_socket2 = socket(AF_INET, SOCK_DGRAM)

#print('BIND: ', addr)
#udp_socket.bind(addr)
#print('BIND 2: ', addr2)
#udp_socket2.bind(addr2)
print('')

#datum = dict({'1.open_passage':'\x02\x02\x00\x00',\
#              '2.rack_position': '\x01\x01\x00\x00',\
#              '3.mobile_racks_count': '\x0b\x00\x00\x00',\
#              '4.block_group': '\x03\x00\x00\x00',\
#              '5.unblock_group': '\x04\x00\x00\x00',\
#              '6.turn_on_vent': '\x06\x00\x00\x00'})
datum = OrderedDict([
                     #('1.open_passage', '\x02\x00\x00\x00'),\
                     ('1.open_passage', '\x02\x01\x00\x00'),\
                     #('2.mobile_racks_count', '\x0b\x00\x00\x00'),\
                     #('3.rack_position', '\x01\x01\x00\x00'),\
                     #('4.block_group', '\x03\x00\x00\x00'),\
                     #('5.unblock_group', '\x04\x00\x00\x00'),\
                     #('6.turn_on_vent', '\x06\x00\x00\x00')
                     ])

MOBILE_RACK_COUNT = 1

for name, data in datum.items():
    print(name)
    if data[0] == '\x01':
        for i in range(0, MOBILE_RACK_COUNT):
            for j in range(1, 3):
                data = [1, (i * 2 + j), 0, 0]
                print('data', data)
                data1 = [int(data[0]), int(i * 2 + j), int(data[2]), int(data[3])]

                print('SENDING FROM:', addr, ' FROM PAIR #', j, ' UPDATED DATA:', list(data1), ' TO:', controller)
                udp_socket.sendto(hex_command(data1), controller)
                try:
                    result = wait_for_response(udp_socket)
                except TimeoutError:
                    print("NO RESPONSE ..\n")
                time.sleep(0.5)
    else:
        if data[0] == '\x02':
            data1 = [2, AISLE, 0, 0]
            data = hex_command(data1)
        print('SENDING FROM:', addr, ' DATA:', list(data), ' TO:', controller)
        udp_socket.sendto('{0}'.format(data), controller)

        try:
            result = wait_for_response(udp_socket)
            if data[0] == '\x0b':
                MOBILE_RACK_COUNT = result 
                print("MOBILE_RACK_COUNT:", MOBILE_RACK_COUNT, "\n")
        except TimeoutError:
            print("NO RESPONSE ..\n")
            pass
        time.sleep(0.5)

print("DONE ..\n")
udp_socket.close()
print("EXITING ..")
